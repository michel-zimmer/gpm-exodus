#!/usr/bin/env python

from gmusicapi import Mobileclient
from oauth2client.client import OAuth2Credentials

from gpm_exodus.settins import Settings

if __name__ == '__main__':
    settings = Settings()
    api = Mobileclient()
    settings.ensure_user_data_dir_exists()
    result = api.perform_oauth(settings.oauth_credentials_file)
    print(isinstance(result, OAuth2Credentials))
