import os
from pathlib import Path

from appdirs import AppDirs


class Settings:
    def __init__(self):
        self.app_dirs = AppDirs('gpm-exodus', 'Michel Zimmer')
        self.oauth_credentials_file = os.path.join(self.app_dirs.user_data_dir, 'oauth.cred')

    def ensure_user_data_dir_exists(self):
        Path(self.app_dirs.user_data_dir).mkdir(parents=True, exist_ok=True)
