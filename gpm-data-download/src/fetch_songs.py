#!/usr/bin/env python
import json
import sys

from gmusicapi import Mobileclient

from gpm_exodus.settins import Settings

if __name__ == '__main__':
    settings = Settings()
    api = Mobileclient()
    api.oauth_login(Mobileclient.FROM_MAC_ADDRESS, settings.oauth_credentials_file)
    songs = api.get_all_songs()
    print(json.dumps(songs))
