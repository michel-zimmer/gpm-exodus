#!/usr/bin/env python
import json

from gmusicapi import Mobileclient

from gpm_exodus.settins import Settings

if __name__ == '__main__':
    settings = Settings()
    api = Mobileclient()
    api.oauth_login(Mobileclient.FROM_MAC_ADDRESS, settings.oauth_credentials_file)
    playlists = api.get_all_user_playlist_contents()
    print(json.dumps(playlists))
