# Google Play Music exodus

Utility scripts to dump data from Google Play Music based on the [`gmusicapi` Python module](https://github.com/simon-weber/gmusicapi).

## How to use

1. Run `src/perform_oauth.py` once to generate and store login credentials on this device. They are used by the following commands.
2. Run `src/fetch_playlists.py > gpm-exodus-playlists.json` to download all playlists.
3. Run `src/fetch_songs.py > gpm-exodus-songs.json` to download all songs.
