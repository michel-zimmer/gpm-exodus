package org.apache.hc.core5.http;

import io.vavr.collection.Seq;

public interface HeadersAssertions {
    static HeadersAssert assertThat(Seq<Header> headers) {
        return new HeadersAssert(headers);
    }
}
