package org.apache.hc.core5.http;

import io.vavr.collection.Seq;
import io.vavr.control.Option;
import org.assertj.core.api.AbstractStringAssert;
import org.assertj.core.api.InstanceOfAssertFactories;
import org.assertj.core.api.ObjectAssert;
import org.assertj.vavr.api.VavrAssertions;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.apache.commons.codec.binary.Base64.decodeBase64;
import static org.apache.commons.lang3.StringUtils.substringAfter;
import static org.apache.commons.lang3.StringUtils.substringBefore;
import static org.assertj.core.api.Assertions.assertThat;

public class HeadersAssert {
    private final Seq<Header> actualHeaders;

    public HeadersAssert(Seq<Header> actualHeaders) {
        this.actualHeaders = actualHeaders;
    }

    public ObjectAssert<Header> contain(final String headerName) {
        final Option<Header> maybeHeader = actualHeaders.find(header -> headerName.equals(header.getName()));
        VavrAssertions.assertThat(maybeHeader).isDefined();
        final Header header = maybeHeader.get();
        return assertThat(header);
    }

    public AbstractStringAssert<?> containAuthorization(
            final String type) {
        return contain("Authorization").extracting(authorizationHeader -> {
            final String authorizationHeaderValue = authorizationHeader.getValue();
            final String prefix = type + " ";
            assertThat(authorizationHeaderValue).startsWith(prefix);
            return authorizationHeaderValue.substring(prefix.length());
        }, InstanceOfAssertFactories.STRING);
    }

    public void containBasicAuthorization(final String expectedUsername,
                                          final String expectedPassword) {
        containAuthorization("Basic").satisfies(encodedBasicToken -> {
            final String basicToken = new String(decodeBase64(encodedBasicToken), UTF_8);
            assertThat(basicToken).matches("[^:]+:[^:]+");
            final String clientId = substringBefore(basicToken, ":");
            final String clientSecret = substringAfter(basicToken, ":");
            assertThat(clientId).isEqualTo(expectedUsername);
            assertThat(clientSecret).isEqualTo(expectedPassword);
        });
    }

    public void containBearerAuthorization(final String expectedBearerToken) {
        containAuthorization("Bearer")
                .satisfies(bearerToken -> assertThat(bearerToken).isEqualTo(expectedBearerToken));
    }
}
