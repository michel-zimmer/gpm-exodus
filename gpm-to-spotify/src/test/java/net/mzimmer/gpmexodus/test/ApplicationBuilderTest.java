package net.mzimmer.gpmexodus.test;

import com.wrapper.spotify.SpotifyTest;
import io.vavr.collection.List;
import io.vavr.collection.Seq;
import io.vavr.control.Option;
import net.mzimmer.gpmexodus.boot.Application;
import net.mzimmer.gpmexodus.boot.ApplicationBuilder;
import net.mzimmer.gpmexodus.infrastructure.callbackserver.CallbackServer;
import net.mzimmer.gpmexodus.lib.database.DataSourceTest;
import net.mzimmer.gpmexodus.lib.filesystem.FileReader;
import net.mzimmer.gpmexodus.lib.stdio.Stdio;

import javax.sql.DataSource;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

import static net.mzimmer.gpmexodus.lib.filesystem.FileReader.RESOURCES_FILE_READER;

public abstract class ApplicationBuilderTest extends DataSourceTest implements SerializationTest, SpotifyTest {
    private final AtomicReference<Option<Application>> applicationStore = new AtomicReference<>(Option.none());
    private final AtomicReference<Seq<Consumer<Application>>> applicationConsumerStore = new AtomicReference<>(List.empty());

    protected Stdio stdio() {
        return Stdio.SYSTEM;
    }

    protected FileReader fileReader() {
        return RESOURCES_FILE_READER;
    }

    protected CallbackServer callbackServer() {
        return CallbackServer.DEFAULT;
    }

    protected void withApplication(Consumer<Application> consumer) {
        final Option<Application> maybeApplication = applicationStore.getAcquire();
        maybeApplication.forEach(consumer);
        maybeApplication.onEmpty(() -> applicationConsumerStore.updateAndGet(consumers -> consumers.append(consumer)));
        applicationStore.setRelease(maybeApplication);
    }

    protected Application application() {
        return applicationStore.get().get();
    }

    private final ApplicationBuilder applicationBuilder =
            config -> {
                final Application application = new Application(config,
                                                                stdio(),
                                                                fileReader(),
                                                                objectMapper(),
                                                                migrator(),
                                                                callbackServer(),
                                                                spotifyHttpManager()) {
                    @Override
                    public DataSource dataSource() {
                        return ApplicationBuilderTest.this.dataSource();
                    }
                };
                applicationStore.getAcquire();
                applicationConsumerStore.get().forEach(consumer -> consumer.accept(application));
                applicationStore.setRelease(Option.of(application));
                return application;
            };

    protected ApplicationBuilder applicationBuilder() {
        return applicationBuilder;
    }
}
