package net.mzimmer.gpmexodus.test;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

import static net.mzimmer.gpmexodus.boot.ObjectMappers.DEFAULT_OBJECT_MAPPER;
import static org.assertj.core.api.Assertions.assertThat;

public interface SerializationTest {
    default ObjectMapper objectMapper() {
        return DEFAULT_OBJECT_MAPPER;
    }

    default InputStream resourceFileAsInputStream(final String path) {
        final InputStream inputStream = getClass().getClassLoader().getResourceAsStream(path);
        assertThat(inputStream).isNotNull();
        return inputStream;
    }

    default String readResourceFile(final String path) {
        final InputStream inputStream = getClass().getClassLoader().getResourceAsStream(path);
        assertThat(inputStream).isNotNull();
        return new BufferedReader(
                new InputStreamReader(inputStream, StandardCharsets.UTF_8))
                .lines()
                .collect(Collectors.joining("\n"));
    }

    default <T> T deserialize(final InputStream json,
                              final Class<T> type) {
        try {
            return objectMapper().readValue(json, type);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    default <T> T deserialize(final String json,
                              final Class<T> type) {
        try {
            return objectMapper().readValue(json, type);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
