package net.mzimmer.gpmexodus.boot;

import io.vavr.collection.Array;
import io.vavr.collection.Seq;
import net.mzimmer.gpmexodus.infrastructure.database.gpm.GpmAlbumEntity;
import net.mzimmer.gpmexodus.infrastructure.database.gpm.GpmPlaylistEntity;
import net.mzimmer.gpmexodus.infrastructure.database.gpm.GpmPlaylistEntryEntity;
import net.mzimmer.gpmexodus.infrastructure.database.gpm.GpmTrackEntity;
import net.mzimmer.gpmexodus.lib.stdio.AssertableStdio;
import net.mzimmer.gpmexodus.lib.stdio.Stdio;
import net.mzimmer.gpmexodus.test.ApplicationBuilderTest;
import org.junit.jupiter.api.Test;

import static net.mzimmer.gpmexodus.infrastructure.database.gpm.GpmAlbumEntityBuilder.aGpmAlbumEntity;
import static net.mzimmer.gpmexodus.infrastructure.database.gpm.GpmPlaylistEntityBuilder.aGpmPlaylistEntity;
import static net.mzimmer.gpmexodus.infrastructure.database.gpm.GpmPlaylistEntryEntityBuilder.aGpmPlaylistEntryEntity;
import static net.mzimmer.gpmexodus.infrastructure.database.gpm.GpmTrackEntityBuilder.aGpmTrackEntity;
import static net.mzimmer.gpmexodus.lib.stdio.StdioAssertions.assertThat;

class ListImportedPlaylistsMainTest extends ApplicationBuilderTest {
    @Test
    void shouldListImportedPlaylists() {
        // given
        final Stdio stdio = new AssertableStdio();
        final Main main = new Main(stdio, new Cli(), applicationBuilder());

        final Seq<GpmAlbumEntity> albums = Array.of(
                aGpmAlbumEntity().withId("album-1")
                                 .withName("Album 1")
                                 .build()
        );
        final Seq<GpmTrackEntity> tracks = Array.of(
                aGpmTrackEntity().withStoreId("track-1")
                                 .withTitle("Track 1")
                                 .withArtistName("Artist 1")
                                 .withAlbumId("album-1")
                                 .build()
        );
        final Seq<GpmPlaylistEntity> playlists = Array.of(
                aGpmPlaylistEntity().withId("playlist-1")
                                    .withName("Playlist 1")
                                    .withDescription("Playlist 1 description")
                                    .build(),
                aGpmPlaylistEntity().withId("playlist-2")
                                    .withName("Playlist 2")
                                    .withDescription("Playlist 2 description")
                                    .build(),
                aGpmPlaylistEntity().withId("playlist-3")
                                    .withName("Playlist 3")
                                    .withDescription("Playlist 3 description")
                                    .build()
        );
        final Seq<GpmPlaylistEntryEntity> playlistEntries = Array.of(
                aGpmPlaylistEntryEntity().withPlaylistId("playlist-1")
                                         .withPosition(1)
                                         .withTrackStoreId("track-1")
                                         .build(),
                aGpmPlaylistEntryEntity().withPlaylistId("playlist-3")
                                         .withPosition(1)
                                         .withTrackStoreId("track-1")
                                         .build(),
                aGpmPlaylistEntryEntity().withPlaylistId("playlist-3")
                                         .withPosition(2)
                                         .withTrackStoreId("track-1")
                                         .build()
        );

        givenAllMigrations();
        withApplication(application -> {
            application.gpmAlbumDao().insert(albums);
            application.gpmTrackDao().insert(tracks);
            application.gpmPlaylistDao().insert(playlists);
            application.gpmPlaylistEntryDao().insert(playlistEntries);
        });

        // when
        main.run("list-imported-gpm-playlists",
                 "--database",
                 "database.db");

        // then
        assertThat(stdio).containsStdout("""
                                                 id         | number of tracks | name       | description
                                                 -----------+------------------+------------+-----------------------
                                                 playlist-1 | 1                | Playlist 1 | Playlist 1 description
                                                 playlist-2 | 0                | Playlist 2 | Playlist 2 description
                                                 playlist-3 | 2                | Playlist 3 | Playlist 3 description
                                                 """);
    }
}
