package net.mzimmer.gpmexodus.boot;

import com.wrapper.spotify.IHttpManager;
import io.vavr.collection.Array;
import io.vavr.collection.Seq;
import net.mzimmer.gpmexodus.infrastructure.database.TrackLinkEntity;
import net.mzimmer.gpmexodus.infrastructure.database.gpm.GpmAlbumEntity;
import net.mzimmer.gpmexodus.infrastructure.database.gpm.GpmPlaylistEntity;
import net.mzimmer.gpmexodus.infrastructure.database.gpm.GpmPlaylistEntryEntity;
import net.mzimmer.gpmexodus.infrastructure.database.gpm.GpmTrackEntity;
import net.mzimmer.gpmexodus.lib.stdio.AssertableStdio;
import net.mzimmer.gpmexodus.lib.stdio.Stdio;
import net.mzimmer.gpmexodus.test.ApplicationBuilderTest;
import org.apache.hc.core5.http.Header;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.net.URI;

import static net.mzimmer.gpmexodus.infrastructure.database.TrackLinkEntityBuilder.aTrackLinkEntity;
import static net.mzimmer.gpmexodus.infrastructure.database.gpm.GpmAlbumEntityBuilder.aGpmAlbumEntity;
import static net.mzimmer.gpmexodus.infrastructure.database.gpm.GpmPlaylistEntityBuilder.aGpmPlaylistEntity;
import static net.mzimmer.gpmexodus.infrastructure.database.gpm.GpmPlaylistEntryEntityBuilder.aGpmPlaylistEntryEntity;
import static net.mzimmer.gpmexodus.infrastructure.database.gpm.GpmTrackEntityBuilder.aGpmTrackEntity;
import static net.mzimmer.gpmexodus.lib.stdio.StdioAssertions.assertThat;
import static org.apache.hc.core5.http.HeadersAssertions.assertThat;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class LinkSpotifyTracksMainTest extends ApplicationBuilderTest {
    @Mock private IHttpManager spotifyHttpManager;

    @Override
    public IHttpManager spotifyHttpManager() {
        return spotifyHttpManager;
    }

    @Test
    void shouldLinkSpotifyTracks() throws Exception {
        // given
        final Stdio stdio = new AssertableStdio();
        final Main main = new Main(stdio, new Cli(), applicationBuilder());

        final Seq<GpmAlbumEntity> albums = Array.of(
                aGpmAlbumEntity().withId("album-1")
                                 .withName("Test e2e Album")
                                 .build(),
                aGpmAlbumEntity().withId("album-2")
                                 .withName("Album 2")
                                 .build(),
                aGpmAlbumEntity().withId("album-3")
                                 .withName("Test e2e Album 2")
                                 .build()
        );
        final Seq<GpmTrackEntity> tracks = Array.of(
                aGpmTrackEntity().withStoreId("track-1")
                                 .withTitle("Test e2e Track 10")
                                 .withArtistName("Test e2e Artist")
                                 .withAlbumId("album-1")
                                 .build(),
                aGpmTrackEntity().withStoreId("track-2")
                                 .withTitle("Track 2")
                                 .withArtistName("Artist 2")
                                 .withAlbumId("album-2")
                                 .build(),
                aGpmTrackEntity().withStoreId("track-3")
                                 .withTitle("Track 3")
                                 .withArtistName("Artist 3")
                                 .withAlbumId("album-2")
                                 .build(),
                aGpmTrackEntity().withStoreId("track-4")
                                 .withTitle("Test e2e Track 18")
                                 .withArtistName("Test e2e Artist 2")
                                 .withAlbumId("album-3")
                                 .build()
        );
        final Seq<GpmPlaylistEntity> playlists = Array.of(
                aGpmPlaylistEntity().withId("playlist-1")
                                    .withName("Playlist 1")
                                    .withDescription("Playlist 1 description")
                                    .build()
        );
        final Seq<GpmPlaylistEntryEntity> playlistEntries = Array.of(
                aGpmPlaylistEntryEntity().withPlaylistId("playlist-1")
                                         .withPosition(1)
                                         .withTrackStoreId("track-1")
                                         .build(),
                aGpmPlaylistEntryEntity().withPlaylistId("playlist-1")
                                         .withPosition(2)
                                         .withTrackStoreId("track-3")
                                         .build(),
                aGpmPlaylistEntryEntity().withPlaylistId("playlist-1")
                                         .withPosition(3)
                                         .withTrackStoreId("track-2")
                                         .build(),
                aGpmPlaylistEntryEntity().withPlaylistId("playlist-1")
                                         .withPosition(4)
                                         .withTrackStoreId("track-3")
                                         .build(),
                aGpmPlaylistEntryEntity().withPlaylistId("playlist-1")
                                         .withPosition(5)
                                         .withTrackStoreId("track-4")
                                         .build()
        );
        final Seq<TrackLinkEntity> trackLinks = Array.of(
                aTrackLinkEntity().withGpmTrackStoreId("track-2")
                                  .withSpotifyTrackId("spotify-5")
                                  .build(),
                aTrackLinkEntity().withGpmTrackStoreId("track-3")
                                  .withSpotifyTrackId("spotify-2")
                                  .build()
        );

        givenAllMigrations();
        withApplication(application -> {
            application.gpmAlbumDao().insert(albums);
            application.gpmTrackDao().insert(tracks);
            application.gpmPlaylistDao().insert(playlists);
            application.gpmPlaylistEntryDao().insert(playlistEntries);
            application.trackLinkDao().insert(trackLinks);
        });

        final ArgumentCaptor<URI> tokenRequestUriCaptor = ArgumentCaptor.forClass(URI.class);
        final ArgumentCaptor<Header[]> tokenRequestHeadersCaptor = ArgumentCaptor.forClass(Header[].class);

        given(spotifyHttpManager.post(tokenRequestUriCaptor.capture(),
                                      tokenRequestHeadersCaptor.capture(),
                                      any()))
                .willReturn("""
                                    {"access_token":"<bearer-token>","token_type":"Bearer","expires_in":3600}""");

        final ArgumentCaptor<URI> trackSearchRequestUriCaptor = ArgumentCaptor.forClass(URI.class);
        final ArgumentCaptor<Header[]> trackSearchRequestHeadersCaptor = ArgumentCaptor.forClass(Header[].class);

        given(spotifyHttpManager.get(trackSearchRequestUriCaptor.capture(),
                                     trackSearchRequestHeadersCaptor.capture()))
                .willReturn(readResourceFile("spotify-track-search-results-two-matches.json"),
                            readResourceFile("spotify-track-search-results-one-match.json"));

        // when
        main.run("link-spotify-tracks",
                 "--playlist",
                 "playlist-1",
                 "--database",
                 "database.db",
                 "--spotify-client-id",
                 "spotify-client-id",
                 "--spotify-client-secret",
                 "spotify-client-secret");

        // then
        assertThat(stdio).containsStdout("""
                                                 Already linked tracks: 2
                                                 Newly linked tracks:   1
                                                 Not linked tracks:     1
                                                                                                  
                                                 [track-1] Test e2e Artist: Test e2e Track 10 (Test e2e Album)
                                                 Spotify ID             | Artist            | Title             | Album
                                                 -----------------------+-------------------+-------------------+-----------------
                                                 2Tw5UQbXuRAZ41FO2r1KlK | Test e2e Artist 2 | Test e2e Track 10 | Test e2e Album 2
                                                 4k8Yy2tBgRE7gO1LPc2mhX | Test e2e Artist 4 | Test e2e Track 10 | Test e2e Album 4
                                                 """);

        assertThat(trackSearchRequestUriCaptor.getAllValues()).hasSize(2);
        assertThat(trackSearchRequestHeadersCaptor.getAllValues()).hasSize(2);
        final URI tokenRequestUri = tokenRequestUriCaptor.getValue();
        final Seq<Header> tokenRequestHeaders = Array.of(tokenRequestHeadersCaptor.getValue());
        final URI trackSearchRequestUri1 = trackSearchRequestUriCaptor.getAllValues().get(0);
        final Seq<Header> trackSearchRequestHeaders1 = Array.of(trackSearchRequestHeadersCaptor.getAllValues().get(0));
        final URI trackSearchRequestUri2 = trackSearchRequestUriCaptor.getAllValues().get(1);
        final Seq<Header> trackSearchRequestHeaders2 = Array.of(trackSearchRequestHeadersCaptor.getAllValues().get(1));

        assertThat(tokenRequestUri).isEqualTo(URI.create("https://accounts.spotify.com:443/api/token"));
        assertThat(tokenRequestHeaders).containBasicAuthorization("spotify-client-id",
                                                                  "spotify-client-secret");

        assertThat(trackSearchRequestUri1).isEqualTo(URI.create("https://api.spotify.com:443/v1/search?q=track%3ATest+e2e+Track+10+album%3ATest+e2e+Album+artist%3ATest+e2e+Artist&type=track"));
        assertThat(trackSearchRequestHeaders1).containBearerAuthorization("<bearer-token>");

        assertThat(trackSearchRequestUri2).isEqualTo(URI.create("https://api.spotify.com:443/v1/search?q=track%3ATest+e2e+Track+18+album%3ATest+e2e+Album+2+artist%3ATest+e2e+Artist+2&type=track"));
        assertThat(trackSearchRequestHeaders2).containBearerAuthorization("<bearer-token>");
    }
}
