package net.mzimmer.gpmexodus.boot;

import io.vavr.collection.Array;
import io.vavr.collection.Seq;
import net.mzimmer.gpmexodus.infrastructure.database.TrackLinkEntity;
import net.mzimmer.gpmexodus.infrastructure.database.gpm.GpmAlbumEntity;
import net.mzimmer.gpmexodus.infrastructure.database.gpm.GpmTrackEntity;
import net.mzimmer.gpmexodus.lib.stdio.AssertableStdio;
import net.mzimmer.gpmexodus.lib.stdio.Stdio;
import net.mzimmer.gpmexodus.test.ApplicationBuilderTest;
import org.junit.jupiter.api.Test;

import static net.mzimmer.gpmexodus.infrastructure.database.TrackLinkEntityBuilder.aTrackLinkEntity;
import static net.mzimmer.gpmexodus.infrastructure.database.gpm.GpmAlbumEntityBuilder.aGpmAlbumEntity;
import static net.mzimmer.gpmexodus.infrastructure.database.gpm.GpmTrackEntityBuilder.aGpmTrackEntity;
import static net.mzimmer.gpmexodus.lib.stdio.StdioAssertions.assertThat;

class LinkSpotifyTrackMainTest extends ApplicationBuilderTest {
    @Test
    void shouldLinkSpotifyTrack() {
        // given
        final Stdio stdio = new AssertableStdio();
        final Main main = new Main(stdio, new Cli(), applicationBuilder());

        final Seq<GpmAlbumEntity> albums = Array.of(
                aGpmAlbumEntity().withId("album-1")
                                 .withName("Album 1")
                                 .build()
        );
        final Seq<GpmTrackEntity> tracks = Array.of(
                aGpmTrackEntity().withStoreId("track-1")
                                 .withTitle("Track 1")
                                 .withArtistName("Artist 1")
                                 .withAlbumId("album-1")
                                 .build()
        );

        givenAllMigrations();
        withApplication(application -> {
            application.gpmAlbumDao().insert(albums);
            application.gpmTrackDao().insert(tracks);
        });

        // when
        main.run("link-spotify-track",
                 "--database",
                 "database.db",
                 "--gpm-track-id",
                 "track-1",
                 "--spotify-track-id",
                 "spotify-1");

        // then
        assertThat(stdio).containsStdout("Linked GPM track-1 to Spotify spotify-1\n");
    }

    @Test
    void shouldLinkAlreadyLinkedSpotifyTrack() {
        // given
        final Stdio stdio = new AssertableStdio();
        final Main main = new Main(stdio, new Cli(), applicationBuilder());

        final Seq<GpmAlbumEntity> albums = Array.of(
                aGpmAlbumEntity().withId("album-1")
                                 .withName("Album 1")
                                 .build()
        );
        final Seq<GpmTrackEntity> tracks = Array.of(
                aGpmTrackEntity().withStoreId("track-1")
                                 .withTitle("Track 1")
                                 .withArtistName("Artist 1")
                                 .withAlbumId("album-1")
                                 .build()
        );
        final Seq<TrackLinkEntity> links = Array.of(
                aTrackLinkEntity().withGpmTrackStoreId("track-1")
                                  .withSpotifyTrackId("spotify-1")
                                  .build()
        );

        givenAllMigrations();
        withApplication(application -> {
            application.gpmAlbumDao().insert(albums);
            application.gpmTrackDao().insert(tracks);
            application.trackLinkDao().insert(links);
        });

        // when
        main.run("link-spotify-track",
                 "--database",
                 "database.db",
                 "--gpm-track-id",
                 "track-1",
                 "--spotify-track-id",
                 "spotify-1");

        // then
        assertThat(stdio).containsStderr("GPM track-1 is already linked to Spotify spotify-1\n");
    }
}
