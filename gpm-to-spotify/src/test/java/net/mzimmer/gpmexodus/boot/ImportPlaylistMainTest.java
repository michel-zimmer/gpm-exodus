package net.mzimmer.gpmexodus.boot;

import net.mzimmer.gpmexodus.infrastructure.database.gpm.GpmAlbumDao;
import net.mzimmer.gpmexodus.infrastructure.database.gpm.GpmPlaylistDao;
import net.mzimmer.gpmexodus.infrastructure.database.gpm.GpmPlaylistEntryDao;
import net.mzimmer.gpmexodus.infrastructure.database.gpm.GpmTrackDao;
import net.mzimmer.gpmexodus.lib.stdio.AssertableStdio;
import net.mzimmer.gpmexodus.lib.stdio.Stdio;
import net.mzimmer.gpmexodus.test.ApplicationBuilderTest;
import org.junit.jupiter.api.Test;

import static net.mzimmer.gpmexodus.infrastructure.database.gpm.GpmAlbumEntityBuilder.aGpmAlbumEntity;
import static net.mzimmer.gpmexodus.infrastructure.database.gpm.GpmPlaylistEntityBuilder.aGpmPlaylistEntity;
import static net.mzimmer.gpmexodus.infrastructure.database.gpm.GpmPlaylistEntryEntityBuilder.aGpmPlaylistEntryEntity;
import static net.mzimmer.gpmexodus.infrastructure.database.gpm.GpmTrackEntityBuilder.aGpmTrackEntity;
import static net.mzimmer.gpmexodus.lib.database.DaoAssertions.assertThat;
import static net.mzimmer.gpmexodus.lib.stdio.StdioAssertions.assertThat;

class ImportPlaylistMainTest extends ApplicationBuilderTest {
    @Test
    void shouldImportPlaylist() {
        // given
        final Stdio stdio = new AssertableStdio();
        final Main main = new Main(stdio, new Cli(), applicationBuilder());

        givenAllMigrations();

        // when
        main.run("import-gpm-playlists",
                 "--playlists",
                 "gpm-exodus-playlists.json",
                 "--database",
                 "database.db");

        // then
        assertThat(stdio).containsStdout("""
                                                 Number of changed playlists:        41
                                                 Number of changed tracks:           933
                                                 Number of changed albums:           555
                                                 Number of changed playlist entries: 1280
                                                 """);

        final GpmAlbumDao gpmAlbumDao = application().gpmAlbumDao();
        final GpmTrackDao gpmTrackDao = application().gpmTrackDao();
        final GpmPlaylistDao gpmPlaylistDao = application().gpmPlaylistDao();
        final GpmPlaylistEntryDao gpmPlaylistEntryDao = application().gpmPlaylistEntryDao();

        assertThat(gpmAlbumDao).contains(
                aGpmAlbumEntity().withId("Byz2vu46pibzmq4dt2noz2utzsy")
                                 .withName("Avengers: Endgame (Original Motion Picture Soundtrack)")
                                 .build()
        );
        assertThat(gpmTrackDao).contains(
                aGpmTrackEntity().withTitle("Portals")
                                 .withArtistName("Alan Silvestri")
                                 .withStoreId("Tqlcqki2qzhudejldszspmymxf4")
                                 .withAlbumId("Byz2vu46pibzmq4dt2noz2utzsy")
                                 .build()
        );
        assertThat(gpmPlaylistDao).contains(
                aGpmPlaylistEntity().withId("bfb51762-9bd3-4ffd-89ec-34655fe35658")
                                    .withName("Pure Energy")
                                    .withDescription("Maximum goosebumps and energy loaded soundtracks")
                                    .build()
        );
        assertThat(gpmPlaylistEntryDao).contains(
                aGpmPlaylistEntryEntity().withPlaylistId("bfb51762-9bd3-4ffd-89ec-34655fe35658")
                                         .withPosition(1)
                                         .withTrackStoreId("Tqlcqki2qzhudejldszspmymxf4")
                                         .build()
        );
    }
}
