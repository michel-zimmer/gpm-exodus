package net.mzimmer.gpmexodus.domain.gpm;

import net.mzimmer.gpmexodus.domain.gpm.types.GpmStoreId;
import net.mzimmer.gpmexodus.domain.types.ArtistName;
import net.mzimmer.gpmexodus.domain.types.TrackName;

import static net.mzimmer.gpmexodus.domain.gpm.GpmAlbumBuilder.aDefaultGpmAlbum;

public class GpmTrackBuilder {
    private TrackName title;
    private ArtistName artistName;
    private GpmAlbum album;
    private GpmStoreId storeId;

    private GpmTrackBuilder() {
    }

    public static GpmTrackBuilder aGpmTrack() {
        return new GpmTrackBuilder();
    }

    public static GpmTrackBuilder aDefaultGpmTrack() {
        return aGpmTrack().withTitle("<title>")
                          .withArtistName("<artist-name>")
                          .withAlbum(aDefaultGpmAlbum().build())
                          .withStoreId("<store-id>");
    }

    public GpmTrackBuilder withTitle(final TrackName title) {
        this.title = title;
        return this;
    }

    public GpmTrackBuilder withTitle(final String title) {
        return withTitle(new TrackName(title));
    }

    public GpmTrackBuilder withArtistName(final ArtistName artistName) {
        this.artistName = artistName;
        return this;
    }

    public GpmTrackBuilder withArtistName(final String artistName) {
        return withArtistName(new ArtistName(artistName));
    }

    public GpmTrackBuilder withAlbum(final GpmAlbum album) {
        this.album = album;
        return this;
    }

    public GpmTrackBuilder withStoreId(final GpmStoreId storeId) {
        this.storeId = storeId;
        return this;
    }

    public GpmTrackBuilder withStoreId(final String storeId) {
        return withStoreId(new GpmStoreId(storeId));
    }

    public GpmTrack build() {
        return new GpmTrack.Immutable(title,
                                      artistName,
                                      album,
                                      storeId);
    }
}