package net.mzimmer.gpmexodus.domain.gpm;

import net.mzimmer.gpmexodus.domain.gpm.types.GpmAlbumId;
import net.mzimmer.gpmexodus.domain.types.AlbumName;

public class GpmAlbumBuilder {
    private GpmAlbumId id;
    private AlbumName name;

    private GpmAlbumBuilder() {
    }

    public static GpmAlbumBuilder aGpmAlbum() {
        return new GpmAlbumBuilder();
    }

    public static GpmAlbumBuilder aDefaultGpmAlbum() {
        return aGpmAlbum().withId("<id>")
                          .withName("<name>");
    }

    public GpmAlbumBuilder withId(final GpmAlbumId id) {
        this.id = id;
        return this;
    }

    public GpmAlbumBuilder withId(final String id) {
        return withId(new GpmAlbumId(id));
    }

    public GpmAlbumBuilder withName(final AlbumName name) {
        this.name = name;
        return this;
    }

    public GpmAlbumBuilder withName(final String name) {
        return withName(new AlbumName(name));
    }

    public GpmAlbum build() {
        return new GpmAlbum.Immutable(id,
                                      name);
    }
}
