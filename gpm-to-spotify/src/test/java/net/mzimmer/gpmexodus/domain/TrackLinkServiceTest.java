package net.mzimmer.gpmexodus.domain;

import io.vavr.Tuple;
import io.vavr.collection.Array;
import io.vavr.collection.HashMap;
import io.vavr.collection.Map;
import io.vavr.collection.Seq;
import net.mzimmer.gpmexodus.domain.gpm.GpmPlaylist;
import net.mzimmer.gpmexodus.domain.gpm.GpmTrack;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmStoreId;
import net.mzimmer.gpmexodus.domain.spotify.SpotifyTrack;
import net.mzimmer.gpmexodus.domain.spotify.SpotifyTrackRepository;
import net.mzimmer.gpmexodus.domain.spotify.types.SpotifyTrackId;
import net.mzimmer.gpmexodus.domain.types.AlbumName;
import net.mzimmer.gpmexodus.domain.types.ArtistName;
import net.mzimmer.gpmexodus.domain.types.TrackName;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static net.mzimmer.gpmexodus.domain.gpm.GpmAlbumBuilder.aGpmAlbum;
import static net.mzimmer.gpmexodus.domain.gpm.GpmPlaylistBuilder.aDefaultGpmPlaylist;
import static net.mzimmer.gpmexodus.domain.gpm.GpmTrackBuilder.aDefaultGpmTrack;
import static net.mzimmer.gpmexodus.domain.gpm.GpmTrackBuilder.aGpmTrack;
import static net.mzimmer.gpmexodus.domain.spotify.SpotifyTrackBuilder.aDefaultSpotifyTrack;
import static net.mzimmer.gpmexodus.domain.spotify.SpotifyTrackBuilder.aSpotifyTrack;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.vavr.api.VavrAssertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@SuppressWarnings("unchecked")
@ExtendWith(MockitoExtension.class)
class TrackLinkServiceTest {
    private TrackLinkService trackLinkService;

    @Mock private TrackLinkRepository trackLinkRepository;
    @Mock private SpotifyTrackRepository spotifyTrackRepository;

    @Captor private ArgumentCaptor<Seq<GpmStoreId>> gpmStoreIdsCaptor;
    @Captor private ArgumentCaptor<TrackName> trackNameCaptor;
    @Captor private ArgumentCaptor<ArtistName> artistNameCaptor;
    @Captor private ArgumentCaptor<AlbumName> albumNameCaptor;

    @BeforeEach
    void setUp() {
        trackLinkService = new TrackLinkService(trackLinkRepository,
                                                spotifyTrackRepository);
    }

    @Test
    void shouldHaveEmptyResultIfPlaylistIsEmpty() {
        // given
        final GpmPlaylist gpmPlaylist = aDefaultGpmPlaylist().build();

        given(trackLinkRepository.trackLinks(any())).willReturn(HashMap.empty());

        // when
        final TrackLinkingResults result = trackLinkService.linkTracks(gpmPlaylist);

        // then
        assertThat(result.alreadyLinkedTracks()).isEmpty();
        assertThat(result.newlyLinkedTracks()).isEmpty();
        assertThat(result.notLinkedTracks()).isEmpty();
    }

    @Test
    void shouldHaveAlreadyLinkedTracks() {
        // given
        final GpmTrack gpmTrack = aDefaultGpmTrack().build();
        final GpmPlaylist gpmPlaylist = aDefaultGpmPlaylist().withTracks(gpmTrack, gpmTrack)
                                                             .build();
        final SpotifyTrackId spotifyTrackId = new SpotifyTrackId("<spotify-track-id>");
        final Map<GpmStoreId, SpotifyTrackId> alreadyLinkedTracks = HashMap.of(
                gpmTrack.storeId(), spotifyTrackId
        );

        given(trackLinkRepository.trackLinks(gpmStoreIdsCaptor.capture())).willReturn(alreadyLinkedTracks);

        // when
        final TrackLinkingResults result = trackLinkService.linkTracks(gpmPlaylist);

        // then
        assertThat(gpmStoreIdsCaptor.getValue()).containsExactly(gpmTrack.storeId());
        assertThat(result.alreadyLinkedTracks()).containsExactly(Tuple.of(gpmTrack, spotifyTrackId));
        assertThat(result.newlyLinkedTracks()).isEmpty();
        assertThat(result.notLinkedTracks()).isEmpty();
    }

    @Test
    void shouldHaveNewlyLinkedTracks() {
        // given
        final GpmTrack gpmTrack = aDefaultGpmTrack().build();
        final GpmPlaylist gpmPlaylist = aDefaultGpmPlaylist().withTracks(gpmTrack, gpmTrack)
                                                             .build();
        final SpotifyTrack spotifyTrack = aDefaultSpotifyTrack().build();

        given(trackLinkRepository.trackLinks(any())).willReturn(HashMap.empty());
        given(spotifyTrackRepository.tracks(trackNameCaptor.capture(),
                                            artistNameCaptor.capture(),
                                            albumNameCaptor.capture())).willReturn(Array.of(spotifyTrack));

        // when
        final TrackLinkingResults result = trackLinkService.linkTracks(gpmPlaylist);

        // then
        assertThat(trackNameCaptor.getValue()).isEqualTo(gpmTrack.title());
        assertThat(artistNameCaptor.getValue()).isEqualTo(gpmTrack.artistName());
        assertThat(albumNameCaptor.getValue()).isEqualTo(gpmTrack.album().name());
        assertThat(result.alreadyLinkedTracks()).isEmpty();
        assertThat(result.newlyLinkedTracks()).containsExactly(Tuple.of(gpmTrack, spotifyTrack));
        assertThat(result.notLinkedTracks()).isEmpty();
        verify(trackLinkRepository).save(new TrackLink(gpmTrack.storeId(),
                                                       spotifyTrack.id()));
    }

    @Test
    void shouldHaveNewlyLinkedTrackBecauseOfExactMatch() {
        // given
        final GpmTrack gpmTrack = aGpmTrack().withTitle("<title>")
                                             .withArtistName("<artist>")
                                             .withAlbum(aGpmAlbum().withId("<id>")
                                                                   .withName("<album>")
                                                                   .build())
                                             .withStoreId("<store-id>")
                                             .build();
        final GpmPlaylist gpmPlaylist = aDefaultGpmPlaylist().withTracks(gpmTrack)
                                                             .build();
        final SpotifyTrack spotifyTrack1 = aSpotifyTrack().withId("<id-1>")
                                                          .withTitle("<Title>")
                                                          .withArtistName("<Artist>")
                                                          .withAlbumName("<Album>")
                                                          .build();
        final SpotifyTrack spotifyTrack2 = aSpotifyTrack().withId("<id-2>")
                                                          .withTitle("<not-matching-title>")
                                                          .withArtistName("<artist>")
                                                          .withAlbumName("<album>")
                                                          .build();
        final Seq<SpotifyTrack> spotifyTracks = Array.of(spotifyTrack1, spotifyTrack2);

        given(trackLinkRepository.trackLinks(any())).willReturn(HashMap.empty());
        given(spotifyTrackRepository.tracks(any(),
                                            any(),
                                            any())).willReturn(spotifyTracks);

        // when
        final TrackLinkingResults result = trackLinkService.linkTracks(gpmPlaylist);

        // then
        assertThat(result.alreadyLinkedTracks()).isEmpty();
        assertThat(result.newlyLinkedTracks()).containsExactly(Tuple.of(gpmTrack, spotifyTrack1));
        assertThat(result.notLinkedTracks()).isEmpty();
    }

    @Test
    void shouldHaveNotLinkedTracks() {
        // given
        final GpmTrack gpmTrack = aDefaultGpmTrack().build();
        final GpmPlaylist gpmPlaylist = aDefaultGpmPlaylist().withTracks(gpmTrack, gpmTrack)
                                                             .build();
        final SpotifyTrack spotifyTrack1 = aDefaultSpotifyTrack().withId("<id-1>").build();
        final SpotifyTrack spotifyTrack2 = aDefaultSpotifyTrack().withId("<id-2>").build();
        final Seq<SpotifyTrack> spotifyTracks = Array.of(spotifyTrack1, spotifyTrack2);

        given(trackLinkRepository.trackLinks(any())).willReturn(HashMap.empty());
        given(spotifyTrackRepository.tracks(any(),
                                            any(),
                                            any())).willReturn(spotifyTracks);

        // when
        final TrackLinkingResults result = trackLinkService.linkTracks(gpmPlaylist);

        // then
        assertThat(result.alreadyLinkedTracks()).isEmpty();
        assertThat(result.newlyLinkedTracks()).isEmpty();
        assertThat(result.notLinkedTracks()).containsExactly(Tuple.of(gpmTrack, spotifyTracks));
    }
}
