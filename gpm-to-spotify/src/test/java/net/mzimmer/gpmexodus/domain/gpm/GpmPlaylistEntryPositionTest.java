package net.mzimmer.gpmexodus.domain.gpm;

import io.vavr.collection.List;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmPlaylistEntryPosition;
import org.junit.jupiter.api.Test;

import static org.assertj.vavr.api.VavrAssertions.assertThat;

class GpmPlaylistEntryPositionTest {
    @Test
    void shouldHaveOrdering() {
        // given
        final GpmPlaylistEntryPosition position3 = new GpmPlaylistEntryPosition(3);
        final GpmPlaylistEntryPosition position1 = new GpmPlaylistEntryPosition(1);
        final GpmPlaylistEntryPosition position2 = new GpmPlaylistEntryPosition(2);

        // when
        final List<GpmPlaylistEntryPosition> result = List.of(position3, position1, position2).sorted();

        // then
        assertThat(result).containsExactly(position1, position2, position3);
    }
}