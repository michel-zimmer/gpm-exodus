package net.mzimmer.gpmexodus.domain.gpm;

import io.vavr.collection.Array;
import io.vavr.collection.Seq;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmPlaylistId;
import net.mzimmer.gpmexodus.domain.types.PlaylistDescription;
import net.mzimmer.gpmexodus.domain.types.PlaylistName;

public class GpmPlaylistBuilder {
    private GpmPlaylistId id;
    private PlaylistName name;
    private PlaylistDescription description;
    private Seq<GpmTrack> tracks;

    private GpmPlaylistBuilder() {
    }

    public static GpmPlaylistBuilder aGpmPlaylist() {
        return new GpmPlaylistBuilder();
    }

    public static GpmPlaylistBuilder aDefaultGpmPlaylist() {
        return aGpmPlaylist().withId("<id>")
                             .withName("<name>")
                             .withDescription("<description>")
                             .withoutTracks();
    }

    public GpmPlaylistBuilder withId(final GpmPlaylistId id) {
        this.id = id;
        return this;
    }

    public GpmPlaylistBuilder withId(final String id) {
        return withId(new GpmPlaylistId(id));
    }

    public GpmPlaylistBuilder withName(final PlaylistName name) {
        this.name = name;
        return this;
    }

    public GpmPlaylistBuilder withName(final String name) {
        return withName(new PlaylistName(name));
    }

    public GpmPlaylistBuilder withDescription(final PlaylistDescription description) {
        this.description = description;
        return this;
    }

    public GpmPlaylistBuilder withDescription(final String description) {
        return withDescription(new PlaylistDescription(description));
    }

    public GpmPlaylistBuilder withTracks(final Seq<GpmTrack> tracks) {
        this.tracks = tracks;
        return this;
    }

    public GpmPlaylistBuilder withTracks(final GpmTrack... tracks) {
        return withTracks(Array.of(tracks));
    }

    public GpmPlaylistBuilder withoutTracks() {
        return withTracks();
    }

    public GpmPlaylist build() {
        return new GpmPlaylist.Immutable(id,
                                         name,
                                         description,
                                         tracks);
    }
}
