package net.mzimmer.gpmexodus.domain.spotify;

import net.mzimmer.gpmexodus.domain.spotify.types.SpotifyTrackId;
import net.mzimmer.gpmexodus.domain.types.AlbumName;
import net.mzimmer.gpmexodus.domain.types.ArtistName;
import net.mzimmer.gpmexodus.domain.types.TrackName;

public class SpotifyTrackBuilder {
    private SpotifyTrackId id;
    private TrackName title;
    private ArtistName artistName;
    private AlbumName albumName;

    private SpotifyTrackBuilder() {
    }

    public static SpotifyTrackBuilder aSpotifyTrack() {
        return new SpotifyTrackBuilder();
    }

    public static SpotifyTrackBuilder aDefaultSpotifyTrack() {
        return aSpotifyTrack().withId("<id>")
                              .withTitle("<title>")
                              .withArtistName("<artist>")
                              .withAlbumName("<album>");
    }

    public SpotifyTrackBuilder withId(final SpotifyTrackId id) {
        this.id = id;
        return this;
    }

    public SpotifyTrackBuilder withId(final String id) {
        return withId(new SpotifyTrackId(id));
    }

    public SpotifyTrackBuilder withTitle(final TrackName title) {
        this.title = title;
        return this;
    }

    public SpotifyTrackBuilder withTitle(final String title) {
        return withTitle(new TrackName(title));
    }

    public SpotifyTrackBuilder withArtistName(final ArtistName artistName) {
        this.artistName = artistName;
        return this;
    }

    public SpotifyTrackBuilder withArtistName(final String artistName) {
        return withArtistName(new ArtistName(artistName));
    }

    public SpotifyTrackBuilder withAlbumName(final AlbumName albumName) {
        this.albumName = albumName;
        return this;
    }

    public SpotifyTrackBuilder withAlbumName(final String albumName) {
        return withAlbumName(new AlbumName(albumName));
    }

    public SpotifyTrack build() {
        return new SpotifyTrack.Immutable(id,
                                          title,
                                          artistName,
                                          albumName);
    }
}