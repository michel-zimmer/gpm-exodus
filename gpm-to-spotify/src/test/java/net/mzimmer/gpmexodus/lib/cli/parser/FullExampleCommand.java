package net.mzimmer.gpmexodus.lib.cli.parser;

import io.vavr.collection.List;
import io.vavr.control.Option;
import net.mzimmer.gpmexodus.lib.cli.CommandDefinition;
import net.mzimmer.gpmexodus.lib.cli.SubCommandDefinition;

final record FullExampleCommand(SomeParameterWithValue someParameterWithValue,
                                SomeOtherParameterWithValue someOtherParameterWithValue) implements TestCommand {
    public static final CommandDefinition<FullExampleCommand> DEFINITION = CommandDefinition.commandDefinition(
            List.of(SomeParameterWithValue.DEFINITION),
            Option.of(SubCommandDefinition.subCommandDefinition("full-example",
                                                                List.of(SomeOtherParameterWithValue.DEFINITION))),
            parameters -> new FullExampleCommand(SomeParameterWithValue.DEFINITION.payload(parameters),
                                                 SomeOtherParameterWithValue.DEFINITION.payload(parameters))
    );
}
