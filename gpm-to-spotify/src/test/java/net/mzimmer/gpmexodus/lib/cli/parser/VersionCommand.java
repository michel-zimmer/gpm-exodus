package net.mzimmer.gpmexodus.lib.cli.parser;

import io.vavr.collection.List;
import io.vavr.control.Option;
import net.mzimmer.gpmexodus.lib.cli.CommandDefinition;

import static net.mzimmer.gpmexodus.lib.cli.CommandDefinition.commandDefinition;
import static net.mzimmer.gpmexodus.lib.cli.ParameterDefinition.parameterDefinition;

final class VersionCommand implements TestCommand {
    private VersionCommand() {
    }

    public static final CommandDefinition<VersionCommand> DEFINITION = commandDefinition(
            List.of(parameterDefinition("version")),
            Option.none(),
            parameters -> versionCommand()
    );

    private static final VersionCommand INSTANCE = new VersionCommand();

    public static VersionCommand versionCommand() {
        return INSTANCE;
    }
}
