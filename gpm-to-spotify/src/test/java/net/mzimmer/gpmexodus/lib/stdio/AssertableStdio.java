package net.mzimmer.gpmexodus.lib.stdio;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;

import static io.vavr.API.TODO;

public class AssertableStdio implements Stdio {
    private final ByteArrayOutputStream stdout = new ByteArrayOutputStream();
    private final ByteArrayOutputStream stderr = new ByteArrayOutputStream();
    private final PrintStream stdoutPrintStream = new PrintStream(stdout);
    private final PrintStream stderrPrintStream = new PrintStream(stderr);

    public String stdoutString() {
        return stdout.toString();
    }

    public String stderrString() {
        return stderr.toString();
    }

    @Override
    public InputStream stdin() {
        return TODO();
    }

    @Override
    public PrintStream stdout() {
        return stdoutPrintStream;
    }

    @Override
    public PrintStream stderr() {
        return stderrPrintStream;
    }
}