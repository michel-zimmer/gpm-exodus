package net.mzimmer.gpmexodus.lib.cli.parser;

import io.vavr.collection.List;
import io.vavr.control.Option;
import net.mzimmer.gpmexodus.lib.cli.CommandDefinition;
import net.mzimmer.gpmexodus.lib.cli.ParameterDefinition;
import net.mzimmer.gpmexodus.lib.cli.ParameterName;
import org.junit.jupiter.api.Test;

import static java.util.function.Function.identity;
import static net.mzimmer.gpmexodus.lib.cli.CommandDefinition.commandDefinition;
import static net.mzimmer.gpmexodus.lib.cli.ParameterDefinition.parameterDefinition;
import static net.mzimmer.gpmexodus.lib.cli.SubCommandDefinition.subCommandDefinition;
import static net.mzimmer.gpmexodus.lib.cli.parser.CommandDefinitions.commandDefinitions;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

class CommandDefinitionsTest {
    @Test
    void shouldNotAllowTheSameSubCommandNameMultipleTimes() {
        // given
        final var commandDefinition = commandDefinition(List.empty(),
                                                        Option.of(subCommandDefinition("sub-command",
                                                                                       List.empty())),
                                                        null);

        // when
        final Throwable result = catchThrowable(() -> commandDefinitions(commandDefinition, commandDefinition));

        // then
        assertThat(result).hasMessage("Sub command sub-command has been defined multiple times");
    }

    @Test
    void shouldNotAllowDifferentDefinitionsForGlobalParameters() {
        // given
        final ParameterDefinition<Void> parameterDefinition1 = parameterDefinition("version");
        final ParameterDefinition<String> parameterDefinition2 = parameterDefinition("version", identity());
        final var commandDefinition1 = commandDefinition(List.of(parameterDefinition1),
                                                         Option.none(),
                                                         null);
        final var commandDefinition2 = commandDefinition(List.of(parameterDefinition2),
                                                         Option.none(),
                                                         null);

        // when
        final Throwable result = catchThrowable(() -> commandDefinitions(commandDefinition1, commandDefinition2));

        // then
        assertThat(result).hasMessage("Global parameter version has been defined in different ways");
    }

    @Test
    void shouldNotAllowDifferentDefinitionsForSubCommandParameters() {
        // given
        final ParameterDefinition<Void> parameterDefinition = parameterDefinition("version");
        final var commandDefinition = commandDefinition(List.empty(),
                                                        Option.of(
                                                                subCommandDefinition("sub-command",
                                                                                     List.of(parameterDefinition,
                                                                                             parameterDefinition))
                                                        ),
                                                        null);

        // when
        final Throwable result = catchThrowable(() -> commandDefinitions(commandDefinition));

        // then
        assertThat(result).hasMessage("Sub command sub-command parameter version has been defined multiple times");
    }

    @Test
    void shouldHaveGlobalParameter() {
        // given
        final ParameterDefinition<Void> parameterDefinition = parameterDefinition("version");
        final CommandDefinition<TestCommand> commandDefinition = commandDefinition(List.of(parameterDefinition),
                                                                                   Option.none(),
                                                                                   null);
        final CommandDefinitions<TestCommand> commandDefinitions = commandDefinitions(commandDefinition);

        // when
        final Option<ParameterDefinition<?>> result = commandDefinitions.findGlobalParameterDefinition(new ParameterName("version"));

        // then
        assertThat(result).contains(parameterDefinition);
    }

    @Test
    void shouldNotHaveGlobalParameter() {
        // given
        final ParameterDefinition<Void> parameterDefinition = parameterDefinition("help");
        final CommandDefinition<TestCommand> commandDefinition = commandDefinition(List.of(parameterDefinition),
                                                                                   Option.none(),
                                                                                   null);
        final CommandDefinitions<TestCommand> commandDefinitions = commandDefinitions(commandDefinition);

        // when
        final Option<ParameterDefinition<?>> result = commandDefinitions.findGlobalParameterDefinition(new ParameterName("version"));

        // then
        assertThat(result).isEmpty();
    }
}