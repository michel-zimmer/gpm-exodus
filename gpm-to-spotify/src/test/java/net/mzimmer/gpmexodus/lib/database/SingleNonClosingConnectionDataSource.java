package net.mzimmer.gpmexodus.lib.database;

import io.vavr.control.Option;

import javax.sql.DataSource;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;
import java.util.logging.Logger;

import static io.vavr.API.TODO;

public class SingleNonClosingConnectionDataSource implements DataSource, AutoCloseable {
    private final DataSource delegate;
    private final AtomicReference<Option<NonClosingConnection>> connectionStore = new AtomicReference<>(Option.none());

    public SingleNonClosingConnectionDataSource(final DataSource delegate) {
        this.delegate = delegate;
    }

    @Override
    public void close() throws SQLException {
        getConnection().close();
    }

    private NonClosingConnection getSingleNonClosingConnection() {
        final Option<NonClosingConnection> maybeConnection = connectionStore.getAcquire();
        final NonClosingConnection connection = maybeConnection.fold(this::buildConnection, Function.identity());
        connectionStore.setRelease(Option.some(connection));
        return connection;
    }

    private NonClosingConnection buildConnection() {
        try {
            return new NonClosingConnection(delegate.getConnection());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Connection getConnection() {
        return getSingleNonClosingConnection();
    }

    @Override
    public Connection getConnection(final String username,
                                    final String password) {
        return TODO();
    }

    @Override
    public PrintWriter getLogWriter() throws SQLException {
        return delegate.getLogWriter();
    }

    @Override
    public void setLogWriter(final PrintWriter out) throws SQLException {
        delegate.setLogWriter(out);
    }

    @Override
    public void setLoginTimeout(final int seconds) throws SQLException {
        delegate.setLoginTimeout(seconds);
    }

    @Override
    public int getLoginTimeout() throws SQLException {
        return delegate.getLoginTimeout();
    }

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        return delegate.getParentLogger();
    }

    @Override
    public <T> T unwrap(final Class<T> iface) throws SQLException {
        return delegate.unwrap(iface);
    }

    @Override
    public boolean isWrapperFor(final Class<?> iface) throws SQLException {
        return delegate.isWrapperFor(iface);
    }
}
