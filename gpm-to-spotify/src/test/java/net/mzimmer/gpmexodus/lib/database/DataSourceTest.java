package net.mzimmer.gpmexodus.lib.database;

import net.mzimmer.gpmexodus.lib.database.flyway.FlywayMigrator;
import org.junit.jupiter.api.AfterEach;
import org.sqlite.SQLiteDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public abstract class DataSourceTest {
    private final SingleNonClosingConnectionDataSource dataSource;
    private final Migrator migrator;

    public DataSourceTest() {
        SQLiteDataSource sqLiteDataSource = new SQLiteDataSource();
        sqLiteDataSource.setUrl("jdbc:sqlite::memory:");
        this.dataSource = new SingleNonClosingConnectionDataSource(sqLiteDataSource);
        migrator = new FlywayMigrator();
    }

    @AfterEach
    void tearDown() throws SQLException {
        dataSource.close();
    }

    protected DataSource dataSource() {
        return dataSource;
    }

    protected Migrator migrator() {
        return migrator;
    }

    protected void givenAllMigrations() {
        givenAllMigrations(dataSource());
    }

    protected void givenAllMigrations(DataSource dataSource) {
        migrator().migrate(dataSource);
    }

    protected void givenSql(String sql) {
        givenSql(sql, dataSource());
    }

    protected void givenSql(String sql,
                            DataSource dataSource) {
        try (Connection connection = dataSource.getConnection()) {
            try (Statement statement = connection.createStatement()) {
                statement.execute(sql);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
