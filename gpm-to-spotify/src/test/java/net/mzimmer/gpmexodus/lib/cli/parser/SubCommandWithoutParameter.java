package net.mzimmer.gpmexodus.lib.cli.parser;

import io.vavr.collection.List;
import io.vavr.control.Option;
import net.mzimmer.gpmexodus.lib.cli.CommandDefinition;

import static net.mzimmer.gpmexodus.lib.cli.CommandDefinition.commandDefinition;
import static net.mzimmer.gpmexodus.lib.cli.SubCommandDefinition.subCommandDefinition;

final class SubCommandWithoutParameter implements TestCommand {
    private SubCommandWithoutParameter() {
    }

    public static final CommandDefinition<SubCommandWithoutParameter> DEFINITION = commandDefinition(
            List.empty(),
            Option.of(subCommandDefinition("sub-command")),
            parameters -> subCommandWithoutParameter()
    );

    private static final SubCommandWithoutParameter INSTANCE = new SubCommandWithoutParameter();

    public static SubCommandWithoutParameter subCommandWithoutParameter() {
        return INSTANCE;
    }
}
