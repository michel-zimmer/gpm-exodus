package net.mzimmer.gpmexodus.lib.cli.parser;

import io.vavr.collection.List;
import io.vavr.control.Option;
import net.mzimmer.gpmexodus.lib.cli.CommandDefinition;

import static net.mzimmer.gpmexodus.lib.cli.CommandDefinition.commandDefinition;
import static net.mzimmer.gpmexodus.lib.cli.SubCommandDefinition.subCommandDefinition;

final record SubCommandWithParameter(SomeParameterWithValue someParameter) implements TestCommand {
    public static final CommandDefinition<SubCommandWithParameter> DEFINITION = commandDefinition(
            List.empty(),
            Option.of(subCommandDefinition("sub-command",
                                           List.of(SomeParameterWithValue.DEFINITION))),
            parameters -> new SubCommandWithParameter(SomeParameterWithValue.DEFINITION.payload(parameters))
    );
}
