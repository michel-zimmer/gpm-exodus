package net.mzimmer.gpmexodus.lib.stdio;

import static org.assertj.core.api.Assertions.assertThat;

public class StdioAssert {
    private final AssertableStdio assertableStdio;

    StdioAssert(AssertableStdio assertableStdio) {
        this.assertableStdio = assertableStdio;
    }

    public StdioAssert containsStdout(final String expected) {
        final String actual = assertableStdio.stdoutString();
        assertThat(actual).isEqualTo(expected);
        return this;
    }

    public StdioAssert containsStderr(final String expected) {
        final String actual = assertableStdio.stderrString();
        assertThat(actual).isEqualTo(expected);
        return this;
    }
}
