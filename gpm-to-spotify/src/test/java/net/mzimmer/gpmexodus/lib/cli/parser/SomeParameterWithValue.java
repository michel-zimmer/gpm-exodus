package net.mzimmer.gpmexodus.lib.cli.parser;

import net.mzimmer.gpmexodus.lib.cli.ParameterDefinition;

import static net.mzimmer.gpmexodus.lib.cli.ParameterDefinition.parameterDefinition;

record SomeParameterWithValue(String value) {
    public static final ParameterDefinition<SomeParameterWithValue> DEFINITION =
            parameterDefinition("some-parameter", SomeParameterWithValue::new);
}
