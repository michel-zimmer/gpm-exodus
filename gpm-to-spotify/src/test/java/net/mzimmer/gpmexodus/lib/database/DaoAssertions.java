package net.mzimmer.gpmexodus.lib.database;

public interface DaoAssertions {
    static <ENTITY extends Entity<PRIMARY_KEY>, PRIMARY_KEY> DaoAssert<ENTITY, PRIMARY_KEY> assertThat(final Dao<ENTITY, PRIMARY_KEY> dao) {
        return new DaoAssert<>(dao);
    }
}
