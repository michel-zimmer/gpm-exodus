package net.mzimmer.gpmexodus.lib.cli.parser;

import io.vavr.control.Either;
import org.junit.jupiter.api.Test;

import static net.mzimmer.gpmexodus.lib.cli.parser.CommandDefinitions.commandDefinitions;
import static net.mzimmer.gpmexodus.lib.cli.parser.SubCommandWithoutParameter.subCommandWithoutParameter;
import static net.mzimmer.gpmexodus.lib.cli.parser.VersionCommand.versionCommand;
import static org.assertj.vavr.api.VavrAssertions.assertThat;

class ParserTest {
    @Test
    void shouldHaveGlobalCommandImplicitlyFromParameter() {
        // given
        final CommandDefinitions<TestCommand> commandDefinitions = commandDefinitions(VersionCommand.DEFINITION);
        final Parser<TestCommand> parser = new Parser<>(commandDefinitions);

        // when
        final Either<String, TestCommand> result = parser.parse("--version");

        // then
        assertThat(result).containsOnRight(versionCommand());
    }

    @Test
    void shouldHaveSubCommand() {
        // given
        final CommandDefinitions<TestCommand> commandDefinitions = commandDefinitions(SubCommandWithoutParameter.DEFINITION);
        final Parser<TestCommand> parser = new Parser<>(commandDefinitions);

        // when
        final Either<String, TestCommand> result = parser.parse("sub-command");

        // then
        assertThat(result).containsOnRight(subCommandWithoutParameter());
    }

    @Test
    void shouldHaveSubCommandWithParameterWithValue() {
        // given
        final CommandDefinitions<TestCommand> commandDefinitions = commandDefinitions(SubCommandWithParameter.DEFINITION);
        final Parser<TestCommand> parser = new Parser<>(commandDefinitions);

        // when
        final Either<String, TestCommand> result = parser.parse("sub-command", "--some-parameter", "value");

        // then
        assertThat(result).containsOnRight(new SubCommandWithParameter(new SomeParameterWithValue("value")));
    }

    @Test
    void shouldHaveFullExampleCommand() {
        // given
        final CommandDefinitions<TestCommand> commandDefinitions = commandDefinitions(FullExampleCommand.DEFINITION);
        final Parser<TestCommand> parser = new Parser<>(commandDefinitions);

        // when
        final Either<String, TestCommand> result = parser.parse("--some-parameter", "--a--",
                                                                "full-example",
                                                                "--some-other-parameter", "b");

        // then
        assertThat(result).containsOnRight(new FullExampleCommand(new SomeParameterWithValue("--a--"),
                                                                  new SomeOtherParameterWithValue("b")));
    }
}
