package net.mzimmer.gpmexodus.lib;

import io.vavr.Tuple;
import io.vavr.Tuple0;
import io.vavr.Tuple3;
import io.vavr.collection.Array;
import io.vavr.collection.Seq;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class TableRendererTest {
    @Test
    void shouldRenderTableWithoutHeader() {
        // given
        final Seq<Tuple3<String, String, String>> rows = Array.of(
                Tuple.of("top left", "top mid", "top right"),
                Tuple.of("mid left", "mid mid", "mid right"),
                Tuple.of("bottom left", "bottom mid", "bottom right")
        );
        final TableRenderer tableRenderer = new TableRenderer();

        // when
        final String result = tableRenderer.renderAsTable(rows);

        // then
        assertThat(result).isEqualTo("""
                                             top left    | top mid    | top right
                                             mid left    | mid mid    | mid right
                                             bottom left | bottom mid | bottom right""");
    }

    @Test
    void shouldRenderTableWithHeader() {
        // given
        final Tuple3<String, String, String> header = Tuple.of("left", "mid", "right");
        final Seq<Tuple3<String, String, String>> rows = Array.of(
                Tuple.of("top left", "top mid", "top right"),
                Tuple.of("mid left", "mid mid", "mid right"),
                Tuple.of("bottom left", "bottom mid", "bottom right")
        );
        final TableRenderer tableRenderer = new TableRenderer();

        // when
        final String result = tableRenderer.renderAsTable(header, rows);

        // then
        assertThat(result).isEqualTo("""
                                             left        | mid        | right
                                             ------------+------------+-------------
                                             top left    | top mid    | top right
                                             mid left    | mid mid    | mid right
                                             bottom left | bottom mid | bottom right""");
    }

    @Test
    void shouldRenderTableWithoutHeaderAndWithoutRows() {
        // given
        final Seq<Tuple3<String, String, String>> rows = Array.of();
        final TableRenderer tableRenderer = new TableRenderer();

        // when
        final String result = tableRenderer.renderAsTable(rows);

        // then
        assertThat(result).isEqualTo("");
    }

    @Test
    void shouldRenderTableWithHeaderAndWithoutColumns() {
        // given
        final Tuple0 header = Tuple.empty();
        final Seq<Tuple0> rows = Array.of(
                Tuple.empty(),
                Tuple.empty(),
                Tuple.empty()
        );
        final TableRenderer tableRenderer = new TableRenderer();

        // when
        final String result = tableRenderer.renderAsTable(header, rows);

        // then
        assertThat(result).isEqualTo("");
    }

    @Test
    void shouldRenderTableWithoutHeaderAndWithoutColumns() {
        // given
        final Seq<Tuple0> rows = Array.of(
                Tuple.empty(),
                Tuple.empty(),
                Tuple.empty()
        );
        final TableRenderer tableRenderer = new TableRenderer();

        // when
        final String result = tableRenderer.renderAsTable(rows);

        // then
        assertThat(result).isEqualTo("");
    }
}