package net.mzimmer.gpmexodus.lib.database;

import io.vavr.control.Option;
import org.assertj.core.api.ObjectAssert;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.vavr.api.VavrAssertions.assertThat;

public class DaoAssert<ENTITY extends Entity<PRIMARY_KEY>, PRIMARY_KEY> {
    private final Dao<ENTITY, PRIMARY_KEY> dao;

    public DaoAssert(Dao<ENTITY, PRIMARY_KEY> dao) {
        this.dao = dao;
    }

    public ObjectAssert<ENTITY> contains(ENTITY expected) {
        final Option<ENTITY> maybeActual = dao.findByPrimaryKey(expected.primaryKey());
        assertThat(maybeActual).isDefined();
        final ENTITY actual = maybeActual.get();
        return assertThat(actual).isEqualTo(expected);
    }
}
