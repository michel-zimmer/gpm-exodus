package net.mzimmer.gpmexodus.lib.cli.parser;

import net.mzimmer.gpmexodus.lib.cli.ParameterDefinition;

import static net.mzimmer.gpmexodus.lib.cli.ParameterDefinition.parameterDefinition;

record SomeOtherParameterWithValue(String value) {
    public static final ParameterDefinition<SomeOtherParameterWithValue> DEFINITION = parameterDefinition(
            "some-other-parameter",
            SomeOtherParameterWithValue::new
    );
}
