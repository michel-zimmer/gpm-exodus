package net.mzimmer.gpmexodus.lib.stdio;

import org.assertj.core.api.Assertions;

public interface StdioAssertions {
    static StdioAssert assertThat(Stdio stdio) {
        Assertions.assertThat(stdio).isInstanceOf(AssertableStdio.class);
        return new StdioAssert((AssertableStdio) stdio);
    }
}
