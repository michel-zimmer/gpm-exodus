package net.mzimmer.gpmexodus.infrastructure.database;

import net.mzimmer.gpmexodus.domain.gpm.types.GpmPlaylistId;
import net.mzimmer.gpmexodus.domain.spotify.types.SpotifyPlaylistId;

public class PlaylistLinkEntityBuilder {
    private GpmPlaylistId gpmPlaylistId;
    private SpotifyPlaylistId spotifyPlaylistId;

    private PlaylistLinkEntityBuilder() {
    }

    public static PlaylistLinkEntityBuilder aPlaylistLinkEntity() {
        return new PlaylistLinkEntityBuilder();
    }

    public PlaylistLinkEntityBuilder withGpmPlaylistId(GpmPlaylistId gpmPlaylistId) {
        this.gpmPlaylistId = gpmPlaylistId;
        return this;
    }

    public PlaylistLinkEntityBuilder withGpmPlaylistId(String gpmPlaylistId) {
        return withGpmPlaylistId(new GpmPlaylistId(gpmPlaylistId));
    }

    public PlaylistLinkEntityBuilder withSpotifyPlaylistId(SpotifyPlaylistId spotifyPlaylistId) {
        this.spotifyPlaylistId = spotifyPlaylistId;
        return this;
    }

    public PlaylistLinkEntityBuilder withSpotifyPlaylistId(String spotifyPlaylistId) {
        return withSpotifyPlaylistId(new SpotifyPlaylistId(spotifyPlaylistId));
    }

    public PlaylistLinkEntity build() {
        return new PlaylistLinkEntity(gpmPlaylistId,
                                      spotifyPlaylistId);
    }
}
