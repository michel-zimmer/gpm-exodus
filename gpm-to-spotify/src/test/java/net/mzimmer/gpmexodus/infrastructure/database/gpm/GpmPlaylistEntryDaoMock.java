package net.mzimmer.gpmexodus.infrastructure.database.gpm;

import io.vavr.collection.Stream;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmPlaylistId;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

interface GpmPlaylistEntryDaoMock {
    static void givenPlaylistEntryDaoWith(GpmPlaylistEntryDao gpmPlaylistEntryDao,
                                          GpmPlaylistEntryEntity... playlistEntryEntities) {
        given(gpmPlaylistEntryDao.findByPlaylistId(any())).willAnswer(invocationOnMock -> {
            final GpmPlaylistId playlistId = invocationOnMock.getArgument(0);
            return Stream.of(playlistEntryEntities)
                         .filter(playlistEntryEntity -> playlistId.equals(playlistEntryEntity.playlistId()));
        });
    }
}