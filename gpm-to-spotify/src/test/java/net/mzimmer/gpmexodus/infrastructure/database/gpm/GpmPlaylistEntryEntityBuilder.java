package net.mzimmer.gpmexodus.infrastructure.database.gpm;

import net.mzimmer.gpmexodus.domain.gpm.types.GpmPlaylistEntryPosition;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmPlaylistId;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmStoreId;

public final class GpmPlaylistEntryEntityBuilder {
    private GpmPlaylistId playlistId;
    private GpmPlaylistEntryPosition position;
    private GpmStoreId trackStoreId;

    private GpmPlaylistEntryEntityBuilder() {
    }

    public static GpmPlaylistEntryEntityBuilder aGpmPlaylistEntryEntity() {
        return new GpmPlaylistEntryEntityBuilder();
    }

    public GpmPlaylistEntryEntityBuilder withPlaylistId(final GpmPlaylistId playlistId) {
        this.playlistId = playlistId;
        return this;
    }

    public GpmPlaylistEntryEntityBuilder withPlaylistId(final String playlistId) {
        return withPlaylistId(new GpmPlaylistId(playlistId));
    }

    public GpmPlaylistEntryEntityBuilder withPosition(final GpmPlaylistEntryPosition position) {
        this.position = position;
        return this;
    }

    public GpmPlaylistEntryEntityBuilder withPosition(final int position) {
        return withPosition(new GpmPlaylistEntryPosition(position));
    }

    public GpmPlaylistEntryEntityBuilder withTrackStoreId(final GpmStoreId trackStoreId) {
        this.trackStoreId = trackStoreId;
        return this;
    }

    public GpmPlaylistEntryEntityBuilder withTrackStoreId(final String trackStoreId) {
        return withTrackStoreId(new GpmStoreId(trackStoreId));
    }

    public GpmPlaylistEntryEntity build() {
        return new GpmPlaylistEntryEntity(playlistId,
                                          position,
                                          trackStoreId);
    }
}
