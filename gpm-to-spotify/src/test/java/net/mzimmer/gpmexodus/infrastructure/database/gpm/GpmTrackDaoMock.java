package net.mzimmer.gpmexodus.infrastructure.database.gpm;

import io.vavr.collection.Stream;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmStoreId;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

interface GpmTrackDaoMock {
    static void givenTrackDaoWith(GpmTrackDao trackDao,
                                  GpmTrackEntity... trackEntities) {
        given(trackDao.findByStoreId(any())).willAnswer(invocationOnMock -> {
            final GpmStoreId storeId = invocationOnMock.getArgument(0);
            return Stream.of(trackEntities)
                         .find(trackEntity -> storeId.equals(trackEntity.storeId()));
        });
    }
}