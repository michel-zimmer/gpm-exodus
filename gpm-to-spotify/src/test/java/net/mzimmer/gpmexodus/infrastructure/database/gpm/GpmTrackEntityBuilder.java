package net.mzimmer.gpmexodus.infrastructure.database.gpm;

import net.mzimmer.gpmexodus.domain.gpm.types.GpmAlbumId;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmStoreId;
import net.mzimmer.gpmexodus.domain.types.ArtistName;
import net.mzimmer.gpmexodus.domain.types.TrackName;

public class GpmTrackEntityBuilder {
    private TrackName title;
    private ArtistName artistName;
    private GpmStoreId storeId;
    private GpmAlbumId albumId;

    private GpmTrackEntityBuilder() {
    }

    public static GpmTrackEntityBuilder aGpmTrackEntity() {
        return new GpmTrackEntityBuilder();
    }

    public GpmTrackEntityBuilder withTitle(final TrackName title) {
        this.title = title;
        return this;
    }

    public GpmTrackEntityBuilder withTitle(final String title) {
        return withTitle(new TrackName(title));
    }

    public GpmTrackEntityBuilder withArtistName(final ArtistName artistName) {
        this.artistName = artistName;
        return this;
    }

    public GpmTrackEntityBuilder withArtistName(final String artistName) {
        return withArtistName(new ArtistName(artistName));
    }

    public GpmTrackEntityBuilder withStoreId(final GpmStoreId storeId) {
        this.storeId = storeId;
        return this;
    }

    public GpmTrackEntityBuilder withStoreId(final String storeId) {
        return withStoreId(new GpmStoreId(storeId));
    }

    public GpmTrackEntityBuilder withAlbumId(final GpmAlbumId albumId) {
        this.albumId = albumId;
        return this;
    }

    public GpmTrackEntityBuilder withAlbumId(final String albumId) {
        return withAlbumId(new GpmAlbumId(albumId));
    }

    public GpmTrackEntity build() {
        return new GpmTrackEntity(title,
                                  artistName,
                                  storeId,
                                  albumId);
    }
}
