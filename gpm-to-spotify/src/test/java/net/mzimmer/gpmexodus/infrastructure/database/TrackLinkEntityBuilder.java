package net.mzimmer.gpmexodus.infrastructure.database;

import net.mzimmer.gpmexodus.domain.gpm.types.GpmStoreId;
import net.mzimmer.gpmexodus.domain.spotify.types.SpotifyTrackId;

public class TrackLinkEntityBuilder {
    private GpmStoreId gpmTrackStoreId;
    private SpotifyTrackId spotifyTrackId;

    private TrackLinkEntityBuilder() {
    }

    public static TrackLinkEntityBuilder aTrackLinkEntity() {
        return new TrackLinkEntityBuilder();
    }

    public TrackLinkEntityBuilder withGpmTrackStoreId(final GpmStoreId gpmTrackStoreId) {
        this.gpmTrackStoreId = gpmTrackStoreId;
        return this;
    }

    public TrackLinkEntityBuilder withGpmTrackStoreId(final String gpmTrackStoreId) {
        return withGpmTrackStoreId(new GpmStoreId(gpmTrackStoreId));
    }

    public TrackLinkEntityBuilder withSpotifyTrackId(final SpotifyTrackId spotifyTrackId) {
        this.spotifyTrackId = spotifyTrackId;
        return this;
    }

    public TrackLinkEntityBuilder withSpotifyTrackId(final String spotifyTrackId) {
        return withSpotifyTrackId(new SpotifyTrackId(spotifyTrackId));
    }

    public TrackLinkEntity build() {
        return new TrackLinkEntity(gpmTrackStoreId,
                                   spotifyTrackId);
    }
}