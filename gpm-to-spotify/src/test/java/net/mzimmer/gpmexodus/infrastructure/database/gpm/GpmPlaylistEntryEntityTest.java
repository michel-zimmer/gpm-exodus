package net.mzimmer.gpmexodus.infrastructure.database.gpm;

import net.mzimmer.gpmexodus.domain.gpm.types.GpmAlbumId;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmPlaylistEntryPosition;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmPlaylistId;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmStoreId;
import net.mzimmer.gpmexodus.domain.types.ArtistName;
import net.mzimmer.gpmexodus.domain.types.TrackName;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static net.mzimmer.gpmexodus.infrastructure.database.gpm.GpmTrackDaoMock.givenTrackDaoWith;

@ExtendWith(MockitoExtension.class)
class GpmPlaylistEntryEntityTest {
    @Mock private GpmTrackDao trackDao;

    @Test
    void shouldHaveTrack() {
        // given
        final GpmPlaylistId playlistId = new GpmPlaylistId("playlist");
        final GpmPlaylistEntryPosition position = new GpmPlaylistEntryPosition(1);
        final TrackName trackTitle1 = new TrackName("track-1");
        final TrackName trackTitle2 = new TrackName("track-2");
        final ArtistName artistName = new ArtistName("artist");
        final GpmStoreId storeId1 = new GpmStoreId("track-1");
        final GpmStoreId storeId2 = new GpmStoreId("track-2");
        final GpmAlbumId albumId = new GpmAlbumId("album");

        final GpmPlaylistEntryEntity playlistEntry = new GpmPlaylistEntryEntity(playlistId, position, storeId1);
        final GpmTrackEntity track1 = new GpmTrackEntity(trackTitle1,
                                                         artistName,
                                                         storeId1,
                                                         albumId);
        final GpmTrackEntity track2 = new GpmTrackEntity(trackTitle2,
                                                         artistName,
                                                         storeId2,
                                                         albumId);

        givenTrackDaoWith(trackDao, track1, track2);

        // when
        final GpmTrackEntity result = playlistEntry.track(trackDao);

        // then
        Assertions.assertThat(result).isEqualTo(track1);
    }
}
