package net.mzimmer.gpmexodus.infrastructure.database.gpm;

import io.vavr.collection.Seq;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmAlbumId;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmPlaylistEntryPosition;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmPlaylistId;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmStoreId;
import net.mzimmer.gpmexodus.domain.types.ArtistName;
import net.mzimmer.gpmexodus.domain.types.PlaylistDescription;
import net.mzimmer.gpmexodus.domain.types.PlaylistName;
import net.mzimmer.gpmexodus.domain.types.TrackName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static net.mzimmer.gpmexodus.infrastructure.database.gpm.GpmPlaylistEntryDaoMock.givenPlaylistEntryDaoWith;
import static net.mzimmer.gpmexodus.infrastructure.database.gpm.GpmTrackDaoMock.givenTrackDaoWith;
import static org.assertj.vavr.api.VavrAssertions.assertThat;

@ExtendWith(MockitoExtension.class)
class GpmPlaylistEntityTest {
    @Mock private GpmPlaylistEntryDao playlistEntryDao;
    @Mock private GpmTrackDao trackDao;

    @Test
    void shouldHaveTracks() {
        // given
        final GpmPlaylistId playlistId = new GpmPlaylistId("playlist-id");
        final GpmStoreId storeId1 = new GpmStoreId("track-1");
        final GpmStoreId storeId2 = new GpmStoreId("track-2");
        final GpmStoreId storeId3 = new GpmStoreId("track-3");
        final GpmAlbumId albumId1 = new GpmAlbumId("album-1");
        final GpmAlbumId albumId2 = new GpmAlbumId("album-2");
        final ArtistName artistName = new ArtistName("artist");
        final TrackName trackTitle1 = new TrackName("track-1");
        final TrackName trackTitle2 = new TrackName("track-2");
        final TrackName trackTitle3 = new TrackName("track-3");
        final PlaylistName playlistName = new PlaylistName("playlist");
        final PlaylistDescription playlistDescription = new PlaylistDescription("description");
        final GpmPlaylistEntity playlistEntity = new GpmPlaylistEntity(playlistId,
                                                                       playlistName,
                                                                       playlistDescription);
        final GpmTrackEntity trackEntity1 = new GpmTrackEntity(trackTitle1,
                                                               artistName,
                                                               storeId1,
                                                               albumId2);
        final GpmTrackEntity trackEntity2 = new GpmTrackEntity(trackTitle2,
                                                               artistName,
                                                               storeId2,
                                                               albumId1);
        final GpmTrackEntity trackEntity3 = new GpmTrackEntity(trackTitle3,
                                                               artistName,
                                                               storeId3,
                                                               albumId1);

        final GpmPlaylistEntryEntity entry1 = new GpmPlaylistEntryEntity(playlistId, new GpmPlaylistEntryPosition(1), storeId1);
        final GpmPlaylistEntryEntity entry2 = new GpmPlaylistEntryEntity(playlistId, new GpmPlaylistEntryPosition(2), storeId2);
        final GpmPlaylistEntryEntity entry3 = new GpmPlaylistEntryEntity(playlistId, new GpmPlaylistEntryPosition(3), storeId3);
        final GpmPlaylistEntryEntity entry4 = new GpmPlaylistEntryEntity(playlistId, new GpmPlaylistEntryPosition(4), storeId2);

        givenPlaylistEntryDaoWith(playlistEntryDao,
                                  entry1,
                                  entry2,
                                  entry3,
                                  entry4);
        givenTrackDaoWith(trackDao,
                          trackEntity2,
                          trackEntity1,
                          trackEntity3);

        // when
        final Seq<GpmTrackEntity> result = playlistEntity.tracks(playlistEntryDao, trackDao);

        // then
        assertThat(result).containsExactly(
                trackEntity1,
                trackEntity2,
                trackEntity3,
                trackEntity2
        );
    }
}
