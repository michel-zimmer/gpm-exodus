package net.mzimmer.gpmexodus.infrastructure.gmusicapi;

import io.vavr.collection.Array;
import io.vavr.collection.Seq;
import io.vavr.control.Option;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmAlbumId;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmPlaylistId;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmStoreId;
import net.mzimmer.gpmexodus.domain.types.*;
import net.mzimmer.gpmexodus.test.SerializationTest;
import org.junit.jupiter.api.Test;

import java.io.InputStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.vavr.api.VavrAssertions.assertThat;

class GmusicapiPlaylistsJsonReadModelTest implements SerializationTest {
    @Test
    void shouldHavePlaylists() {
        // given
        final InputStream json = resourceFileAsInputStream("gpm-exodus-playlists.json");

        // when
        final GmusicapiPlaylistJsonReadModel[] result = deserialize(json, GmusicapiPlaylistJsonReadModel[].class);

        // then
        final Array<GmusicapiPlaylistJsonReadModel> playlists = Array.of(result);

        final GpmPlaylistId pureEnergyPlaylistId = new GpmPlaylistId("bfb51762-9bd3-4ffd-89ec-34655fe35658");
        final GmusicapiPlaylistJsonReadModel pureEnergyPlaylist = assertPlaylist(playlists, pureEnergyPlaylistId);
        assertThat(pureEnergyPlaylist.id()).isEqualTo(pureEnergyPlaylistId);
        assertThat(pureEnergyPlaylist.name()).isEqualTo(new PlaylistName("Pure Energy"));
        assertThat(pureEnergyPlaylist.description()).contains(new PlaylistDescription("Maximum goosebumps and energy loaded soundtracks"));

        final GpmStoreId portalsStoreId = new GpmStoreId("Tqlcqki2qzhudejldszspmymxf4");
        final GmusicapiTrackJsonReadModel portalsTrack = assertTrack(pureEnergyPlaylist.tracks(), portalsStoreId);
        assertThat(portalsTrack.title()).isEqualTo(new TrackName("Portals"));
        assertThat(portalsTrack.artist()).isEqualTo(new ArtistName("Alan Silvestri"));
        assertThat(portalsTrack.album()).isEqualTo(new AlbumName("Avengers: Endgame (Original Motion Picture Soundtrack)"));
        assertThat(portalsTrack.storeId()).isEqualTo(portalsStoreId);
        assertThat(portalsTrack.albumId()).isEqualTo(new GpmAlbumId("Byz2vu46pibzmq4dt2noz2utzsy"));


        final GpmPlaylistId werderseePlaylistId = new GpmPlaylistId("1ea034a5-b9cf-43be-96cc-de306efce129");
        final GmusicapiPlaylistJsonReadModel werderseePlaylist = assertPlaylist(playlists, werderseePlaylistId);
        assertThat(werderseePlaylist.id()).isEqualTo(werderseePlaylistId);
        assertThat(werderseePlaylist.name()).isEqualTo(new PlaylistName("Werdersee"));
        assertThat(werderseePlaylist.description()).isEmpty();

        final GpmStoreId undisclosedDesiresStoreId = new GpmStoreId("Traz4r4plgyw6xm46qp7u3lpfby");
        final GmusicapiTrackJsonReadModel undisclosedDesiresTrack = assertTrack(werderseePlaylist.tracks(), undisclosedDesiresStoreId);
        assertThat(undisclosedDesiresTrack.title()).isEqualTo(new TrackName("Undisclosed Desires"));
        assertThat(undisclosedDesiresTrack.artist()).isEqualTo(new ArtistName("Muse"));
        assertThat(undisclosedDesiresTrack.album()).isEqualTo(new AlbumName("The Resistance"));
        assertThat(undisclosedDesiresTrack.storeId()).isEqualTo(undisclosedDesiresStoreId);
        assertThat(undisclosedDesiresTrack.albumId()).isEqualTo(new GpmAlbumId("Bs6v4yncrdzmp3tpqvvs4dlg7ba"));

        final GpmStoreId getLuckyStoreId = new GpmStoreId("Tn5ffsjyzeolxehkzxds5nqkdii");
        final GmusicapiTrackJsonReadModel getLuckyDesiresTrack = assertTrack(werderseePlaylist.tracks(), getLuckyStoreId);
        assertThat(getLuckyDesiresTrack.title()).isEqualTo(new TrackName("Get Lucky (feat. Pharrell Williams & Nile Rodgers)"));
        assertThat(getLuckyDesiresTrack.artist()).isEqualTo(new ArtistName("Daft Punk"));
        assertThat(getLuckyDesiresTrack.album()).isEqualTo(new AlbumName("Random Access Memories"));
        assertThat(getLuckyDesiresTrack.storeId()).isEqualTo(getLuckyStoreId);
        assertThat(getLuckyDesiresTrack.albumId()).isEqualTo(new GpmAlbumId("Bpc2cw3xyf7i3ffifugspb3ufti"));
    }

    private GmusicapiPlaylistJsonReadModel assertPlaylist(final Seq<GmusicapiPlaylistJsonReadModel> playlists,
                                                          final GpmPlaylistId gpmPlaylistId) {
        final Option<GmusicapiPlaylistJsonReadModel> maybePlaylist = playlists.find(playlist -> gpmPlaylistId.equals(playlist.id()));
        assertThat(maybePlaylist).isDefined();
        return maybePlaylist.get();
    }

    private GmusicapiTrackJsonReadModel assertTrack(final Seq<GmusicapiTrackJsonReadModel> tracks,
                                                    final GpmStoreId storeId) {
        final Option<GmusicapiTrackJsonReadModel> maybeTrack = tracks.find(track -> storeId.equals(track.storeId()));
        assertThat(maybeTrack).isDefined();
        return maybeTrack.get();
    }
}
