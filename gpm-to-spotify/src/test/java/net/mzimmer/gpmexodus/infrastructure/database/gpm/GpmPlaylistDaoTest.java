package net.mzimmer.gpmexodus.infrastructure.database.gpm;

import io.vavr.collection.List;
import io.vavr.collection.Seq;
import io.vavr.control.Option;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmPlaylistId;
import net.mzimmer.gpmexodus.domain.types.PlaylistDescription;
import net.mzimmer.gpmexodus.domain.types.PlaylistName;
import net.mzimmer.gpmexodus.lib.database.DataSourceTest;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.vavr.api.VavrAssertions.assertThat;

class GpmPlaylistDaoTest extends DataSourceTest {
    private final GpmPlaylistDao playlistDao = new GpmPlaylistDao(dataSource());

    @Test
    void shouldHaveAllPlaylists() {
        // given
        givenAllMigrations();
        givenSql("""
                         insert into gpm_data_playlist (id, name, description)
                         values
                         ('playlist-1', 'Playlist 1', 'Description of playlist 1'),
                         ('playlist-2', 'Playlist 2', 'Description of playlist 2'),
                         ('playlist-3', 'Playlist 3', 'Description of playlist 3')
                         """);

        // when
        final Seq<GpmPlaylistEntity> result = playlistDao.findAll();

        // then
        assertThat(result).containsExactlyInAnyOrder(
                new GpmPlaylistEntity(new GpmPlaylistId("playlist-1"),
                                      new PlaylistName("Playlist 1"),
                                      new PlaylistDescription("Description of playlist 1")),
                new GpmPlaylistEntity(new GpmPlaylistId("playlist-2"),
                                      new PlaylistName("Playlist 2"),
                                      new PlaylistDescription("Description of playlist 2")),
                new GpmPlaylistEntity(new GpmPlaylistId("playlist-3"),
                                      new PlaylistName("Playlist 3"),
                                      new PlaylistDescription("Description of playlist 3"))
        );
    }

    @Test
    void shouldHavePlaylistById() {
        // given
        givenAllMigrations();
        givenSql("""
                         insert into gpm_data_playlist (id, name, description)
                         values
                         ('playlist-1', 'Playlist 1', 'Description of playlist 1'),
                         ('playlist-2', 'Playlist 2', 'Description of playlist 2'),
                         ('playlist-3', 'Playlist 3', 'Description of playlist 3')
                         """);

        // when
        final Option<GpmPlaylistEntity> result = playlistDao.findById(new GpmPlaylistId("playlist-2"));

        // then
        assertThat(result).contains(new GpmPlaylistEntity(new GpmPlaylistId("playlist-2"),
                                                          new PlaylistName("Playlist 2"),
                                                          new PlaylistDescription("Description of playlist 2")));
    }

    @Test
    void shouldNotHavePlaylistById() {
        // given
        givenAllMigrations();
        givenSql("""
                         insert into gpm_data_playlist (id, name, description)
                         values
                         ('playlist-1', 'Playlist 1', 'Description of playlist 1'),
                         ('playlist-2', 'Playlist 2', 'Description of playlist 2'),
                         ('playlist-3', 'Playlist 3', 'Description of playlist 3')
                         """);

        // when
        final Option<GpmPlaylistEntity> result = playlistDao.findById(new GpmPlaylistId("playlist-4"));

        // then
        assertThat(result).isEmpty();
    }

    @Test
    void shouldInsertOrUpdate() {
        // given
        givenAllMigrations();
        givenSql("""
                         insert into gpm_data_playlist (id, name, description)
                         values
                         ('playlist-1', 'Playlist 1', 'Description of playlist 1'),
                         ('playlist-2', 'Playlist 2', 'Description of playlist 2')
                         """);

        final GpmPlaylistEntity playlist1 = new GpmPlaylistEntity(new GpmPlaylistId("playlist-1"),
                                                                  new PlaylistName("Best of"),
                                                                  new PlaylistDescription("My personal best of"));
        final GpmPlaylistEntity playlist2 = new GpmPlaylistEntity(new GpmPlaylistId("playlist-2"),
                                                                  new PlaylistName("Playlist 2"),
                                                                  new PlaylistDescription("Description of playlist 2"));
        final GpmPlaylistEntity playlist3 = new GpmPlaylistEntity(new GpmPlaylistId("playlist-3"),
                                                                  new PlaylistName("Playlist 3"),
                                                                  new PlaylistDescription(""));
        Seq<GpmPlaylistEntity> playlistChanges = List.of(
                playlist3,
                playlist1
        );

        // when
        final int result = playlistDao.insertOrUpdate(playlistChanges);

        // then
        assertThat(result).isEqualTo(2);
        assertThat(playlistDao.findAll()).containsExactlyInAnyOrder(
                playlist1,
                playlist2,
                playlist3
        );
    }
}
