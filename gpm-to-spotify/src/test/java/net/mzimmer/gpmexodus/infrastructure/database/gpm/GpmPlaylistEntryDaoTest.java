package net.mzimmer.gpmexodus.infrastructure.database.gpm;

import io.vavr.collection.Seq;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmPlaylistEntryPosition;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmPlaylistId;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmStoreId;
import net.mzimmer.gpmexodus.lib.database.DataSourceTest;
import org.junit.jupiter.api.Test;

import static net.mzimmer.gpmexodus.infrastructure.database.gpm.GpmPlaylistEntryEntityBuilder.aGpmPlaylistEntryEntity;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.vavr.api.VavrAssertions.assertThat;

class GpmPlaylistEntryDaoTest extends DataSourceTest {
    private final GpmPlaylistEntryDao playlistEntryDao = new GpmPlaylistEntryDao(dataSource());

    @Test
    void shouldHaveAllPlaylistEntriesByPlaylistId() {
        // given
        givenAllMigrations();
        givenSql("""
                         insert into gpm_data_album (id, name)
                         values ('album', '')""");
        givenSql("""                                                    
                         insert into gpm_data_track (title, artist, store_id, album_id)
                         values ('', '', 'track-1', 'album'),
                                ('', '', 'track-2', 'album')""");
        givenSql("""                                                    
                         insert into gpm_data_playlist (id, name, description)
                         values ('playlist', '', '')""");
        givenSql("""                                                    
                         insert into gpm_data_playlist_entry (playlist_id, position, track_store_id)
                         values ('playlist', 2, 'track-2'),
                                ('playlist', 1, 'track-1'),
                                ('playlist', 3, 'track-1')""");

        final GpmPlaylistId playlistId = new GpmPlaylistId("playlist");

        // when
        final Seq<GpmPlaylistEntryEntity> result = playlistEntryDao.findByPlaylistId(playlistId);

        // then
        assertThat(result).containsExactlyInAnyOrder(
                new GpmPlaylistEntryEntity(playlistId,
                                           new GpmPlaylistEntryPosition(1),
                                           new GpmStoreId("track-1")),
                new GpmPlaylistEntryEntity(playlistId,
                                           new GpmPlaylistEntryPosition(2),
                                           new GpmStoreId("track-2")),
                new GpmPlaylistEntryEntity(playlistId,
                                           new GpmPlaylistEntryPosition(3),
                                           new GpmStoreId("track-1"))
        );
    }

    @Test
    void shouldDeleteByPlaylistId() {
        // given
        givenAllMigrations();
        givenSql("""
                         insert into gpm_data_album (id, name)
                         values ('album', '')""");
        givenSql("""                                                    
                         insert into gpm_data_track (title, artist, store_id, album_id)
                         values ('', '', 'track-1', 'album'),
                                ('', '', 'track-2', 'album')""");
        givenSql("""                                                    
                         insert into gpm_data_playlist (id, name, description)
                         values ('playlist-1', '', ''),
                                ('playlist-2', '', '')""");
        givenSql("""                                                    
                         insert into gpm_data_playlist_entry (playlist_id, position, track_store_id)
                         values ('playlist-1', 2, 'track-2'),
                                ('playlist-1', 1, 'track-1'),
                                ('playlist-2', 1, 'track-1')""");

        // when
        final int result = playlistEntryDao.deleteByPlaylistId(new GpmPlaylistId("playlist-1"));

        // then
        assertThat(result).isEqualTo(2);

        assertThat(playlistEntryDao.findAll()).containsExactlyInAnyOrder(
                aGpmPlaylistEntryEntity().withPlaylistId("playlist-2")
                                         .withPosition(1)
                                         .withTrackStoreId("track-1")
                                         .build()
        );
    }
}
