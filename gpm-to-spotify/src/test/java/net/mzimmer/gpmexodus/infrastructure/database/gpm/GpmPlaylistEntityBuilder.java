package net.mzimmer.gpmexodus.infrastructure.database.gpm;

import net.mzimmer.gpmexodus.domain.gpm.types.GpmPlaylistId;
import net.mzimmer.gpmexodus.domain.types.PlaylistDescription;
import net.mzimmer.gpmexodus.domain.types.PlaylistName;

public final class GpmPlaylistEntityBuilder {
    private GpmPlaylistId id;
    private PlaylistName name;
    private PlaylistDescription description;

    private GpmPlaylistEntityBuilder() {
    }

    public static GpmPlaylistEntityBuilder aGpmPlaylistEntity() {
        return new GpmPlaylistEntityBuilder();
    }

    public GpmPlaylistEntityBuilder withId(final GpmPlaylistId id) {
        this.id = id;
        return this;
    }

    public GpmPlaylistEntityBuilder withId(final String id) {
        return withId(new GpmPlaylistId(id));
    }

    public GpmPlaylistEntityBuilder withName(final PlaylistName name) {
        this.name = name;
        return this;
    }

    public GpmPlaylistEntityBuilder withName(final String name) {
        return withName(new PlaylistName(name));
    }

    public GpmPlaylistEntityBuilder withDescription(final PlaylistDescription description) {
        this.description = description;
        return this;
    }

    public GpmPlaylistEntityBuilder withDescription(final String description) {
        return withDescription(new PlaylistDescription(description));
    }

    public GpmPlaylistEntity build() {
        return new GpmPlaylistEntity(id,
                                     name,
                                     description);
    }
}
