package net.mzimmer.gpmexodus.infrastructure.database.gpm;

import io.vavr.control.Option;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmAlbumId;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmStoreId;
import net.mzimmer.gpmexodus.domain.types.ArtistName;
import net.mzimmer.gpmexodus.domain.types.TrackName;
import net.mzimmer.gpmexodus.lib.database.DataSourceTest;
import org.junit.jupiter.api.Test;

import static org.assertj.vavr.api.VavrAssertions.assertThat;

class GpmTrackDaoTest extends DataSourceTest {
    private final GpmTrackDao trackDao = new GpmTrackDao(dataSource());

    @Test
    void shouldHaveTrackByStoreId() {
        // given
        givenAllMigrations();
        givenSql("""
                         insert into gpm_data_album (id, name)
                         values ('album-id', '')""");
        givenSql("""                                                    
                         insert into gpm_data_track (title, artist, store_id, album_id)
                         values ('title', 'artist', 'store-id', 'album-id')""");

        GpmStoreId storeId = new GpmStoreId("store-id");

        // when
        final Option<GpmTrackEntity> result = trackDao.findByStoreId(storeId);

        // then
        assertThat(result).contains(
                new GpmTrackEntity(new TrackName("title"),
                                   new ArtistName("artist"),
                                   storeId,
                                   new GpmAlbumId("album-id"))
        );
    }
}
