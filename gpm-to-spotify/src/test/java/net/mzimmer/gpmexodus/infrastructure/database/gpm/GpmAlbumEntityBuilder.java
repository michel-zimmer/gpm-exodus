package net.mzimmer.gpmexodus.infrastructure.database.gpm;

import net.mzimmer.gpmexodus.domain.gpm.types.GpmAlbumId;
import net.mzimmer.gpmexodus.domain.types.AlbumName;

public class GpmAlbumEntityBuilder {
    private GpmAlbumId id;
    private AlbumName name;

    private GpmAlbumEntityBuilder() {
    }

    public static GpmAlbumEntityBuilder aGpmAlbumEntity() {
        return new GpmAlbumEntityBuilder();
    }

    public GpmAlbumEntityBuilder withId(final GpmAlbumId id) {
        this.id = id;
        return this;
    }

    public GpmAlbumEntityBuilder withId(final String id) {
        return withId(new GpmAlbumId(id));
    }

    public GpmAlbumEntityBuilder withName(final AlbumName name) {
        this.name = name;
        return this;
    }

    public GpmAlbumEntityBuilder withName(final String name) {
        return withName(new AlbumName(name));
    }

    public GpmAlbumEntity build() {
        return new GpmAlbumEntity(id,
                                  name);
    }
}
