package com.wrapper.spotify;

import com.wrapper.spotify.exceptions.SpotifyWebApiException;
import io.vavr.collection.Array;
import org.apache.hc.core5.http.Header;
import org.apache.hc.core5.http.HttpEntity;
import org.apache.hc.core5.http.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;

public class LoggingHttpManager implements IHttpManager {
    private static final Logger LOG = LoggerFactory.getLogger(LoggingHttpManager.class);

    private final IHttpManager delegate;
    private final Logger log;

    public LoggingHttpManager(final IHttpManager delegate) {
        this(delegate, LOG);
    }

    public LoggingHttpManager(final IHttpManager delegate,
                              final Logger log) {
        this.delegate = delegate;
        this.log = log;
    }

    private void log(String method,
                     URI uri,
                     Header[] requestHeaders,
                     HttpEntity requestBody,
                     String responseBody) {
        log.debug("{} {}\n---\n{}\n---\n{}\n---\n{}",
                  method,
                  uri.toASCIIString(),
                  Array.of(requestHeaders).map(header -> header.getName() + ": " + header.getValue()).mkString("\n"),
                  "",
                  responseBody);
    }

    @Override
    public String get(URI uri,
                      Header[] headers) throws IOException, SpotifyWebApiException, ParseException {
        String response = null;
        try {
            response = delegate.get(uri, headers);
            return response;
        } finally {
            log("GET", uri, headers, null, response);
        }
    }

    @Override
    public String post(URI uri,
                       Header[] headers,
                       HttpEntity body) throws IOException, SpotifyWebApiException, ParseException {
        String response = null;
        try {
            response = delegate.post(uri, headers, body);
            return response;
        } finally {
            log("POST", uri, headers, body, response);
        }
    }

    @Override
    public String put(URI uri,
                      Header[] headers,
                      HttpEntity body) throws IOException, SpotifyWebApiException, ParseException {
        String response = null;
        try {
            response = delegate.put(uri, headers, body);
            return response;
        } finally {
            log("PUT", uri, headers, body, response);
        }
    }

    @Override
    public String delete(URI uri,
                         Header[] headers,
                         HttpEntity body) throws IOException, SpotifyWebApiException, ParseException {
        String response = null;
        try {
            response = delegate.delete(uri, headers, body);
            return response;
        } finally {
            log("DELETE", uri, headers, body, response);
        }
    }
}
