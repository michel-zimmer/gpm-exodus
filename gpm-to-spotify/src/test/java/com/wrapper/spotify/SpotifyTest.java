package com.wrapper.spotify;

public interface SpotifyTest {
    default IHttpManager spotifyHttpManager() {
        return new NotImplementedHttpManager();
    }
}
