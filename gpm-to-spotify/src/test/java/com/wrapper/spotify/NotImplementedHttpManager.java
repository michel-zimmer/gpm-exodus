package com.wrapper.spotify;

import org.apache.hc.core5.http.Header;
import org.apache.hc.core5.http.HttpEntity;

import java.net.URI;

import static io.vavr.API.TODO;

public class NotImplementedHttpManager implements IHttpManager {
    @Override
    public String get(URI uri,
                      Header[] headers) {
        return TODO();
    }

    @Override
    public String post(URI uri,
                       Header[] headers,
                       HttpEntity body) {
        return TODO();
    }

    @Override
    public String put(URI uri,
                      Header[] headers,
                      HttpEntity body) {
        return TODO();
    }

    @Override
    public String delete(URI uri,
                         Header[] headers,
                         HttpEntity body) {
        return TODO();
    }
}
