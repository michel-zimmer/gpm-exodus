package net.mzimmer.gpmexodus.adapter.active;

import io.vavr.collection.List;
import io.vavr.control.Option;
import net.mzimmer.gpmexodus.application.SpotifyWithLoginApplicationService;
import net.mzimmer.gpmexodus.application.model.SynchronizePlaylistCommandPayload;
import net.mzimmer.gpmexodus.application.model.SynchronizePlaylistCommandResult;
import net.mzimmer.gpmexodus.boot.Application;
import net.mzimmer.gpmexodus.boot.Command;
import net.mzimmer.gpmexodus.boot.Config;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmPlaylistId;
import net.mzimmer.gpmexodus.infrastructure.spotify.SpotifyCredentials;
import net.mzimmer.gpmexodus.lib.cli.CommandDefinition;
import net.mzimmer.gpmexodus.lib.database.JdbcUri;
import net.mzimmer.gpmexodus.lib.stdio.Output;

import static net.mzimmer.gpmexodus.adapter.active.Parameters.*;
import static net.mzimmer.gpmexodus.lib.cli.CommandDefinition.commandDefinition;
import static net.mzimmer.gpmexodus.lib.cli.SubCommandDefinition.subCommandDefinition;
import static net.mzimmer.gpmexodus.lib.stdio.Output.stderr;
import static net.mzimmer.gpmexodus.lib.stdio.Output.stdout;

public record SyncPlaylistCommand(JdbcUri database,
                                  SpotifyCredentials spotifyCredentials,
                                  GpmPlaylistId playlistId) implements Command {
    public static final CommandDefinition<SyncPlaylistCommand> DEFINITION =
            commandDefinition(
                    List.of(DATABASE, SPOTIFY_CLIENT_ID, SPOTIFY_CLIENT_SECRET, ACCESS_TOKEN),
                    Option.some(subCommandDefinition("sync-playlist",
                                                     List.of(PLAYLIST))),
                    parameters -> new SyncPlaylistCommand(DATABASE.payload(parameters),
                                                          new SpotifyCredentials(SPOTIFY_CLIENT_ID.payload(parameters),
                                                                                 SPOTIFY_CLIENT_SECRET.payload(parameters),
                                                                                 ACCESS_TOKEN.payload(parameters)),
                                                          PLAYLIST.payload(parameters))
            );

    @Override
    public Config config() {
        return new Config(database, spotifyCredentials);
    }

    @Override
    public Output execute(final Application application) {
        final SpotifyWithLoginApplicationService spotifyWithLoginApplicationService = application.spotifyWithLoginApplicationService();

        final SynchronizePlaylistCommandPayload commandPayload = new SynchronizePlaylistCommandPayload(playlistId);

        final SynchronizePlaylistCommandResult commandResult = spotifyWithLoginApplicationService.synchronizePlaylist(commandPayload);

        return render(commandResult);
    }

    private Output render(final SynchronizePlaylistCommandResult commandResult) {
        if (commandResult instanceof SynchronizePlaylistCommandResult.SynchronizedPlaylist synchronizedPlaylist) {
            return stdout("Synchronized to Spotify Playlist " + synchronizedPlaylist.playlist().id().value());
        } else if (commandResult instanceof SynchronizePlaylistCommandResult.PlaylistNotFound) {
            return stderr("Playlist not found");
        } else {
            throw new IllegalStateException();
        }
    }
}
