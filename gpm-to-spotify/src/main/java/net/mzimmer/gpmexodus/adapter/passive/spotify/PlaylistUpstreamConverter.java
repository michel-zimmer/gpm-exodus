package net.mzimmer.gpmexodus.adapter.passive.spotify;

import com.wrapper.spotify.model_objects.specification.Playlist;
import io.vavr.API;
import net.mzimmer.gpmexodus.domain.spotify.SpotifyPlaylist;
import net.mzimmer.gpmexodus.domain.spotify.types.SpotifyPlaylistId;
import net.mzimmer.gpmexodus.domain.types.PlaylistDescription;
import net.mzimmer.gpmexodus.domain.types.PlaylistName;

public interface PlaylistUpstreamConverter {
    static SpotifyPlaylist toDomain(final Playlist playlist) {
        return new SpotifyPlaylist.LazyTracks(new SpotifyPlaylistId(playlist.getId()),
                                              new PlaylistName(playlist.getName()),
                                              new PlaylistDescription(playlist.getDescription()),
                                              API::TODO);
    }
}
