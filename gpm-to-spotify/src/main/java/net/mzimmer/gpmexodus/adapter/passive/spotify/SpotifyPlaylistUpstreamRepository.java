package net.mzimmer.gpmexodus.adapter.passive.spotify;

import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.exceptions.SpotifyWebApiException;
import com.wrapper.spotify.model_objects.specification.Playlist;
import com.wrapper.spotify.requests.data.playlists.ChangePlaylistsDetailsRequest;
import io.vavr.collection.Array;
import io.vavr.collection.Iterator;
import io.vavr.collection.Seq;
import net.mzimmer.gpmexodus.domain.spotify.SpotifyPlaylist;
import net.mzimmer.gpmexodus.domain.spotify.SpotifyPlaylistRepository;
import net.mzimmer.gpmexodus.domain.spotify.SpotifyTrack;
import net.mzimmer.gpmexodus.domain.spotify.SpotifyTrackUri;
import net.mzimmer.gpmexodus.domain.spotify.types.SpotifyPlaylistId;
import net.mzimmer.gpmexodus.domain.types.PlaylistName;
import org.apache.hc.core5.http.ParseException;

import java.io.IOException;

import static io.vavr.collection.Iterator.narrow;
import static java.util.function.Function.identity;
import static net.mzimmer.gpmexodus.adapter.passive.spotify.PlaylistUpstreamConverter.toDomain;
import static net.mzimmer.gpmexodus.lib.StringUtils.emptyToNone;

public class SpotifyPlaylistUpstreamRepository implements SpotifyPlaylistRepository {
    private final SpotifyApi spotifyApiWithLogin;

    public SpotifyPlaylistUpstreamRepository(final SpotifyApi spotifyApiWithLogin) {
        this.spotifyApiWithLogin = spotifyApiWithLogin;
    }

    @Override
    public SpotifyPlaylist create(final PlaylistName name) {
        try {
            final String userId = spotifyApiWithLogin.getCurrentUsersProfile().build().execute().getId();
            final Playlist playlist = spotifyApiWithLogin.createPlaylist(userId, name.value())
                                                         .public_(false)
                                                         .collaborative(false)
                                                         .build()
                                                         .execute();
            return toDomain(playlist);
        } catch (IOException | ParseException | SpotifyWebApiException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public SpotifyPlaylist spotifyPlaylist(final SpotifyPlaylistId id) {
        try {
            final Playlist playlist = spotifyApiWithLogin.getPlaylist(id.value())
                                                         .build()
                                                         .execute();
            return toDomain(playlist);
        } catch (IOException | ParseException | SpotifyWebApiException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void save(final SpotifyPlaylist playlist) {
        try {
            emptyToNone(playlist.description().value())
                    .toList()
                    .foldLeft(spotifyApiWithLogin.changePlaylistsDetails(playlist.id().value())
                                                 .name(playlist.name().value()),
                              ChangePlaylistsDetailsRequest.Builder::description)
                    .build()
                    .execute();

            final Seq<String> trackUris = playlist.tracks()
                                                  .map(SpotifyTrack::uri)
                                                  .map(SpotifyTrackUri::value);
            final Iterator<Seq<String>> trackUriBatches = narrow(trackUris.grouped(100));
            final String[] firstTrackUriBatch = trackUriBatches.headOption()
                                                               .<Seq<String>>fold(Array::empty, identity())
                                                               .toJavaArray(String[]::new);
            final Iterator<String[]> remainingTrackUriBatches = trackUriBatches.tailOption()
                                                                               .<Iterator<Seq<String>>>fold(Iterator::empty, identity())
                                                                               .map(trackUriBatch -> trackUriBatch.toJavaArray(String[]::new));

            spotifyApiWithLogin.replacePlaylistsItems(playlist.id().value(), firstTrackUriBatch)
                               .build()
                               .execute();
            remainingTrackUriBatches.forEach(trackUriBatch -> {
                try {
                    spotifyApiWithLogin.addItemsToPlaylist(playlist.id().value(), trackUriBatch)
                                       .build()
                                       .execute();
                } catch (IOException | SpotifyWebApiException | ParseException e) {
                    throw new RuntimeException(e);
                }
            });
        } catch (IOException | ParseException | SpotifyWebApiException e) {
            throw new RuntimeException(e);
        }
    }
}
