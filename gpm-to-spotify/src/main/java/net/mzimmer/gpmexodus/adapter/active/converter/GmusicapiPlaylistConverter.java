package net.mzimmer.gpmexodus.adapter.active.converter;

import net.mzimmer.gpmexodus.domain.gpm.GpmPlaylist;
import net.mzimmer.gpmexodus.domain.types.PlaylistDescription;
import net.mzimmer.gpmexodus.infrastructure.gmusicapi.GmusicapiPlaylistJsonReadModel;

public interface GmusicapiPlaylistConverter {
    static GpmPlaylist toDomain(final GmusicapiPlaylistJsonReadModel gpmPlaylistJsonReadModel) {
        return new GpmPlaylist.Immutable(gpmPlaylistJsonReadModel.id(),
                                         gpmPlaylistJsonReadModel.name(),
                                         gpmPlaylistJsonReadModel.description().getOrElse(() -> new PlaylistDescription("")),
                                         gpmPlaylistJsonReadModel.tracks().map(GmusicapiTrackConverter::toDomain));
    }
}
