package net.mzimmer.gpmexodus.adapter.passive.gpm.converter;

import io.vavr.Lazy;
import io.vavr.collection.Seq;
import net.mzimmer.gpmexodus.domain.gpm.GpmPlaylist;
import net.mzimmer.gpmexodus.domain.gpm.GpmTrack;
import net.mzimmer.gpmexodus.infrastructure.database.gpm.GpmAlbumDao;
import net.mzimmer.gpmexodus.infrastructure.database.gpm.GpmPlaylistEntity;
import net.mzimmer.gpmexodus.infrastructure.database.gpm.GpmPlaylistEntryDao;
import net.mzimmer.gpmexodus.infrastructure.database.gpm.GpmTrackDao;

import java.util.function.Function;

public interface PlaylistEntityConverter {
    static GpmPlaylistEntity toEntity(final GpmPlaylist playlist) {
        return new GpmPlaylistEntity(playlist.id(),
                                     playlist.name(),
                                     playlist.description());
    }

    static GpmPlaylist toDomain(final GpmPlaylistEntity playlistEntity,
                                final GpmPlaylistEntryDao playlistEntryDao,
                                final GpmTrackDao trackDao,
                                final GpmAlbumDao albumDao) {
        final Lazy<Seq<GpmTrack>> tracks = Lazy.of(
                () -> playlistEntity.tracks(playlistEntryDao, trackDao)
                                    .map(TrackEntityConverter.toDomain(albumDao))
        );
        return new GpmPlaylist.LazyTracks(playlistEntity.id(),
                                          playlistEntity.name(),
                                          playlistEntity.description(),
                                          tracks);
    }

    static Function<GpmPlaylistEntity, GpmPlaylist> toDomain(final GpmPlaylistEntryDao playlistEntryDao,
                                                             final GpmTrackDao trackDao,
                                                             final GpmAlbumDao albumDao) {
        return playlistEntity -> toDomain(playlistEntity,
                                          playlistEntryDao,
                                          trackDao,
                                          albumDao);
    }
}
