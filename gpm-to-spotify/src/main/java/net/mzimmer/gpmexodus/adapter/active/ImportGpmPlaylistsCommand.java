package net.mzimmer.gpmexodus.adapter.active;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.vavr.collection.Array;
import io.vavr.collection.List;
import io.vavr.collection.Seq;
import io.vavr.control.Option;
import net.mzimmer.gpmexodus.adapter.active.converter.GmusicapiPlaylistConverter;
import net.mzimmer.gpmexodus.application.ImportApplicationService;
import net.mzimmer.gpmexodus.application.model.ImportGpmPlaylistsCommandPayload;
import net.mzimmer.gpmexodus.application.model.ImportGpmPlaylistsCommandResult;
import net.mzimmer.gpmexodus.boot.Application;
import net.mzimmer.gpmexodus.boot.Command;
import net.mzimmer.gpmexodus.boot.Config;
import net.mzimmer.gpmexodus.domain.gpm.GpmPlaylist;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmPlaylistsFilePath;
import net.mzimmer.gpmexodus.infrastructure.gmusicapi.GmusicapiPlaylistJsonReadModel;
import net.mzimmer.gpmexodus.lib.cli.CommandDefinition;
import net.mzimmer.gpmexodus.lib.database.JdbcUri;
import net.mzimmer.gpmexodus.lib.filesystem.FileReader;
import net.mzimmer.gpmexodus.lib.stdio.Output;

import java.io.IOException;
import java.io.InputStream;

import static net.mzimmer.gpmexodus.adapter.active.Parameters.DATABASE;
import static net.mzimmer.gpmexodus.adapter.active.Parameters.PLAYLISTS;
import static net.mzimmer.gpmexodus.lib.cli.CommandDefinition.commandDefinition;
import static net.mzimmer.gpmexodus.lib.cli.SubCommandDefinition.subCommandDefinition;
import static net.mzimmer.gpmexodus.lib.stdio.Output.stdout;

public record ImportGpmPlaylistsCommand(JdbcUri database,
                                        GpmPlaylistsFilePath playlists) implements Command {
    public static final CommandDefinition<ImportGpmPlaylistsCommand> DEFINITION =
            commandDefinition(
                    List.of(DATABASE),
                    Option.of(subCommandDefinition("import-gpm-playlists",
                                                   List.of(PLAYLISTS))),
                    parameters -> new ImportGpmPlaylistsCommand(DATABASE.payload(parameters),
                                                                PLAYLISTS.payload(parameters))
            );

    @Override
    public Config config() {
        return new Config(database);
    }

    @Override
    public Output execute(final Application application) {
        final ImportApplicationService importApplicationService = application.importApplicationService();
        final FileReader fileReader = application.fileReader();
        final ObjectMapper objectMapper = application.objectMapper();

        final InputStream inputStream = fileReader.read(playlists.value());
        final Seq<GpmPlaylist> playlistsToImport = parsePlaylists(objectMapper, inputStream)
                .map(GmusicapiPlaylistConverter::toDomain);

        final ImportGpmPlaylistsCommandPayload commandPayload = new ImportGpmPlaylistsCommandPayload(playlistsToImport);

        final ImportGpmPlaylistsCommandResult commandResult =
                importApplicationService.importGpmPlaylistsFile(commandPayload);

        return render(commandResult);
    }

    private Array<GmusicapiPlaylistJsonReadModel> parsePlaylists(final ObjectMapper objectMapper,
                                                                 final InputStream inputStream) {
        try {
            return Array.of(objectMapper.readValue(inputStream, GmusicapiPlaylistJsonReadModel[].class));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Output render(ImportGpmPlaylistsCommandResult commandResult) {
        return stdout("""   
                              Number of changed playlists:        %d
                              Number of changed tracks:           %d
                              Number of changed albums:           %d
                              Number of changed playlist entries: %d"""
                              .formatted(commandResult.numberOfChangedPlaylists().value(),
                                         commandResult.numberOfChangedTracks().value(),
                                         commandResult.numberOfChangedAlbums().value(),
                                         commandResult.numberOfChangedPlaylistEntries().value()));
    }
}
