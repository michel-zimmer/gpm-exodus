package net.mzimmer.gpmexodus.adapter.passive.gpm.converter;

import io.vavr.Lazy;
import net.mzimmer.gpmexodus.domain.gpm.GpmAlbum;
import net.mzimmer.gpmexodus.domain.gpm.GpmTrack;
import net.mzimmer.gpmexodus.infrastructure.database.gpm.GpmAlbumDao;
import net.mzimmer.gpmexodus.infrastructure.database.gpm.GpmTrackEntity;

import java.util.function.Function;

public interface TrackEntityConverter {
    static GpmTrackEntity toEntity(final GpmTrack track) {
        return new GpmTrackEntity(track.title(),
                                  track.artistName(),
                                  track.storeId(),
                                  track.album().id());
    }

    static GpmTrack toDomain(final GpmTrackEntity trackEntity,
                             final GpmAlbumDao albumDao) {
        final Lazy<GpmAlbum> album = Lazy.of(
                () -> albumDao.findByPrimaryKey(trackEntity.albumId())
                              .map(AlbumEntityConverter::toDomain)
                              .get()
        );
        return new GpmTrack.LazyAlbum(trackEntity.title(),
                                      trackEntity.artistName(),
                                      album,
                                      trackEntity.storeId());
    }

    static Function<GpmTrackEntity, GpmTrack> toDomain(GpmAlbumDao albumDao) {
        return trackEntity -> toDomain(trackEntity,
                                       albumDao);
    }
}
