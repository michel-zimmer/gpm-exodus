package net.mzimmer.gpmexodus.adapter.active.converter;

import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.Tuple4;
import net.mzimmer.gpmexodus.application.model.LinkProgressCommandResult;
import net.mzimmer.gpmexodus.domain.gpm.GpmPlaylist;
import net.mzimmer.gpmexodus.domain.spotify.types.SpotifyPlaylistId;
import net.mzimmer.gpmexodus.domain.types.LinkProgress;
import net.mzimmer.gpmexodus.domain.types.LinkProgressSummary;
import net.mzimmer.gpmexodus.domain.types.OverallLinkProgress;
import net.mzimmer.gpmexodus.domain.types.PlaylistLinkProgress;
import net.mzimmer.gpmexodus.lib.TableRenderer;
import net.mzimmer.gpmexodus.lib.stdio.Output;

import static net.mzimmer.gpmexodus.lib.stdio.Output.stdout;

public class LinkProgressCommandResultConverter {
    private static final Tuple4<String, String, String, String> TABLE_HEADER =
            Tuple.of("GPM ID", "Name", "Spotify ID", "Progress");

    private final TableRenderer tableRenderer;

    public LinkProgressCommandResultConverter(final TableRenderer tableRenderer) {
        this.tableRenderer = tableRenderer;
    }

    public Output render(final LinkProgressCommandResult commandResult) {
        final LinkProgressSummary linkProgressSummary = commandResult.linkProgressSummary();
        final OverallLinkProgress overallLinkProgress = linkProgressSummary.overallProgress();
        final String playlistProgressTable = tableRenderer.renderAsTable(TABLE_HEADER,
                                                                         linkProgressSummary.playlistProgress().map(this::render));
        return stdout("""
                              Overall progress: %s
                                                            
                              %s"""
                              .formatted(render(overallLinkProgress),
                                         playlistProgressTable));
    }

    private Tuple4<String, String, String, String> render(Tuple2<GpmPlaylist, PlaylistLinkProgress> playlistWithLinkProgress) {
        final GpmPlaylist playlist = playlistWithLinkProgress._1();
        final PlaylistLinkProgress linkProgress = playlistWithLinkProgress._2();
        return Tuple.of(playlist.id().value(),
                        playlist.name().value(),
                        linkProgress.maybeSpotifyPlaylistId().fold(() -> "", SpotifyPlaylistId::value),
                        render(linkProgress));
    }

    private String render(LinkProgress linkProgress) {
        return "%d / %d".formatted(linkProgress.numberOfUniqueLinkedTracks().value(),
                                   linkProgress.numberOfUniqueTracks().value());
    }
}
