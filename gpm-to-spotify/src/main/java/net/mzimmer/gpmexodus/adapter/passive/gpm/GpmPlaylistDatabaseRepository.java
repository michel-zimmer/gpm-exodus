package net.mzimmer.gpmexodus.adapter.passive.gpm;

import io.vavr.collection.Seq;
import io.vavr.control.Option;
import net.mzimmer.gpmexodus.adapter.passive.gpm.converter.AlbumEntityConverter;
import net.mzimmer.gpmexodus.adapter.passive.gpm.converter.PlaylistEntityConverter;
import net.mzimmer.gpmexodus.adapter.passive.gpm.converter.TrackEntityConverter;
import net.mzimmer.gpmexodus.domain.gpm.GpmAlbum;
import net.mzimmer.gpmexodus.domain.gpm.GpmPlaylist;
import net.mzimmer.gpmexodus.domain.gpm.GpmPlaylistRepository;
import net.mzimmer.gpmexodus.domain.gpm.GpmTrack;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmPlaylistId;
import net.mzimmer.gpmexodus.domain.types.NumberOfChangedAlbums;
import net.mzimmer.gpmexodus.domain.types.NumberOfChangedPlaylistEntries;
import net.mzimmer.gpmexodus.domain.types.NumberOfChangedPlaylists;
import net.mzimmer.gpmexodus.domain.types.NumberOfChangedTracks;
import net.mzimmer.gpmexodus.infrastructure.database.gpm.*;

import static net.mzimmer.gpmexodus.adapter.passive.gpm.converter.PlaylistEntityConverter.toDomain;
import static net.mzimmer.gpmexodus.lib.java.lang.Iterables.singleFromDuplicates;

public class GpmPlaylistDatabaseRepository implements GpmPlaylistRepository {
    private final GpmAlbumDao albumDao;
    private final GpmTrackDao trackDao;
    private final GpmPlaylistDao playlistDao;
    private final GpmPlaylistEntryDao playlistEntryDao;

    public GpmPlaylistDatabaseRepository(final GpmAlbumDao albumDao,
                                         final GpmTrackDao trackDao,
                                         final GpmPlaylistDao playlistDao,
                                         final GpmPlaylistEntryDao playlistEntryDao) {
        this.albumDao = albumDao;
        this.trackDao = trackDao;
        this.playlistDao = playlistDao;
        this.playlistEntryDao = playlistEntryDao;
    }

    @Override
    public SaveResults save(Seq<GpmPlaylist> playlists) {
        final Seq<GpmTrack> tracks = playlists.flatMap(GpmPlaylist::tracks)
                                              .groupBy(GpmTrack::storeId)
                                              .mapValues(singleFromDuplicates(GpmTrack::title,
                                                                              GpmTrack::artistName,
                                                                              track -> track.album().id()))
                                              .values();
        final Seq<GpmAlbum> albums = tracks.map(GpmTrack::album)
                                           .groupBy(GpmAlbum::id)
                                           .mapValues(singleFromDuplicates(GpmAlbum::name))
                                           .values();
        final Seq<GpmPlaylistEntryEntity> entries = playlists.flatMap(playlist -> playlist.positionsToTrack()
                                                                                          .map(positionWithTrack -> new GpmPlaylistEntryEntity(playlist.id(),
                                                                                                                                               positionWithTrack._1(),
                                                                                                                                               positionWithTrack._2().storeId())));

        final NumberOfChangedAlbums numberOfChangedAlbums =
                new NumberOfChangedAlbums(albumDao.insertOrUpdate(albums.map(AlbumEntityConverter::toEntity)));

        final NumberOfChangedTracks numberOfChangedTracks =
                new NumberOfChangedTracks(trackDao.insertOrUpdate(tracks.map(TrackEntityConverter::toEntity)));

        final NumberOfChangedPlaylists numberOfChangedPlaylists =
                new NumberOfChangedPlaylists(playlistDao.insertOrUpdate(playlists.map(PlaylistEntityConverter::toEntity)));

        final NumberOfChangedPlaylistEntries numberOfChangedPlaylistEntries =
                new NumberOfChangedPlaylistEntries(
                        entries.groupBy(GpmPlaylistEntryEntity::playlistId)
                               .map(playlistIdWithEntries ->
                                            playlistEntryDao.deleteByPlaylistId(playlistIdWithEntries._1()) +
                                            playlistEntryDao.insertOrUpdate(playlistIdWithEntries._2()))
                               .fold(0, Integer::sum)
                );

        return new SaveResults(numberOfChangedAlbums,
                               numberOfChangedTracks,
                               numberOfChangedPlaylists,
                               numberOfChangedPlaylistEntries);
    }

    @Override
    public Seq<GpmPlaylist> playlists() {
        return playlistDao.findAll()
                          .map(toDomain(playlistEntryDao, trackDao, albumDao));
    }

    @Override
    public Option<GpmPlaylist> playlist(GpmPlaylistId id) {
        return playlistDao.findById(id)
                          .map(toDomain(playlistEntryDao, trackDao, albumDao));
    }
}
