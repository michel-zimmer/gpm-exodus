package net.mzimmer.gpmexodus.adapter.passive;

import io.vavr.control.Option;
import net.mzimmer.gpmexodus.domain.PlaylistLink;
import net.mzimmer.gpmexodus.domain.PlaylistLinkRepository;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmPlaylistId;
import net.mzimmer.gpmexodus.infrastructure.database.PlaylistLinkDao;
import net.mzimmer.gpmexodus.infrastructure.database.PlaylistLinkEntity;

public class PlaylistLinkDaoRepository implements PlaylistLinkRepository {
    private final PlaylistLinkDao playlistLinkDao;

    public PlaylistLinkDaoRepository(final PlaylistLinkDao playlistLinkDao) {
        this.playlistLinkDao = playlistLinkDao;
    }

    @Override
    public Option<PlaylistLink> playlistLink(final GpmPlaylistId gpmPlaylistId) {
        return playlistLinkDao.findByGpmPlaylistId(gpmPlaylistId).map(this::toDomain);
    }

    @Override
    public void save(final PlaylistLink playlistLink) {
        playlistLinkDao.insertOrUpdate(toEntity(playlistLink));
    }

    private PlaylistLinkEntity toEntity(final PlaylistLink playlistLink) {
        return new PlaylistLinkEntity(playlistLink.gpmPlaylistId(),
                                      playlistLink.spotifyPlaylistId());
    }

    private PlaylistLink toDomain(final PlaylistLinkEntity entity) {
        return new PlaylistLink(entity.gpmPlaylistId(),
                                entity.spotifyPlaylistId());
    }
}
