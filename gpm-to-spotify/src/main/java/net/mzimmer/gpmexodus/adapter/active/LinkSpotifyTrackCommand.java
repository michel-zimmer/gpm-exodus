package net.mzimmer.gpmexodus.adapter.active;

import io.vavr.collection.List;
import io.vavr.control.Option;
import net.mzimmer.gpmexodus.application.ManageApplicationService;
import net.mzimmer.gpmexodus.application.model.LinkSpotifyTrackCommandPayload;
import net.mzimmer.gpmexodus.application.model.LinkSpotifyTrackCommandResult;
import net.mzimmer.gpmexodus.boot.Application;
import net.mzimmer.gpmexodus.boot.Command;
import net.mzimmer.gpmexodus.boot.Config;
import net.mzimmer.gpmexodus.domain.TrackLink;
import net.mzimmer.gpmexodus.lib.cli.CommandDefinition;
import net.mzimmer.gpmexodus.lib.database.JdbcUri;
import net.mzimmer.gpmexodus.lib.stdio.Output;

import static net.mzimmer.gpmexodus.adapter.active.Parameters.*;
import static net.mzimmer.gpmexodus.lib.cli.CommandDefinition.commandDefinition;
import static net.mzimmer.gpmexodus.lib.cli.SubCommandDefinition.subCommandDefinition;
import static net.mzimmer.gpmexodus.lib.stdio.Output.stderr;
import static net.mzimmer.gpmexodus.lib.stdio.Output.stdout;

public record LinkSpotifyTrackCommand(JdbcUri database,
                                      TrackLink trackLink) implements Command {
    public static final CommandDefinition<LinkSpotifyTrackCommand> DEFINITION =
            commandDefinition(
                    List.of(DATABASE),
                    Option.of(subCommandDefinition("link-spotify-track",
                                                   List.of(GPM_TRACK_ID, SPOTIFY_TRACK_ID))),
                    parameters -> new LinkSpotifyTrackCommand(DATABASE.payload(parameters),
                                                              new TrackLink(GPM_TRACK_ID.payload(parameters),
                                                                            SPOTIFY_TRACK_ID.payload(parameters)))
            );

    @Override
    public Config config() {
        return new Config(database);
    }

    @Override
    public Output execute(Application application) {
        final ManageApplicationService manageApplicationService = application.manageApplicationService();

        final LinkSpotifyTrackCommandPayload commandPayload = new LinkSpotifyTrackCommandPayload(trackLink);

        final LinkSpotifyTrackCommandResult commandResult = manageApplicationService.linkSpotifyTrack(commandPayload);

        return render(commandResult);
    }

    private Output render(final LinkSpotifyTrackCommandResult commandResult) {
        if (commandResult instanceof LinkSpotifyTrackCommandResult.NewlyLinked newlyLinked) {
            return stdout("Linked GPM %s to Spotify %s".formatted(newlyLinked.trackLink().gpmTrackStoreId().value(),
                                                                  newlyLinked.trackLink().spotifyTrackId().value()));
        } else if (commandResult instanceof LinkSpotifyTrackCommandResult.AlreadyLinked alreadyLinked) {
            return stderr("GPM %s is already linked to Spotify %s".formatted(alreadyLinked.trackLink().gpmTrackStoreId().value(),
                                                                             alreadyLinked.trackLink().spotifyTrackId().value()));
        } else {
            throw new IllegalStateException();
        }
    }
}
