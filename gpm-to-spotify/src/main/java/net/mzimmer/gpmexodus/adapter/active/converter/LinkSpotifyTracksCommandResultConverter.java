package net.mzimmer.gpmexodus.adapter.active.converter;

import io.vavr.Tuple;
import io.vavr.Tuple4;
import io.vavr.collection.Map;
import io.vavr.collection.Seq;
import net.mzimmer.gpmexodus.application.model.LinkSpotifyTracksCommandResult;
import net.mzimmer.gpmexodus.domain.gpm.GpmTrack;
import net.mzimmer.gpmexodus.domain.spotify.SpotifyTrack;
import net.mzimmer.gpmexodus.lib.TableRenderer;
import net.mzimmer.gpmexodus.lib.stdio.Output;

import static net.mzimmer.gpmexodus.lib.stdio.Output.stderr;
import static net.mzimmer.gpmexodus.lib.stdio.Output.stdout;

public class LinkSpotifyTracksCommandResultConverter {
    private static final Tuple4<String, String, String, String> MATCHES_TABLE_HEADER =
            Tuple.of("Spotify ID", "Artist", "Title", "Album");

    private final TableRenderer tableRenderer;

    public LinkSpotifyTracksCommandResultConverter(final TableRenderer tableRenderer) {
        this.tableRenderer = tableRenderer;
    }

    public Output render(final LinkSpotifyTracksCommandResult commandResult) {
        if (commandResult instanceof LinkSpotifyTracksCommandResult.PlaylistFound playlistFound) {
            return stdout(render(playlistFound));
        } else if (commandResult instanceof LinkSpotifyTracksCommandResult.PlaylistNotFound) {
            return stderr("Playlist not found");
        } else {
            throw new IllegalStateException();
        }
    }

    private String render(final LinkSpotifyTracksCommandResult.PlaylistFound playlistFound) {
        return """
                Already linked tracks: %d
                Newly linked tracks:   %d
                Not linked tracks:     %d%s"""
                .formatted(playlistFound.alreadyLinkedTracks().size(),
                           playlistFound.newlyLinkedTracks().size(),
                           playlistFound.notLinkedTracks().size(),
                           render(playlistFound.notLinkedTracks()));
    }

    private String render(final Map<GpmTrack, Seq<SpotifyTrack>> notLinkedTracks) {
        return notLinkedTracks.map(notLinkedTrack -> notLinkedTrack.apply(this::render)).mkString();
    }

    private String render(final GpmTrack gpmTrack,
                          final Seq<SpotifyTrack> matches) {
        return """
                                
                                
                [%s] %s: %s (%s)
                %s"""
                .formatted(gpmTrack.storeId().value(),
                           gpmTrack.artistName().value(),
                           gpmTrack.title().value(),
                           gpmTrack.album().name().value(),
                           render(matches));
    }

    private String render(final Seq<SpotifyTrack> matches) {
        return tableRenderer.renderAsTable(MATCHES_TABLE_HEADER,
                                           matches.map(this::render));
    }

    private Tuple4<String, String, String, String> render(final SpotifyTrack spotifyTrack) {
        return Tuple.of(spotifyTrack.id().value(),
                        spotifyTrack.artistName().value(),
                        spotifyTrack.title().value(),
                        spotifyTrack.albumName().value());
    }
}
