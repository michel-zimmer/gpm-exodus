package net.mzimmer.gpmexodus.adapter.passive.gpm;

import io.vavr.collection.Seq;
import net.mzimmer.gpmexodus.adapter.passive.gpm.converter.TrackEntityConverter;
import net.mzimmer.gpmexodus.domain.gpm.GpmTrack;
import net.mzimmer.gpmexodus.domain.gpm.GpmTrackRepository;
import net.mzimmer.gpmexodus.infrastructure.database.gpm.GpmAlbumDao;
import net.mzimmer.gpmexodus.infrastructure.database.gpm.GpmTrackDao;

public class GpmTrackDatabaseRepository implements GpmTrackRepository {
    private final GpmTrackDao trackDao;
    private final GpmAlbumDao albumDao;

    public GpmTrackDatabaseRepository(final GpmTrackDao trackDao,
                                      final GpmAlbumDao albumDao) {
        this.trackDao = trackDao;
        this.albumDao = albumDao;
    }

    @Override
    public Seq<GpmTrack> tracks() {
        return trackDao.findAll().map(TrackEntityConverter.toDomain(albumDao));
    }
}
