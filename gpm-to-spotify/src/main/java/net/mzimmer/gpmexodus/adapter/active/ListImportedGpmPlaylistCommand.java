package net.mzimmer.gpmexodus.adapter.active;

import io.vavr.collection.List;
import io.vavr.control.Option;
import net.mzimmer.gpmexodus.adapter.active.converter.ListImportedGpmPlaylistCommandResultConverter;
import net.mzimmer.gpmexodus.application.ManageApplicationService;
import net.mzimmer.gpmexodus.application.model.ListImportedGpmPlaylistCommandPayload;
import net.mzimmer.gpmexodus.application.model.ListImportedGpmPlaylistCommandResult;
import net.mzimmer.gpmexodus.boot.Application;
import net.mzimmer.gpmexodus.boot.Command;
import net.mzimmer.gpmexodus.boot.Config;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmPlaylistId;
import net.mzimmer.gpmexodus.lib.TableRenderer;
import net.mzimmer.gpmexodus.lib.cli.CommandDefinition;
import net.mzimmer.gpmexodus.lib.database.JdbcUri;
import net.mzimmer.gpmexodus.lib.stdio.Output;

import static net.mzimmer.gpmexodus.adapter.active.Parameters.DATABASE;
import static net.mzimmer.gpmexodus.adapter.active.Parameters.PLAYLIST;
import static net.mzimmer.gpmexodus.lib.cli.CommandDefinition.commandDefinition;
import static net.mzimmer.gpmexodus.lib.cli.SubCommandDefinition.subCommandDefinition;

public record ListImportedGpmPlaylistCommand(GpmPlaylistId playlist,
                                             JdbcUri database) implements Command {
    public static final CommandDefinition<ListImportedGpmPlaylistCommand> DEFINITION =
            commandDefinition(
                    List.of(DATABASE),
                    Option.of(subCommandDefinition("list-imported-gpm-playlist",
                                                   List.of(PLAYLIST))),
                    parameters -> new ListImportedGpmPlaylistCommand(PLAYLIST.payload(parameters),
                                                                     DATABASE.payload(parameters))
            );

    @Override
    public Config config() {
        return new Config(database);
    }

    @Override
    public Output execute(final Application application) {
        final ManageApplicationService manageApplicationService = application.manageApplicationService();
        final TableRenderer tableRenderer = application.tableRenderer();
        final ListImportedGpmPlaylistCommandResultConverter resultConverter = new ListImportedGpmPlaylistCommandResultConverter(tableRenderer);

        final ListImportedGpmPlaylistCommandPayload commandPayload = new ListImportedGpmPlaylistCommandPayload(playlist);

        final ListImportedGpmPlaylistCommandResult commandResult = manageApplicationService.listImportedGpmPlaylist(commandPayload);

        return resultConverter.render(commandResult);
    }
}
