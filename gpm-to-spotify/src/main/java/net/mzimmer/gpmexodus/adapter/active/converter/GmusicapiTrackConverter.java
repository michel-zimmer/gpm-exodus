package net.mzimmer.gpmexodus.adapter.active.converter;

import net.mzimmer.gpmexodus.domain.gpm.GpmAlbum;
import net.mzimmer.gpmexodus.domain.gpm.GpmTrack;
import net.mzimmer.gpmexodus.infrastructure.gmusicapi.GmusicapiTrackJsonReadModel;

public interface GmusicapiTrackConverter {
    static GpmTrack toDomain(final GmusicapiTrackJsonReadModel gmusicapiTrackJsonReadModel) {
        final GpmAlbum.Immutable album = new GpmAlbum.Immutable(gmusicapiTrackJsonReadModel.albumId(),
                                                                gmusicapiTrackJsonReadModel.album());
        return new GpmTrack.Immutable(gmusicapiTrackJsonReadModel.title(),
                                      gmusicapiTrackJsonReadModel.artist(),
                                      album,
                                      gmusicapiTrackJsonReadModel.storeId());
    }
}
