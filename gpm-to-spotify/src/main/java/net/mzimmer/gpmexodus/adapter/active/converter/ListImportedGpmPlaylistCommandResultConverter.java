package net.mzimmer.gpmexodus.adapter.active.converter;

import io.vavr.Tuple;
import io.vavr.Tuple5;
import io.vavr.collection.Map;
import io.vavr.collection.Seq;
import io.vavr.control.Option;
import net.mzimmer.gpmexodus.application.model.ListImportedGpmPlaylistCommandResult;
import net.mzimmer.gpmexodus.domain.gpm.GpmPlaylist;
import net.mzimmer.gpmexodus.domain.gpm.GpmTrack;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmStoreId;
import net.mzimmer.gpmexodus.domain.spotify.types.SpotifyTrackId;
import net.mzimmer.gpmexodus.lib.TableRenderer;
import net.mzimmer.gpmexodus.lib.stdio.Output;

import static net.mzimmer.gpmexodus.lib.stdio.Output.stderr;
import static net.mzimmer.gpmexodus.lib.stdio.Output.stdout;

public class ListImportedGpmPlaylistCommandResultConverter {
    private static final Tuple5<String, String, String, String, String> OUTPUT_TABLE_HEADER =
            Tuple.of("Title", "Artist", "Album", "Store ID", "Spotify ID");

    private final TableRenderer tableRenderer;

    public ListImportedGpmPlaylistCommandResultConverter(final TableRenderer tableRenderer) {
        this.tableRenderer = tableRenderer;
    }

    public Output render(final ListImportedGpmPlaylistCommandResult commandResult) {
        return commandResult.maybePlaylist().fold(
                () -> stderr("Playlist not found"),
                gpmPlaylist -> stdout(render(gpmPlaylist, commandResult.links()))
        );
    }

    private String render(final GpmPlaylist gpmPlaylist,
                          final Map<GpmStoreId, SpotifyTrackId> links) {
        final String head = render(gpmPlaylist);
        final Seq<Tuple5<String, String, String, String, String>> rows =
                gpmPlaylist.tracks().map(track -> render(track, links.get(track.storeId())));
        final String table = tableRenderer.renderAsTable(OUTPUT_TABLE_HEADER, rows);
        return head + "\n" + table;
    }

    private String render(final GpmPlaylist playlist) {
        return """
                Name: %s
                Description: %s
                ID: %s
                """
                .formatted(playlist.name().value(),
                           playlist.description().value(),
                           playlist.id().value());
    }

    private Tuple5<String, String, String, String, String> render(final GpmTrack gpmTrack,
                                                                  final Option<SpotifyTrackId> maybeSpotifyTrackId) {
        return Tuple.of(gpmTrack.title().value(),
                        gpmTrack.artistName().value(),
                        gpmTrack.album().name().value(),
                        gpmTrack.storeId().value(),
                        maybeSpotifyTrackId.fold(() -> "",
                                                 SpotifyTrackId::value));
    }
}
