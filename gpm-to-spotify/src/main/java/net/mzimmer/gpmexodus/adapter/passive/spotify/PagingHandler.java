package net.mzimmer.gpmexodus.adapter.passive.spotify;

import com.wrapper.spotify.model_objects.specification.Paging;
import io.vavr.collection.Array;
import io.vavr.collection.Seq;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PagingHandler {
    private static final Logger LOG = LoggerFactory.getLogger(PagingHandler.class);

    public <T> Seq<T> allPages(final Paging<T> paging) {
        if (paging.getNext() != null) {
            LOG.warn("More than one page for " + paging.getHref());
        }
        return Array.of(paging.getItems());
    }
}
