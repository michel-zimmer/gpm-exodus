package net.mzimmer.gpmexodus.adapter.active;

import io.vavr.Function1;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmPlaylistId;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmPlaylistsFilePath;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmStoreId;
import net.mzimmer.gpmexodus.domain.spotify.types.SpotifyTrackId;
import net.mzimmer.gpmexodus.infrastructure.spotify.AccessToken;
import net.mzimmer.gpmexodus.infrastructure.spotify.ClientId;
import net.mzimmer.gpmexodus.infrastructure.spotify.ClientSecret;
import net.mzimmer.gpmexodus.lib.cli.ParameterDefinition;
import net.mzimmer.gpmexodus.lib.database.JdbcUri;

import java.nio.file.Path;

import static net.mzimmer.gpmexodus.lib.cli.ParameterDefinition.parameterDefinition;

public interface Parameters {
    ParameterDefinition<GpmPlaylistsFilePath> PLAYLISTS =
            parameterDefinition("playlists", Function1.of(GpmPlaylistsFilePath::new).compose(Path::of));

    ParameterDefinition<JdbcUri> DATABASE =
            parameterDefinition("database", JdbcUri::new);

    ParameterDefinition<GpmPlaylistId> PLAYLIST =
            parameterDefinition("playlist", GpmPlaylistId::new);

    ParameterDefinition<ClientId> SPOTIFY_CLIENT_ID =
            parameterDefinition("spotify-client-id", ClientId::new);

    ParameterDefinition<ClientSecret> SPOTIFY_CLIENT_SECRET =
            parameterDefinition("spotify-client-secret", ClientSecret::new);

    ParameterDefinition<AccessToken> ACCESS_TOKEN =
            parameterDefinition("access-token", AccessToken::new);

    ParameterDefinition<GpmStoreId> GPM_TRACK_ID =
            parameterDefinition("gpm-track-id", GpmStoreId::new);

    ParameterDefinition<SpotifyTrackId> SPOTIFY_TRACK_ID =
            parameterDefinition("spotify-track-id", SpotifyTrackId::new);
}
