package net.mzimmer.gpmexodus.adapter.active;

import io.vavr.collection.List;
import io.vavr.control.Option;
import net.mzimmer.gpmexodus.adapter.active.converter.LinkProgressCommandResultConverter;
import net.mzimmer.gpmexodus.application.ManageApplicationService;
import net.mzimmer.gpmexodus.application.model.LinkProgressCommandResult;
import net.mzimmer.gpmexodus.boot.Application;
import net.mzimmer.gpmexodus.boot.Command;
import net.mzimmer.gpmexodus.boot.Config;
import net.mzimmer.gpmexodus.lib.TableRenderer;
import net.mzimmer.gpmexodus.lib.cli.CommandDefinition;
import net.mzimmer.gpmexodus.lib.database.JdbcUri;
import net.mzimmer.gpmexodus.lib.stdio.Output;

import static net.mzimmer.gpmexodus.adapter.active.Parameters.DATABASE;
import static net.mzimmer.gpmexodus.lib.cli.CommandDefinition.commandDefinition;
import static net.mzimmer.gpmexodus.lib.cli.SubCommandDefinition.subCommandDefinition;

public record LinkProgressCommand(JdbcUri database) implements Command {
    public static final CommandDefinition<LinkProgressCommand> DEFINITION = commandDefinition(
            List.of(DATABASE),
            Option.of(subCommandDefinition("link-progress")),
            parameters -> new LinkProgressCommand(DATABASE.payload(parameters))
    );

    @Override
    public Config config() {
        return new Config(database);
    }

    @Override
    public Output execute(final Application application) {
        final ManageApplicationService manageApplicationService = application.manageApplicationService();
        final TableRenderer tableRenderer = application.tableRenderer();
        final LinkProgressCommandResultConverter resultConverter = new LinkProgressCommandResultConverter(tableRenderer);

        final LinkProgressCommandResult commandResult = manageApplicationService.linkProgress();

        return resultConverter.render(commandResult);
    }
}
