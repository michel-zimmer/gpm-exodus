package net.mzimmer.gpmexodus.adapter.active.converter;

import io.vavr.Tuple;
import io.vavr.Tuple4;
import net.mzimmer.gpmexodus.application.model.ListImportedGpmPlaylistsCommandResult;
import net.mzimmer.gpmexodus.domain.gpm.GpmPlaylist;
import net.mzimmer.gpmexodus.lib.TableRenderer;
import net.mzimmer.gpmexodus.lib.stdio.Output;

import static net.mzimmer.gpmexodus.lib.stdio.Output.stdout;

public class ListImportedGpmPlaylistsCommandResultConverter {
    private static final Tuple4<String, String, String, String> OUTPUT_TABLE_HEADER =
            Tuple.of("id", "number of tracks", "name", "description");

    private final TableRenderer tableRenderer;

    public ListImportedGpmPlaylistsCommandResultConverter(final TableRenderer tableRenderer) {
        this.tableRenderer = tableRenderer;
    }

    public Output render(final ListImportedGpmPlaylistsCommandResult commandResult) {
        return stdout(tableRenderer.renderAsTable(OUTPUT_TABLE_HEADER,
                                                  commandResult.playlists()
                                                               .map(this::render)));
    }

    private Tuple4<String, String, String, String> render(final GpmPlaylist playlist) {
        return Tuple.of(playlist.id().value(),
                        String.valueOf(playlist.tracks().size()),
                        playlist.name().value(),
                        playlist.description().value());
    }
}
