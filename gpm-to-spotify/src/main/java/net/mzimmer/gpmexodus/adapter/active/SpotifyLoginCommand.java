package net.mzimmer.gpmexodus.adapter.active;

import com.wrapper.spotify.exceptions.SpotifyWebApiException;
import com.wrapper.spotify.model_objects.credentials.AuthorizationCodeCredentials;
import io.vavr.collection.List;
import io.vavr.control.Option;
import net.mzimmer.gpmexodus.boot.Application;
import net.mzimmer.gpmexodus.boot.Command;
import net.mzimmer.gpmexodus.boot.Config;
import net.mzimmer.gpmexodus.infrastructure.callbackserver.CallbackServer;
import net.mzimmer.gpmexodus.infrastructure.callbackserver.Result;
import net.mzimmer.gpmexodus.infrastructure.spotify.SpotifyCredentials;
import net.mzimmer.gpmexodus.lib.cli.CommandDefinition;
import net.mzimmer.gpmexodus.lib.stdio.Output;
import net.mzimmer.gpmexodus.lib.stdio.Stdio;
import org.apache.hc.core5.http.ParseException;

import java.io.IOException;
import java.net.URI;

import static net.mzimmer.gpmexodus.adapter.active.Parameters.SPOTIFY_CLIENT_ID;
import static net.mzimmer.gpmexodus.adapter.active.Parameters.SPOTIFY_CLIENT_SECRET;
import static net.mzimmer.gpmexodus.lib.cli.CommandDefinition.commandDefinition;
import static net.mzimmer.gpmexodus.lib.cli.SubCommandDefinition.subCommandDefinition;
import static net.mzimmer.gpmexodus.lib.stdio.Output.stderr;
import static net.mzimmer.gpmexodus.lib.stdio.Output.stdout;

public record SpotifyLoginCommand(SpotifyCredentials credentials) implements Command {
    public static final CommandDefinition<SpotifyLoginCommand> DEFINITION =
            commandDefinition(
                    List.empty(),
                    Option.of(subCommandDefinition("spotify-login",
                                                   List.of(SPOTIFY_CLIENT_ID, SPOTIFY_CLIENT_SECRET))),
                    parameters -> new SpotifyLoginCommand(new SpotifyCredentials(SPOTIFY_CLIENT_ID.payload(parameters),
                                                                                 SPOTIFY_CLIENT_SECRET.payload(parameters)))
            );

    @Override
    public Config config() {
        return new Config(credentials);
    }

    @Override
    public Output execute(final Application application) {
        final Stdio stdio = application.stdio();
        final CallbackServer callbackServer = application.callbackServer();

        callbackServer.start();

        final URI loginUri = application.spotifyApiBuilder()
                                        .build()
                                        .authorizationCodeUri()
                                        .scope("playlist-modify-public,playlist-modify-private")
                                        .build()
                                        .execute();
        stdout("Please login via " + loginUri.toASCIIString()).println(stdio);

        final Result result = callbackServer.result();
        callbackServer.stop();

        if (result instanceof Result.Success success) {
            try {
                final AuthorizationCodeCredentials authorizationCodeCredentials =
                        application.spotifyApiBuilder()
                                   .build()
                                   .authorizationCode(success.code().value())
                                   .build()
                                   .execute();
                return stdout("""
                                      Access token:  %s
                                      Refresh token: %s"""
                                      .formatted(authorizationCodeCredentials.getAccessToken(),
                                                 authorizationCodeCredentials.getRefreshToken()));
            } catch (IOException | SpotifyWebApiException | ParseException e) {
                throw new RuntimeException(e);
            }
        } else if (result instanceof Result.Error error) {
            return stderr(error.message());
        } else {
            throw new IllegalStateException();
        }
    }
}
