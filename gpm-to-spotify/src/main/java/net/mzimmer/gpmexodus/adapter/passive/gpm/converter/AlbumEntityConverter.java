package net.mzimmer.gpmexodus.adapter.passive.gpm.converter;

import net.mzimmer.gpmexodus.domain.gpm.GpmAlbum;
import net.mzimmer.gpmexodus.infrastructure.database.gpm.GpmAlbumEntity;

public interface AlbumEntityConverter {
    static GpmAlbumEntity toEntity(final GpmAlbum album) {
        return new GpmAlbumEntity(album.id(),
                                  album.name());
    }

    static GpmAlbum toDomain(final GpmAlbumEntity entity) {
        return new GpmAlbum.Immutable(entity.id(),
                                      entity.albumName());
    }
}
