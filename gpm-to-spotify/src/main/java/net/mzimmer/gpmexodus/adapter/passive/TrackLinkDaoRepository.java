package net.mzimmer.gpmexodus.adapter.passive;

import io.vavr.collection.Map;
import io.vavr.collection.Seq;
import io.vavr.control.Option;
import net.mzimmer.gpmexodus.domain.TrackLink;
import net.mzimmer.gpmexodus.domain.TrackLinkRepository;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmStoreId;
import net.mzimmer.gpmexodus.domain.spotify.types.SpotifyTrackId;
import net.mzimmer.gpmexodus.infrastructure.database.TrackLinkDao;
import net.mzimmer.gpmexodus.infrastructure.database.TrackLinkEntity;

public class TrackLinkDaoRepository implements TrackLinkRepository {
    private final TrackLinkDao trackLinkDao;

    public TrackLinkDaoRepository(final TrackLinkDao trackLinkDao) {
        this.trackLinkDao = trackLinkDao;
    }

    @Override
    public Map<GpmStoreId, SpotifyTrackId> trackLinks(final Seq<GpmStoreId> gpmStoreIds) {
        return trackLinkDao.findByGpmStoreIds(gpmStoreIds);
    }

    @Override
    public Option<TrackLink> trackLink(GpmStoreId gpmStoreId) {
        return trackLinkDao.findByGpmStoreId(gpmStoreId).map(this::toDomain);
    }

    @Override
    public void save(final TrackLink trackLink) {
        trackLinkDao.insert(toEntity(trackLink));
    }

    private TrackLinkEntity toEntity(final TrackLink trackLink) {
        return new TrackLinkEntity(trackLink.gpmTrackStoreId(),
                                   trackLink.spotifyTrackId());
    }

    private TrackLink toDomain(final TrackLinkEntity entity) {
        return new TrackLink(entity.gpmTrackStoreId(),
                             entity.spotifyTrackId());
    }
}
