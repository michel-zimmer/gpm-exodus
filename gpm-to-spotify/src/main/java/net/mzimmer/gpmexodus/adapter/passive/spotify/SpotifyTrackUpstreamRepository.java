package net.mzimmer.gpmexodus.adapter.passive.spotify;

import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.exceptions.SpotifyWebApiException;
import com.wrapper.spotify.model_objects.specification.ArtistSimplified;
import com.wrapper.spotify.model_objects.specification.Track;
import io.vavr.collection.Array;
import io.vavr.collection.Seq;
import net.mzimmer.gpmexodus.domain.spotify.SpotifyTrack;
import net.mzimmer.gpmexodus.domain.spotify.SpotifyTrackRepository;
import net.mzimmer.gpmexodus.domain.spotify.types.SpotifyTrackId;
import net.mzimmer.gpmexodus.domain.types.AlbumName;
import net.mzimmer.gpmexodus.domain.types.ArtistName;
import net.mzimmer.gpmexodus.domain.types.TrackName;
import org.apache.hc.core5.http.ParseException;

import java.io.IOException;

public class SpotifyTrackUpstreamRepository implements SpotifyTrackRepository {
    private final SpotifyApi spotifyApi;
    private final PagingHandler pagingHandler;

    public SpotifyTrackUpstreamRepository(final SpotifyApi spotifyApi,
                                          final PagingHandler pagingHandler) {
        this.spotifyApi = spotifyApi;
        this.pagingHandler = pagingHandler;
    }

    @Override
    public Seq<SpotifyTrack> tracks(final TrackName trackName,
                                    final ArtistName artistName,
                                    final AlbumName albumName) {
        final String query = "track:%s album:%s artist:%s"
                .formatted(trackName.value(),
                           albumName.value(),
                           artistName.value());
        try {
            return pagingHandler.allPages(spotifyApi.searchTracks(query)
                                                    .build()
                                                    .execute())
                                .map(this::toDomain);
        } catch (IOException | SpotifyWebApiException | ParseException e) {
            throw new RuntimeException(e);
        }
    }

    private SpotifyTrack toDomain(Track track) {
        return new SpotifyTrack.Immutable(new SpotifyTrackId(track.getId()),
                                          new TrackName(track.getName()),
                                          new ArtistName(Array.of(track.getArtists()).map(ArtistSimplified::getName).mkString(", ")),
                                          new AlbumName(track.getAlbum().getName()));
    }
}
