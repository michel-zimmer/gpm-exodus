package net.mzimmer.gpmexodus.adapter.active;

import io.vavr.collection.List;
import io.vavr.control.Option;
import net.mzimmer.gpmexodus.adapter.active.converter.ListImportedGpmPlaylistsCommandResultConverter;
import net.mzimmer.gpmexodus.application.ManageApplicationService;
import net.mzimmer.gpmexodus.application.model.ListImportedGpmPlaylistsCommandResult;
import net.mzimmer.gpmexodus.boot.Application;
import net.mzimmer.gpmexodus.boot.Command;
import net.mzimmer.gpmexodus.boot.Config;
import net.mzimmer.gpmexodus.lib.TableRenderer;
import net.mzimmer.gpmexodus.lib.cli.CommandDefinition;
import net.mzimmer.gpmexodus.lib.database.JdbcUri;
import net.mzimmer.gpmexodus.lib.stdio.Output;

import static net.mzimmer.gpmexodus.adapter.active.Parameters.DATABASE;
import static net.mzimmer.gpmexodus.lib.cli.CommandDefinition.commandDefinition;
import static net.mzimmer.gpmexodus.lib.cli.SubCommandDefinition.subCommandDefinition;

public record ListImportedGpmPlaylistsCommand(JdbcUri database) implements Command {
    public static final CommandDefinition<ListImportedGpmPlaylistsCommand> DEFINITION = commandDefinition(
            List.of(DATABASE),
            Option.of(subCommandDefinition("list-imported-gpm-playlists")),
            parameters -> new ListImportedGpmPlaylistsCommand(DATABASE.payload(parameters))
    );

    @Override
    public Config config() {
        return new Config(database);
    }

    @Override
    public Output execute(final Application application) {
        final TableRenderer tableRenderer = application.tableRenderer();
        final ManageApplicationService manageApplicationService = application.manageApplicationService();
        final ListImportedGpmPlaylistsCommandResultConverter resultConverter = new ListImportedGpmPlaylistsCommandResultConverter(tableRenderer);

        final ListImportedGpmPlaylistsCommandResult commandResult = manageApplicationService.listImportedGpmPlaylists();

        return resultConverter.render(commandResult);
    }
}
