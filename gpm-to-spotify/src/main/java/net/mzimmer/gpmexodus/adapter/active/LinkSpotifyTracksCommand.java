package net.mzimmer.gpmexodus.adapter.active;

import io.vavr.collection.List;
import io.vavr.control.Option;
import net.mzimmer.gpmexodus.adapter.active.converter.LinkSpotifyTracksCommandResultConverter;
import net.mzimmer.gpmexodus.application.SpotifyApplicationService;
import net.mzimmer.gpmexodus.application.model.LinkSpotifyTracksCommandPayload;
import net.mzimmer.gpmexodus.application.model.LinkSpotifyTracksCommandResult;
import net.mzimmer.gpmexodus.boot.Application;
import net.mzimmer.gpmexodus.boot.Command;
import net.mzimmer.gpmexodus.boot.Config;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmPlaylistId;
import net.mzimmer.gpmexodus.infrastructure.spotify.SpotifyCredentials;
import net.mzimmer.gpmexodus.lib.TableRenderer;
import net.mzimmer.gpmexodus.lib.cli.CommandDefinition;
import net.mzimmer.gpmexodus.lib.database.JdbcUri;
import net.mzimmer.gpmexodus.lib.stdio.Output;

import static net.mzimmer.gpmexodus.adapter.active.Parameters.*;
import static net.mzimmer.gpmexodus.lib.cli.CommandDefinition.commandDefinition;
import static net.mzimmer.gpmexodus.lib.cli.SubCommandDefinition.subCommandDefinition;

public record LinkSpotifyTracksCommand(GpmPlaylistId playlist,
                                       JdbcUri database,
                                       SpotifyCredentials spotifyCredentials) implements Command {
    public static final CommandDefinition<LinkSpotifyTracksCommand> DEFINITION =
            commandDefinition(
                    List.of(DATABASE,
                            SPOTIFY_CLIENT_ID,
                            SPOTIFY_CLIENT_SECRET),
                    Option.of(subCommandDefinition("link-spotify-tracks",
                                                   List.of(PLAYLIST))),
                    parameters -> new LinkSpotifyTracksCommand(PLAYLIST.payload(parameters),
                                                               DATABASE.payload(parameters),
                                                               new SpotifyCredentials(SPOTIFY_CLIENT_ID.payload(parameters),
                                                                                      SPOTIFY_CLIENT_SECRET.payload(parameters)))
            );

    @Override
    public Config config() {
        return new Config(database, spotifyCredentials);
    }

    @Override
    public Output execute(Application application) {
        final SpotifyApplicationService spotifyApplicationService = application.spotifyApplicationService();
        final TableRenderer tableRenderer = application.tableRenderer();
        final LinkSpotifyTracksCommandResultConverter resultConverter = new LinkSpotifyTracksCommandResultConverter(tableRenderer);

        final LinkSpotifyTracksCommandPayload commandPayload = new LinkSpotifyTracksCommandPayload(playlist);

        final LinkSpotifyTracksCommandResult commandResult = spotifyApplicationService.linkSpotifyTracks(commandPayload);

        return resultConverter.render(commandResult);
    }
}
