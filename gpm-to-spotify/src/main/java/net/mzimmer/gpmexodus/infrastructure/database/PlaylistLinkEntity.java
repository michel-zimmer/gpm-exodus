package net.mzimmer.gpmexodus.infrastructure.database;

import net.mzimmer.gpmexodus.domain.gpm.types.GpmPlaylistId;
import net.mzimmer.gpmexodus.domain.spotify.types.SpotifyPlaylistId;
import net.mzimmer.gpmexodus.lib.database.Entity;

public record PlaylistLinkEntity(GpmPlaylistId gpmPlaylistId,
                                 SpotifyPlaylistId spotifyPlaylistId)
        implements Entity<GpmPlaylistId> {

    @Override
    public GpmPlaylistId primaryKey() {
        return gpmPlaylistId;
    }
}
