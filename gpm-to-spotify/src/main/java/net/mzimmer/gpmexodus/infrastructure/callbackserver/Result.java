package net.mzimmer.gpmexodus.infrastructure.callbackserver;

public interface Result {
    final record Success(Code code) implements Result {
    }

    final record Error(String message) implements Result {
    }
}
