package net.mzimmer.gpmexodus.infrastructure.spotify;

public record AuthorizationCode(String value) {
}
