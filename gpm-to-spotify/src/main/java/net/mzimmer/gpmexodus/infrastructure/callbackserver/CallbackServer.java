package net.mzimmer.gpmexodus.infrastructure.callbackserver;

import io.vavr.collection.List;
import io.vavr.control.Option;
import org.apache.hc.core5.http.ClassicHttpRequest;
import org.apache.hc.core5.http.ClassicHttpResponse;
import org.apache.hc.core5.http.NameValuePair;
import org.apache.hc.core5.http.impl.bootstrap.HttpServer;
import org.apache.hc.core5.http.io.HttpRequestHandler;
import org.apache.hc.core5.http.io.entity.StringEntity;
import org.apache.hc.core5.http.protocol.HttpContext;
import org.apache.hc.core5.net.URLEncodedUtils;

import java.io.IOException;
import java.net.URI;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;

import static java.lang.Runtime.getRuntime;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.apache.hc.core5.http.ContentType.TEXT_HTML;
import static org.apache.hc.core5.http.impl.bootstrap.ServerBootstrap.bootstrap;
import static org.apache.hc.core5.io.CloseMode.GRACEFUL;

public class CallbackServer implements HttpRequestHandler, AutoCloseable {
    private static final int DEFAULT_PORT = 51234;
    public static final CallbackServer DEFAULT = new CallbackServer(DEFAULT_PORT);
    private static final String CALLBACK_PATH = "/callback";

    private final HttpServer httpServer;
    private final URI redirectUri;
    private final CountDownLatch latch = new CountDownLatch(1);
    private final AtomicReference<Result> resultStore = new AtomicReference<>();

    public CallbackServer(int port) {
        redirectUri = URI.create("http://localhost:" + port + CALLBACK_PATH);
        httpServer = bootstrap().setListenerPort(port)
                                .register("*", this)
                                .create();
    }

    public URI redirectUri() {
        return redirectUri;
    }


    public void start() {
        getRuntime().addShutdownHook(new Thread(CallbackServer.this::close));
        try {
            httpServer.start();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void stop() {
        close();
    }

    private void setResult(final Result result) {
        resultStore.set(result);
        latch.countDown();
    }

    public Result result() {
        try {
            latch.await();
            return resultStore.get();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void handle(final ClassicHttpRequest request,
                       final ClassicHttpResponse response,
                       final HttpContext context) {
        final Function<String, Result> errorBuilder =
                details -> new Result.Error("Could not get authorization code. Callback was %s %s. %s"
                                                    .formatted(request.getMethod(),
                                                               request.getPath(),
                                                               details));
        final Result result;

        if (!"GET".equals(request.getMethod())) {
            result = errorBuilder.apply("Not a GET request");
        } else if (!request.getPath().startsWith(CALLBACK_PATH + "?")) {
            result = errorBuilder.apply("Not a request to callback path");
        } else if (request.getEntity() != null) {
            result = errorBuilder.apply("Not an empty request");
        } else {
            final List<NameValuePair> queryParameters = List.ofAll(URLEncodedUtils.parse(URI.create(request.getPath()).getQuery(), UTF_8));
            final Option<NameValuePair> maybeCodeQueryParameter = queryParameters.find(queryParameter -> "code".equals(queryParameter.getName()));

            result = maybeCodeQueryParameter.fold(
                    () -> errorBuilder.apply("Parameter code is missing"),
                    codeQueryParameter -> new Result.Success(new Code(codeQueryParameter.getValue()))
            );
        }

        if (result instanceof Result.Success) {
            response.setCode(200);
            response.setEntity(new StringEntity("""
                                                        <!doctype html>
                                                        <html>
                                                        <body>
                                                        <p>success, window can be closed</p>
                                                        </body>
                                                        </html>
                                                        """,
                                                TEXT_HTML));
        } else if (result instanceof Result.Error error) {
            response.setCode(500);
            response.setEntity(new StringEntity("""
                                                        <!doctype html>
                                                        <html>
                                                        <body>
                                                        <p>error, window can be closed</p>
                                                        <p>%s</p>
                                                        </body>
                                                        </html>
                                                        """.formatted(error.message()),
                                                TEXT_HTML));
        } else {
            throw new IllegalStateException();
        }

        setResult(result);
    }

    @Override
    public void close() {
        httpServer.close(GRACEFUL);
    }
}
