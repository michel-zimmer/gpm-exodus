package net.mzimmer.gpmexodus.infrastructure.database.gpm;

import io.vavr.collection.Array;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmAlbumId;
import net.mzimmer.gpmexodus.domain.types.AlbumName;
import net.mzimmer.gpmexodus.lib.database.*;

import javax.sql.DataSource;

import static net.mzimmer.gpmexodus.lib.database.ColumnsBuilder.columns;

public class GpmAlbumDao
        extends AbstractDao<GpmAlbumEntity, GpmAlbumId>
        implements Dao<GpmAlbumEntity, GpmAlbumId> {

    public static final TableName TABLE = new TableName("gpm_data_album");
    public static final ColumnName ID = new ColumnName("id");
    public static final ColumnName NAME = new ColumnName("name");
    public static final Columns<GpmAlbumEntity, GpmAlbumId> COLUMNS =
            columns(GpmAlbumEntity.class, GpmAlbumId.class)
                    .withPrimaryColumnNames(ID)
                    .withNonPrimaryColumnNames(NAME)
                    .withPrimaryKeyParameters(primaryKey -> Array.of(new Parameter.Text(primaryKey.value())))
                    .withNonPrimaryKeyParameters(entity -> Array.of(new Parameter.Text(entity.albumName().value())))
                    .withReadEntity(resultSet -> new GpmAlbumEntity(new GpmAlbumId(resultSet.getString(ID.value())),
                                                                    new AlbumName(resultSet.getString(NAME.value()))))
                    .build();

    public GpmAlbumDao(final DataSource dataSource) {
        super(dataSource, TABLE, COLUMNS);
    }
}
