package net.mzimmer.gpmexodus.infrastructure.gmusicapi;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.vavr.collection.Array;
import io.vavr.collection.Seq;
import io.vavr.control.Option;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmPlaylistId;
import net.mzimmer.gpmexodus.domain.types.PlaylistDescription;
import net.mzimmer.gpmexodus.domain.types.PlaylistName;

import static net.mzimmer.gpmexodus.lib.StringUtils.emptyToNone;

@JsonIgnoreProperties(ignoreUnknown = true)
public record GmusicapiPlaylistJsonReadModel(GpmPlaylistId id,
                                             PlaylistName name,
                                             Option<PlaylistDescription> description,
                                             Seq<GmusicapiTrackJsonReadModel> tracks) {

    @JsonCreator
    static GmusicapiPlaylistJsonReadModel jsonCreator(@JsonProperty("id") String id,
                                                      @JsonProperty("name") String name,
                                                      @JsonProperty("description") String description,
                                                      @JsonProperty("tracks") GmusicapiPlaylistEntryJsonReadModel[] entries) {
        return new GmusicapiPlaylistJsonReadModel(new GpmPlaylistId(id),
                                                  new PlaylistName(name),
                                                  emptyToNone(description).map(PlaylistDescription::new),
                                                  Array.of(entries).flatMap(GmusicapiPlaylistEntryJsonReadModel::track));
    }
}
