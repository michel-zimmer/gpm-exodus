package net.mzimmer.gpmexodus.infrastructure.database.gpm;

import io.vavr.collection.Array;
import io.vavr.control.Option;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmAlbumId;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmStoreId;
import net.mzimmer.gpmexodus.domain.types.ArtistName;
import net.mzimmer.gpmexodus.domain.types.TrackName;
import net.mzimmer.gpmexodus.lib.database.*;

import javax.sql.DataSource;

import static net.mzimmer.gpmexodus.lib.database.ColumnsBuilder.columns;

public class GpmTrackDao
        extends AbstractDao<GpmTrackEntity, GpmStoreId>
        implements Dao<GpmTrackEntity, GpmStoreId> {

    public static final TableName TABLE = new TableName("gpm_data_track");
    public static final ColumnName STORE_ID = new ColumnName("store_id");
    public static final ColumnName TITLE = new ColumnName("title");
    public static final ColumnName ARTIST = new ColumnName("artist");
    public static final ColumnName ALBUM_ID = new ColumnName("album_id");
    public static final Columns<GpmTrackEntity, GpmStoreId> COLUMNS =
            columns(GpmTrackEntity.class, GpmStoreId.class)
                    .withPrimaryColumnNames(STORE_ID)
                    .withNonPrimaryColumnNames(TITLE, ARTIST, ALBUM_ID)
                    .withPrimaryKeyParameters(primaryKey -> Array.of(new Parameter.Text(primaryKey.value())))
                    .withNonPrimaryKeyParameters(entity -> Array.of(new Parameter.Text(entity.title().value()),
                                                                    new Parameter.Text(entity.artistName().value()),
                                                                    new Parameter.Text(entity.albumId().value())))
                    .withReadEntity(resultSet -> new GpmTrackEntity(new TrackName(resultSet.getString(TITLE.value())),
                                                                    new ArtistName(resultSet.getString(ARTIST.value())),
                                                                    new GpmStoreId(resultSet.getString(STORE_ID.value())),
                                                                    new GpmAlbumId(resultSet.getString(ALBUM_ID.value()))))
                    .build();

    public GpmTrackDao(final DataSource dataSource) {
        super(dataSource, TABLE, COLUMNS);
    }

    public Option<GpmTrackEntity> findByStoreId(GpmStoreId storeId) {
        return findByPrimaryKey(storeId);
    }
}
