package net.mzimmer.gpmexodus.infrastructure.database.gpm;

import io.vavr.collection.Array;
import io.vavr.control.Option;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmPlaylistId;
import net.mzimmer.gpmexodus.domain.types.PlaylistDescription;
import net.mzimmer.gpmexodus.domain.types.PlaylistName;
import net.mzimmer.gpmexodus.lib.database.*;

import javax.sql.DataSource;

import static net.mzimmer.gpmexodus.lib.database.ColumnsBuilder.columns;

public class GpmPlaylistDao
        extends AbstractDao<GpmPlaylistEntity, GpmPlaylistId>
        implements Dao<GpmPlaylistEntity, GpmPlaylistId> {

    public static final TableName TABLE = new TableName("gpm_data_playlist");
    public static final ColumnName ID = new ColumnName("id");
    public static final ColumnName NAME = new ColumnName("name");
    public static final ColumnName DESCRIPTION = new ColumnName("description");
    public static final Columns<GpmPlaylistEntity, GpmPlaylistId> COLUMNS =
            columns(GpmPlaylistEntity.class, GpmPlaylistId.class)
                    .withPrimaryColumnNames(ID)
                    .withNonPrimaryColumnNames(NAME, DESCRIPTION)
                    .withPrimaryKeyParameters(primaryKey -> Array.of(new Parameter.Text(primaryKey.value())))
                    .withNonPrimaryKeyParameters(entity -> Array.of(new Parameter.Text(entity.name().value()),
                                                                    new Parameter.Text(entity.description().value())))
                    .withReadEntity(resultSet -> new GpmPlaylistEntity(new GpmPlaylistId(resultSet.getString(ID.value())),
                                                                       new PlaylistName(resultSet.getString(NAME.value())),
                                                                       new PlaylistDescription(resultSet.getString(DESCRIPTION.value()))))
                    .build();

    public GpmPlaylistDao(final DataSource dataSource) {
        super(dataSource, TABLE, COLUMNS);
    }

    public Option<GpmPlaylistEntity> findById(GpmPlaylistId id) {
        return findByPrimaryKey(id);
    }
}
