package net.mzimmer.gpmexodus.infrastructure.database;

import net.mzimmer.gpmexodus.domain.gpm.types.GpmStoreId;
import net.mzimmer.gpmexodus.domain.spotify.types.SpotifyTrackId;
import net.mzimmer.gpmexodus.lib.database.Entity;

public record TrackLinkEntity(GpmStoreId gpmTrackStoreId,
                              SpotifyTrackId spotifyTrackId)
        implements Entity<GpmStoreId> {

    @Override
    public GpmStoreId primaryKey() {
        return gpmTrackStoreId;
    }
}
