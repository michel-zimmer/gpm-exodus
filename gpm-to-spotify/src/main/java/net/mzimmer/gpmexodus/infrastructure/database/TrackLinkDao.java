package net.mzimmer.gpmexodus.infrastructure.database;

import io.vavr.collection.Array;
import io.vavr.collection.Map;
import io.vavr.collection.Seq;
import io.vavr.control.Option;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmStoreId;
import net.mzimmer.gpmexodus.domain.spotify.types.SpotifyTrackId;
import net.mzimmer.gpmexodus.lib.database.*;

import javax.sql.DataSource;

import static net.mzimmer.gpmexodus.lib.database.ColumnsBuilder.columns;

public class TrackLinkDao
        extends AbstractDao<TrackLinkEntity, GpmStoreId>
        implements Dao<TrackLinkEntity, GpmStoreId> {

    public static final TableName TABLE = new TableName("gpm_data_track_to_spotify");
    public static final ColumnName GPM_TRACK_STORE_ID = new ColumnName("gpm_data_track_store_id");
    public static final ColumnName SPOTIFY_TRACK_ID = new ColumnName("spotify_track_id");

    public static final Columns<TrackLinkEntity, GpmStoreId> COLUMNS =
            columns(TrackLinkEntity.class, GpmStoreId.class)
                    .withPrimaryColumnNames(GPM_TRACK_STORE_ID)
                    .withNonPrimaryColumnNames(SPOTIFY_TRACK_ID)
                    .withPrimaryKeyParameters(primaryKey -> Array.of(new Parameter.Text(primaryKey.value())))
                    .withNonPrimaryKeyParameters(entity -> Array.of(new Parameter.Text(entity.spotifyTrackId().value())))
                    .withReadEntity(resultSet -> new TrackLinkEntity(new GpmStoreId(resultSet.getString(GPM_TRACK_STORE_ID.value())),
                                                                     new SpotifyTrackId(resultSet.getString(SPOTIFY_TRACK_ID.value()))))
                    .build();

    public TrackLinkDao(final DataSource dataSource) {
        super(dataSource, TABLE, COLUMNS);
    }

    public Map<GpmStoreId, SpotifyTrackId> findByGpmStoreIds(final Seq<GpmStoreId> gpmStoreIds) {
        return findByPrimaryKeys(gpmStoreIds).mapValues(TrackLinkEntity::spotifyTrackId);
    }

    public Option<TrackLinkEntity> findByGpmStoreId(final GpmStoreId gpmStoreId) {
        return findByPrimaryKey(gpmStoreId);
    }
}
