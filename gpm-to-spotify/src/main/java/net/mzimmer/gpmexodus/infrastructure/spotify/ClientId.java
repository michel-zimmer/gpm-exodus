package net.mzimmer.gpmexodus.infrastructure.spotify;

public record ClientId(String value) {
}
