package net.mzimmer.gpmexodus.infrastructure.gmusicapi;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmAlbumId;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmStoreId;
import net.mzimmer.gpmexodus.domain.types.AlbumName;
import net.mzimmer.gpmexodus.domain.types.ArtistName;
import net.mzimmer.gpmexodus.domain.types.TrackName;

@JsonIgnoreProperties(ignoreUnknown = true)
public record GmusicapiTrackJsonReadModel(GpmStoreId storeId,
                                          TrackName title,
                                          ArtistName artist,
                                          AlbumName album,
                                          GpmAlbumId albumId) {

    @JsonCreator
    static GmusicapiTrackJsonReadModel jsonCreator(@JsonProperty("storeId") String storeId,
                                                   @JsonProperty("title") String title,
                                                   @JsonProperty("artist") String artistName,
                                                   @JsonProperty("album") String albumName,
                                                   @JsonProperty("albumId") String albumId) {
        return new GmusicapiTrackJsonReadModel(new GpmStoreId(storeId),
                                               new TrackName(title),
                                               new ArtistName(artistName),
                                               new AlbumName(albumName),
                                               new GpmAlbumId(albumId));
    }
}
