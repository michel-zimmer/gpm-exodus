package net.mzimmer.gpmexodus.infrastructure.database.gpm;

import io.vavr.collection.Array;
import io.vavr.collection.Seq;
import io.vavr.collection.Stream;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmPlaylistEntryId;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmPlaylistEntryPosition;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmPlaylistId;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmStoreId;
import net.mzimmer.gpmexodus.lib.database.*;

import javax.sql.DataSource;
import java.sql.ResultSet;

public class GpmPlaylistEntryDao
        extends AbstractDao<GpmPlaylistEntryEntity, GpmPlaylistEntryId>
        implements Dao<GpmPlaylistEntryEntity, GpmPlaylistEntryId> {

    public static final TableName TABLE = new TableName("gpm_data_playlist_entry");
    public static final ColumnName PLAYLIST_ID = new ColumnName("playlist_id");
    public static final ColumnName POSITION = new ColumnName("position");
    public static final ColumnName TRACK_STORE_ID = new ColumnName("track_store_id");
    public static final Columns<GpmPlaylistEntryEntity, GpmPlaylistEntryId> COLUMNS =
            ColumnsBuilder.columns(GpmPlaylistEntryEntity.class, GpmPlaylistEntryId.class)
                          .withPrimaryColumnNames(PLAYLIST_ID, POSITION)
                          .withNonPrimaryColumnNames(TRACK_STORE_ID)
                          .withPrimaryKeyParameters(primaryKey -> Array.of(new Parameter.Text(primaryKey.playlistId().value()),
                                                                           new Parameter.Integer(primaryKey.position().value())))
                          .withNonPrimaryKeyParameters(entity -> Array.of(new Parameter.Text(entity.storeId().value())))
                          .withReadEntity(resultSet -> new GpmPlaylistEntryEntity(new GpmPlaylistId(resultSet.getString(PLAYLIST_ID.value())),
                                                                                  new GpmPlaylistEntryPosition(resultSet.getInt(POSITION.value())),
                                                                                  new GpmStoreId(resultSet.getString(TRACK_STORE_ID.value()))))
                          .build();
    private static final String FIND_BY_PLAYLIST_ID_SQL = "select * from %s where %s".formatted(TABLE.value(), PLAYLIST_ID.value() + " = ?");
    private static final String DELETE_BY_PLAYLIST_ID_SQL = "delete from %s where %s = ?".formatted(TABLE.value(), PLAYLIST_ID.value());

    public GpmPlaylistEntryDao(final DataSource dataSource) {
        super(dataSource, TABLE, COLUMNS);
    }

    public Seq<GpmPlaylistEntryEntity> findByPlaylistId(GpmPlaylistId playlistId) {
        return withPreparedStatement(FIND_BY_PLAYLIST_ID_SQL, preparedStatement -> {
            writeParameters(preparedStatement,
                            Array.of(new Parameter.Text(playlistId.value())),
                            0);
            final ResultSet resultSet = preparedStatement.executeQuery();
            return Stream.iterate(() -> nextEntity(resultSet)).toArray();
        });
    }

    public int deleteByPlaylistId(GpmPlaylistId playlistId) {
        return withPreparedStatement(DELETE_BY_PLAYLIST_ID_SQL, preparedStatement -> {
            writeParameters(preparedStatement,
                            Array.of(new Parameter.Text(playlistId.value())),
                            0);
            return preparedStatement.executeUpdate();
        });
    }
}
