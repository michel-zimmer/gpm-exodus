package net.mzimmer.gpmexodus.infrastructure.spotify;

import io.vavr.control.Option;

public final record SpotifyCredentials(ClientId clientId,
                                       ClientSecret clientSecret,
                                       Option<AccessToken> maybeAccessToken) {
    public SpotifyCredentials(final ClientId clientId,
                              final ClientSecret clientSecret) {
        this(clientId,
             clientSecret,
             Option.none());
    }

    public SpotifyCredentials(final ClientId clientId,
                              final ClientSecret clientSecret,
                              final AccessToken accessToken) {
        this(clientId,
             clientSecret,
             Option.of(accessToken));
    }
}
