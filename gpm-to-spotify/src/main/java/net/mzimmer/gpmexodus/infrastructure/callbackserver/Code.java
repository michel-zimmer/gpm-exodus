package net.mzimmer.gpmexodus.infrastructure.callbackserver;

public record Code(String value) {
}
