package net.mzimmer.gpmexodus.infrastructure.database.gpm;

import net.mzimmer.gpmexodus.domain.gpm.types.GpmAlbumId;
import net.mzimmer.gpmexodus.domain.types.AlbumName;
import net.mzimmer.gpmexodus.lib.database.Entity;

public record GpmAlbumEntity(GpmAlbumId id,
                             AlbumName albumName)
        implements Entity<GpmAlbumId> {

    @Override
    public GpmAlbumId primaryKey() {
        return id;
    }
}
