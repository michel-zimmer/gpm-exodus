package net.mzimmer.gpmexodus.infrastructure.database.gpm;

import io.vavr.collection.Seq;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmPlaylistId;
import net.mzimmer.gpmexodus.domain.types.PlaylistDescription;
import net.mzimmer.gpmexodus.domain.types.PlaylistName;
import net.mzimmer.gpmexodus.lib.database.Entity;

public record GpmPlaylistEntity(GpmPlaylistId id,
                                PlaylistName name,
                                PlaylistDescription description)
        implements Entity<GpmPlaylistId> {

    @Override
    public GpmPlaylistId primaryKey() {
        return id;
    }

    public Seq<GpmTrackEntity> tracks(final GpmPlaylistEntryDao playlistEntryDao,
                                      final GpmTrackDao trackDao) {
        return playlistEntryDao.findByPlaylistId(id)
                               .sortBy(GpmPlaylistEntryEntity::position)
                               .map(trackEntity -> trackEntity.track(trackDao));
    }
}
