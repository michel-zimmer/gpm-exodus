package net.mzimmer.gpmexodus.infrastructure.database.gpm;

import net.mzimmer.gpmexodus.domain.gpm.types.GpmAlbumId;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmStoreId;
import net.mzimmer.gpmexodus.domain.types.ArtistName;
import net.mzimmer.gpmexodus.domain.types.TrackName;
import net.mzimmer.gpmexodus.lib.database.Entity;

public record GpmTrackEntity(TrackName title,
                             ArtistName artistName,
                             GpmStoreId storeId,
                             GpmAlbumId albumId)
        implements Entity<GpmStoreId> {

    @Override
    public GpmStoreId primaryKey() {
        return storeId;
    }
}
