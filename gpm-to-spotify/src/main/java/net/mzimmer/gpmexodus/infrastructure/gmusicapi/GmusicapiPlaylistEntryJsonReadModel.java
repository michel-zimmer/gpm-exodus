package net.mzimmer.gpmexodus.infrastructure.gmusicapi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.vavr.control.Option;

@JsonIgnoreProperties(ignoreUnknown = true)
public record GmusicapiPlaylistEntryJsonReadModel(Option<GmusicapiTrackJsonReadModel> track) {
}
