package net.mzimmer.gpmexodus.infrastructure.database;

import io.vavr.collection.Array;
import io.vavr.control.Option;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmPlaylistId;
import net.mzimmer.gpmexodus.domain.spotify.types.SpotifyPlaylistId;
import net.mzimmer.gpmexodus.lib.database.*;

import javax.sql.DataSource;

import static net.mzimmer.gpmexodus.lib.database.ColumnsBuilder.columns;

public class PlaylistLinkDao
        extends AbstractDao<PlaylistLinkEntity, GpmPlaylistId>
        implements Dao<PlaylistLinkEntity, GpmPlaylistId> {

    public static final TableName TABLE = new TableName("gpm_data_playlist_to_spotify");
    public static final ColumnName GPM_PLAYLIST_ID = new ColumnName("gpm_data_playlist_id");
    public static final ColumnName SPOTIFY_PLAYLIST_ID = new ColumnName("spotify_playlist_id");
    public static final Columns<PlaylistLinkEntity, GpmPlaylistId> COLUMNS =
            columns(PlaylistLinkEntity.class, GpmPlaylistId.class)
                    .withPrimaryColumnNames(GPM_PLAYLIST_ID)
                    .withNonPrimaryColumnNames(SPOTIFY_PLAYLIST_ID)
                    .withPrimaryKeyParameters(primaryKey -> Array.of(new Parameter.Text(primaryKey.value())))
                    .withNonPrimaryKeyParameters(entity -> Array.of(new Parameter.Text(entity.spotifyPlaylistId().value())))
                    .withReadEntity(resultSet -> new PlaylistLinkEntity(new GpmPlaylistId(resultSet.getString(GPM_PLAYLIST_ID.value())),
                                                                        new SpotifyPlaylistId(resultSet.getString(SPOTIFY_PLAYLIST_ID.value()))))
                    .build();

    public PlaylistLinkDao(final DataSource dataSource) {
        super(dataSource, TABLE, COLUMNS);
    }

    public Option<PlaylistLinkEntity> findByGpmPlaylistId(final GpmPlaylistId gpmPlaylistId) {
        return findByPrimaryKey(gpmPlaylistId);
    }
}
