package net.mzimmer.gpmexodus.infrastructure.database.gpm;

import net.mzimmer.gpmexodus.domain.gpm.types.GpmPlaylistEntryId;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmPlaylistEntryPosition;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmPlaylistId;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmStoreId;
import net.mzimmer.gpmexodus.lib.database.Entity;

public record GpmPlaylistEntryEntity(GpmPlaylistId playlistId,
                                     GpmPlaylistEntryPosition position,
                                     GpmStoreId storeId)
        implements Entity<GpmPlaylistEntryId> {

    public GpmPlaylistEntryId primaryKey() {
        return new GpmPlaylistEntryId(playlistId, position);
    }

    public GpmTrackEntity track(GpmTrackDao trackDao) {
        return trackDao.findByStoreId(storeId).get();
    }
}
