package net.mzimmer.gpmexodus.infrastructure.spotify;

public record AccessToken(String value) {
}
