package net.mzimmer.gpmexodus.infrastructure.spotify;

public record ClientSecret(String value) {
}
