package net.mzimmer.gpmexodus.application.model;

import net.mzimmer.gpmexodus.domain.TrackLink;

public record LinkSpotifyTrackCommandPayload(TrackLink trackLink) {
}
