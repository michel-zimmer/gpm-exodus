package net.mzimmer.gpmexodus.application.model;

import net.mzimmer.gpmexodus.domain.types.NumberOfChangedAlbums;
import net.mzimmer.gpmexodus.domain.types.NumberOfChangedPlaylistEntries;
import net.mzimmer.gpmexodus.domain.types.NumberOfChangedPlaylists;
import net.mzimmer.gpmexodus.domain.types.NumberOfChangedTracks;

public record ImportGpmPlaylistsCommandResult(NumberOfChangedAlbums numberOfChangedAlbums,
                                              NumberOfChangedTracks numberOfChangedTracks,
                                              NumberOfChangedPlaylists numberOfChangedPlaylists,
                                              NumberOfChangedPlaylistEntries numberOfChangedPlaylistEntries) {
}
