package net.mzimmer.gpmexodus.application;

import io.vavr.collection.Seq;
import net.mzimmer.gpmexodus.application.model.ImportGpmPlaylistsCommandPayload;
import net.mzimmer.gpmexodus.application.model.ImportGpmPlaylistsCommandResult;
import net.mzimmer.gpmexodus.domain.gpm.GpmPlaylist;
import net.mzimmer.gpmexodus.domain.gpm.GpmPlaylistRepository;

public class ImportApplicationService {
    private final GpmPlaylistRepository gpmPlaylistRepository;

    public ImportApplicationService(final GpmPlaylistRepository gpmPlaylistRepository) {
        this.gpmPlaylistRepository = gpmPlaylistRepository;
    }

    public ImportGpmPlaylistsCommandResult importGpmPlaylistsFile(ImportGpmPlaylistsCommandPayload commandPayload) {
        final Seq<GpmPlaylist> gpmPlaylists = commandPayload.gpmPlaylists();

        final GpmPlaylistRepository.SaveResults saveResults = gpmPlaylistRepository.save(gpmPlaylists);

        return new ImportGpmPlaylistsCommandResult(saveResults.numberOfChangedAlbums(),
                                                   saveResults.numberOfChangedTracks(),
                                                   saveResults.numberOfChangedPlaylists(),
                                                   saveResults.numberOfChangedPlaylistEntries());
    }
}
