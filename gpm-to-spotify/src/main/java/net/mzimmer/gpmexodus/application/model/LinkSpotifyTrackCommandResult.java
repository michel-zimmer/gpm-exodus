package net.mzimmer.gpmexodus.application.model;

import net.mzimmer.gpmexodus.domain.TrackLink;

public interface LinkSpotifyTrackCommandResult {
    record NewlyLinked(TrackLink trackLink) implements LinkSpotifyTrackCommandResult {
    }

    record AlreadyLinked(TrackLink trackLink) implements LinkSpotifyTrackCommandResult {
    }
}
