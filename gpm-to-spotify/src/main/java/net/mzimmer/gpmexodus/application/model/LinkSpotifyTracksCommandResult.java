package net.mzimmer.gpmexodus.application.model;

import io.vavr.collection.Map;
import io.vavr.collection.Seq;
import net.mzimmer.gpmexodus.domain.gpm.GpmTrack;
import net.mzimmer.gpmexodus.domain.spotify.SpotifyTrack;
import net.mzimmer.gpmexodus.domain.spotify.types.SpotifyTrackId;

public interface LinkSpotifyTracksCommandResult {
    record PlaylistFound(Map<GpmTrack, SpotifyTrackId> alreadyLinkedTracks,
                         Map<GpmTrack, SpotifyTrack> newlyLinkedTracks,
                         Map<GpmTrack, Seq<SpotifyTrack>> notLinkedTracks) implements LinkSpotifyTracksCommandResult {
    }

    record PlaylistNotFound() implements LinkSpotifyTracksCommandResult {
    }
}
