package net.mzimmer.gpmexodus.application;

import io.vavr.control.Option;
import net.mzimmer.gpmexodus.application.model.SynchronizePlaylistCommandPayload;
import net.mzimmer.gpmexodus.application.model.SynchronizePlaylistCommandResult;
import net.mzimmer.gpmexodus.domain.PlaylistSynchronizationService;
import net.mzimmer.gpmexodus.domain.gpm.GpmPlaylist;
import net.mzimmer.gpmexodus.domain.gpm.GpmPlaylistRepository;

public class SpotifyWithLoginApplicationService {
    private final GpmPlaylistRepository gpmPlaylistRepository;
    private final PlaylistSynchronizationService playlistSynchronizationService;

    public SpotifyWithLoginApplicationService(final GpmPlaylistRepository gpmPlaylistRepository,
                                              final PlaylistSynchronizationService playlistSynchronizationService) {
        this.gpmPlaylistRepository = gpmPlaylistRepository;
        this.playlistSynchronizationService = playlistSynchronizationService;
    }

    public SynchronizePlaylistCommandResult synchronizePlaylist(final SynchronizePlaylistCommandPayload commandPayload) {
        final Option<GpmPlaylist> maybeGpmPlaylist = gpmPlaylistRepository.playlist(commandPayload.playlistId());

        return maybeGpmPlaylist.fold(
                SynchronizePlaylistCommandResult.PlaylistNotFound::new,
                gpmPlaylist -> new SynchronizePlaylistCommandResult.SynchronizedPlaylist(
                        playlistSynchronizationService.synchronizePlaylist(gpmPlaylist)
                                                      .playlist()
                )
        );
    }
}
