package net.mzimmer.gpmexodus.application.model;

import net.mzimmer.gpmexodus.domain.types.LinkProgressSummary;

public record LinkProgressCommandResult(LinkProgressSummary linkProgressSummary) {
}
