package net.mzimmer.gpmexodus.application.model;

import net.mzimmer.gpmexodus.domain.gpm.types.GpmPlaylistId;

public record SynchronizePlaylistCommandPayload(GpmPlaylistId playlistId) {
}
