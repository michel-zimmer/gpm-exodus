package net.mzimmer.gpmexodus.application.model;

import io.vavr.collection.Map;
import io.vavr.control.Option;
import net.mzimmer.gpmexodus.domain.gpm.GpmPlaylist;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmStoreId;
import net.mzimmer.gpmexodus.domain.spotify.types.SpotifyTrackId;

public record ListImportedGpmPlaylistCommandResult(Option<GpmPlaylist> maybePlaylist,
                                                   Map<GpmStoreId, SpotifyTrackId> links) {
}
