package net.mzimmer.gpmexodus.application;

import io.vavr.control.Option;
import net.mzimmer.gpmexodus.application.model.LinkSpotifyTracksCommandPayload;
import net.mzimmer.gpmexodus.application.model.LinkSpotifyTracksCommandResult;
import net.mzimmer.gpmexodus.domain.TrackLinkService;
import net.mzimmer.gpmexodus.domain.TrackLinkingResults;
import net.mzimmer.gpmexodus.domain.gpm.GpmPlaylist;
import net.mzimmer.gpmexodus.domain.gpm.GpmPlaylistRepository;

public class SpotifyApplicationService {
    private final GpmPlaylistRepository gpmPlaylistRepository;
    private final TrackLinkService trackLinkService;

    public SpotifyApplicationService(final GpmPlaylistRepository gpmPlaylistRepository,
                                     final TrackLinkService trackLinkService) {
        this.gpmPlaylistRepository = gpmPlaylistRepository;
        this.trackLinkService = trackLinkService;
    }

    public LinkSpotifyTracksCommandResult linkSpotifyTracks(final LinkSpotifyTracksCommandPayload commandPayload) {
        final Option<GpmPlaylist> maybeGpmPlaylist = gpmPlaylistRepository.playlist(commandPayload.gpmPlaylistId());
        return maybeGpmPlaylist.fold(
                LinkSpotifyTracksCommandResult.PlaylistNotFound::new,
                gpmPlaylist -> {
                    final TrackLinkingResults trackLinkingResults = trackLinkService.linkTracks(gpmPlaylist);
                    return new LinkSpotifyTracksCommandResult.PlaylistFound(trackLinkingResults.alreadyLinkedTracks(),
                                                                            trackLinkingResults.newlyLinkedTracks(),
                                                                            trackLinkingResults.notLinkedTracks());
                }
        );
    }
}
