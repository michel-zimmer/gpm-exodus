package net.mzimmer.gpmexodus.application.model;

import net.mzimmer.gpmexodus.domain.spotify.SpotifyPlaylist;

public interface SynchronizePlaylistCommandResult {
    final record SynchronizedPlaylist(SpotifyPlaylist playlist) implements SynchronizePlaylistCommandResult {
    }

    final record PlaylistNotFound() implements SynchronizePlaylistCommandResult {
    }
}
