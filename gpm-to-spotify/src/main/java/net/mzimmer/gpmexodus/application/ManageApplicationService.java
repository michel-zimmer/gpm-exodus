package net.mzimmer.gpmexodus.application;

import io.vavr.collection.HashMap;
import io.vavr.collection.Map;
import io.vavr.collection.Seq;
import io.vavr.control.Option;
import net.mzimmer.gpmexodus.application.model.*;
import net.mzimmer.gpmexodus.domain.LinkProgressService;
import net.mzimmer.gpmexodus.domain.TrackLink;
import net.mzimmer.gpmexodus.domain.TrackLinkRepository;
import net.mzimmer.gpmexodus.domain.gpm.GpmPlaylist;
import net.mzimmer.gpmexodus.domain.gpm.GpmPlaylistRepository;
import net.mzimmer.gpmexodus.domain.gpm.GpmTrack;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmStoreId;
import net.mzimmer.gpmexodus.domain.spotify.types.SpotifyTrackId;

public class ManageApplicationService {
    private final GpmPlaylistRepository gpmPlaylistRepository;
    private final TrackLinkRepository trackLinkRepository;
    private final LinkProgressService linkProgressService;

    public ManageApplicationService(final GpmPlaylistRepository gpmPlaylistRepository,
                                    final TrackLinkRepository trackLinkRepository,
                                    final LinkProgressService linkProgressService) {
        this.gpmPlaylistRepository = gpmPlaylistRepository;
        this.trackLinkRepository = trackLinkRepository;
        this.linkProgressService = linkProgressService;
    }

    public ListImportedGpmPlaylistsCommandResult listImportedGpmPlaylists() {
        final Seq<GpmPlaylist> playlists = gpmPlaylistRepository.playlists();

        return new ListImportedGpmPlaylistsCommandResult(playlists);
    }

    public ListImportedGpmPlaylistCommandResult listImportedGpmPlaylist(final ListImportedGpmPlaylistCommandPayload commandPayload) {
        final Option<GpmPlaylist> maybePlaylist = gpmPlaylistRepository.playlist(commandPayload.playlistId());

        final Map<GpmStoreId, SpotifyTrackId> links = maybePlaylist.fold(
                HashMap::empty,
                playlist -> {
                    final Seq<GpmStoreId> gpmStoreIds = playlist.tracks().map(GpmTrack::storeId);
                    return trackLinkRepository.trackLinks(gpmStoreIds);
                }
        );

        return new ListImportedGpmPlaylistCommandResult(maybePlaylist, links);
    }

    public LinkSpotifyTrackCommandResult linkSpotifyTrack(final LinkSpotifyTrackCommandPayload commandPayload) {
        final TrackLink trackLink = commandPayload.trackLink();
        final Option<TrackLink> maybeTrackLink = trackLinkRepository.trackLink(trackLink.gpmTrackStoreId());

        return maybeTrackLink.fold(
                () -> {
                    trackLinkRepository.save(trackLink);
                    return new LinkSpotifyTrackCommandResult.NewlyLinked(trackLink);
                },
                LinkSpotifyTrackCommandResult.AlreadyLinked::new
        );
    }

    public LinkProgressCommandResult linkProgress() {
        return new LinkProgressCommandResult(linkProgressService.linkProgress());
    }
}
