package net.mzimmer.gpmexodus.application.model;

import io.vavr.collection.Seq;
import net.mzimmer.gpmexodus.domain.gpm.GpmPlaylist;

public record ImportGpmPlaylistsCommandPayload(Seq<GpmPlaylist> gpmPlaylists) {
}
