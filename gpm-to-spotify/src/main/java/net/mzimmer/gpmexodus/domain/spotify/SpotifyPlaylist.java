package net.mzimmer.gpmexodus.domain.spotify;

import io.vavr.Lazy;
import io.vavr.collection.Seq;
import net.mzimmer.gpmexodus.domain.spotify.types.SpotifyPlaylistId;
import net.mzimmer.gpmexodus.domain.types.PlaylistDescription;
import net.mzimmer.gpmexodus.domain.types.PlaylistName;

import java.util.function.Supplier;

public interface SpotifyPlaylist {
    SpotifyPlaylistId id();

    PlaylistName name();

    PlaylistDescription description();

    Seq<SpotifyTrack> tracks();

    final class LazyTracks implements SpotifyPlaylist {
        private final SpotifyPlaylistId id;
        private final PlaylistName name;
        private final PlaylistDescription description;
        private final Lazy<Seq<SpotifyTrack>> tracks;

        public LazyTracks(SpotifyPlaylistId id,
                          PlaylistName name,
                          PlaylistDescription description,
                          Supplier<Seq<SpotifyTrack>> tracks) {
            this.id = id;
            this.name = name;
            this.description = description;
            this.tracks = Lazy.of(tracks);
        }

        public SpotifyPlaylistId id() {
            return id;
        }

        public PlaylistName name() {
            return name;
        }

        public PlaylistDescription description() {
            return description;
        }

        @Override
        public Seq<SpotifyTrack> tracks() {
            return tracks.get();
        }
    }
}
