package net.mzimmer.gpmexodus.domain.types;

public interface LinkProgress {
    NumberOfUniqueLinkedTracks numberOfUniqueLinkedTracks();

    NumberOfUniqueTracks numberOfUniqueTracks();
}
