package net.mzimmer.gpmexodus.domain;

import io.vavr.collection.Map;
import io.vavr.collection.Seq;
import io.vavr.control.Option;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmStoreId;
import net.mzimmer.gpmexodus.domain.spotify.types.SpotifyTrackId;

public interface TrackLinkRepository {
    Map<GpmStoreId, SpotifyTrackId> trackLinks(Seq<GpmStoreId> gpmStoreIds);

    Option<TrackLink> trackLink(GpmStoreId gpmStoreId);

    void save(TrackLink trackLink);
}
