package net.mzimmer.gpmexodus.domain.gpm.types;

public record GpmPlaylistId(String value) {
}
