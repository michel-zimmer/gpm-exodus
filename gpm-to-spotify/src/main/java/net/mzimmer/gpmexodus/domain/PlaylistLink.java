package net.mzimmer.gpmexodus.domain;

import net.mzimmer.gpmexodus.domain.gpm.types.GpmPlaylistId;
import net.mzimmer.gpmexodus.domain.spotify.types.SpotifyPlaylistId;

public record PlaylistLink(GpmPlaylistId gpmPlaylistId,
                           SpotifyPlaylistId spotifyPlaylistId) {
}
