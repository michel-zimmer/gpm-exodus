package net.mzimmer.gpmexodus.domain.gpm.types;

public record GpmPlaylistEntryId(GpmPlaylistId playlistId,
                                 GpmPlaylistEntryPosition position) {
}
