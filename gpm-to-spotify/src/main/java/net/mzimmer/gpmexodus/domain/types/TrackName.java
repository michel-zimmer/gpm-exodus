package net.mzimmer.gpmexodus.domain.types;

public record TrackName(String value) {
    public boolean equalsIgnoreCase(final TrackName anotherTrackName) {
        return value.equalsIgnoreCase(anotherTrackName.value);
    }
}
