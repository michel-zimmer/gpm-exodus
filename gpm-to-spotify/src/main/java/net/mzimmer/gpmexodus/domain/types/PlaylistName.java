package net.mzimmer.gpmexodus.domain.types;

public record PlaylistName(String value) {
}
