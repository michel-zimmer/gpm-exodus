package net.mzimmer.gpmexodus.domain.types;

public record NumberOfChangedPlaylists(int value) {
}
