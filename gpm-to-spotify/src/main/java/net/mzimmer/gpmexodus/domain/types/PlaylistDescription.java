package net.mzimmer.gpmexodus.domain.types;

public record PlaylistDescription(String value) {
}
