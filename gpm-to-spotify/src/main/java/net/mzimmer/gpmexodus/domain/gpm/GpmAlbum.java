package net.mzimmer.gpmexodus.domain.gpm;

import net.mzimmer.gpmexodus.domain.gpm.types.GpmAlbumId;
import net.mzimmer.gpmexodus.domain.types.AlbumName;

public interface GpmAlbum {
    GpmAlbumId id();

    AlbumName name();

    record Immutable(GpmAlbumId id,
                     AlbumName name) implements GpmAlbum {
    }
}
