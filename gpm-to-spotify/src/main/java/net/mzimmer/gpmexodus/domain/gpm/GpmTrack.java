package net.mzimmer.gpmexodus.domain.gpm;

import io.vavr.Lazy;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmStoreId;
import net.mzimmer.gpmexodus.domain.spotify.SpotifyTrack;
import net.mzimmer.gpmexodus.domain.types.ArtistName;
import net.mzimmer.gpmexodus.domain.types.TrackName;

import java.util.function.Supplier;

public interface GpmTrack {
    TrackName title();

    ArtistName artistName();

    GpmAlbum album();

    GpmStoreId storeId();

    default boolean matchesIgnoreCase(SpotifyTrack spotifyTrack) {
        return title().equalsIgnoreCase(spotifyTrack.title()) &&
               artistName().equalsIgnoreCase(spotifyTrack.artistName()) &&
               album().name().equalsIgnoreCase(spotifyTrack.albumName());
    }

    record Immutable(TrackName title,
                     ArtistName artistName,
                     GpmAlbum album,
                     GpmStoreId storeId) implements GpmTrack {
    }

    final class LazyAlbum implements GpmTrack {
        private final TrackName title;
        private final ArtistName artistName;
        private final Lazy<GpmAlbum> lazyAlbum;
        private final GpmStoreId storeId;

        public LazyAlbum(TrackName title,
                         ArtistName artistName,
                         Supplier<GpmAlbum> albumSupplier,
                         GpmStoreId storeId) {
            this.title = title;
            this.artistName = artistName;
            this.lazyAlbum = Lazy.of(albumSupplier);
            this.storeId = storeId;
        }

        public TrackName title() {
            return title;
        }

        public ArtistName artistName() {
            return artistName;
        }

        public GpmAlbum album() {
            return lazyAlbum.get();
        }

        public GpmStoreId storeId() {
            return storeId;
        }
    }
}
