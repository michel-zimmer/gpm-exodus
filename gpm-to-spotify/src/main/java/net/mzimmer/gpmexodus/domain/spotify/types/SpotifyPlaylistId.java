package net.mzimmer.gpmexodus.domain.spotify.types;

public record SpotifyPlaylistId(String value) {
}
