package net.mzimmer.gpmexodus.domain.gpm;

import io.vavr.collection.Seq;
import io.vavr.control.Option;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmPlaylistId;
import net.mzimmer.gpmexodus.domain.types.NumberOfChangedAlbums;
import net.mzimmer.gpmexodus.domain.types.NumberOfChangedPlaylistEntries;
import net.mzimmer.gpmexodus.domain.types.NumberOfChangedPlaylists;
import net.mzimmer.gpmexodus.domain.types.NumberOfChangedTracks;

public interface GpmPlaylistRepository {
    SaveResults save(Seq<GpmPlaylist> playlists);

    Seq<GpmPlaylist> playlists();

    Option<GpmPlaylist> playlist(GpmPlaylistId id);

    record SaveResults(NumberOfChangedAlbums numberOfChangedAlbums,
                       NumberOfChangedTracks numberOfChangedTracks,
                       NumberOfChangedPlaylists numberOfChangedPlaylists,
                       NumberOfChangedPlaylistEntries numberOfChangedPlaylistEntries) {
    }
}
