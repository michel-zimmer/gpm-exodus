package net.mzimmer.gpmexodus.domain.spotify.types;

public record SpotifyTrackId(String value) {
}
