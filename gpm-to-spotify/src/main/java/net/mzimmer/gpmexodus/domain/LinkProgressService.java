package net.mzimmer.gpmexodus.domain;

import io.vavr.collection.Map;
import io.vavr.collection.Seq;
import net.mzimmer.gpmexodus.domain.gpm.GpmPlaylist;
import net.mzimmer.gpmexodus.domain.gpm.GpmPlaylistRepository;
import net.mzimmer.gpmexodus.domain.gpm.GpmTrack;
import net.mzimmer.gpmexodus.domain.gpm.GpmTrackRepository;
import net.mzimmer.gpmexodus.domain.types.*;

import static java.util.function.Function.identity;

public class LinkProgressService {
    private final GpmPlaylistRepository gpmPlaylistRepository;
    private final GpmTrackRepository gpmTrackRepository;
    private final TrackLinkRepository trackLinkRepository;
    private final PlaylistLinkRepository playlistLinkRepository;

    public LinkProgressService(final GpmPlaylistRepository gpmPlaylistRepository,
                               final GpmTrackRepository gpmTrackRepository,
                               final TrackLinkRepository trackLinkRepository,
                               final PlaylistLinkRepository playlistLinkRepository) {
        this.gpmPlaylistRepository = gpmPlaylistRepository;
        this.gpmTrackRepository = gpmTrackRepository;
        this.trackLinkRepository = trackLinkRepository;
        this.playlistLinkRepository = playlistLinkRepository;
    }

    public LinkProgressSummary linkProgress() {
        final Seq<GpmTrack> gpmTracks = gpmTrackRepository.tracks();

        final NumberOfUniqueLinkedTracks overallNumberOfUniqueLinkedTracks =
                new NumberOfUniqueLinkedTracks(trackLinkRepository.trackLinks(gpmTracks.map(GpmTrack::storeId)).size());
        final NumberOfUniqueTracks overallNumberOfUniqueTracks = new NumberOfUniqueTracks(gpmTracks.size());
        final OverallLinkProgress overallProgress = new OverallLinkProgress(overallNumberOfUniqueLinkedTracks,
                                                                            overallNumberOfUniqueTracks);

        final Map<GpmPlaylist, PlaylistLinkProgress> playlistProgress = gpmPlaylistRepository.playlists().toLinkedMap(
                identity(),
                gpmPlaylist -> gpmPlaylist.syncStatus(playlistLinkRepository, trackLinkRepository)
        );

        return new LinkProgressSummary(overallProgress, playlistProgress);
    }
}
