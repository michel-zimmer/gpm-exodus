package net.mzimmer.gpmexodus.domain.spotify;

import io.vavr.collection.Seq;
import net.mzimmer.gpmexodus.domain.types.AlbumName;
import net.mzimmer.gpmexodus.domain.types.ArtistName;
import net.mzimmer.gpmexodus.domain.types.TrackName;

public interface SpotifyTrackRepository {
    Seq<SpotifyTrack> tracks(TrackName trackName,
                             ArtistName artistName,
                             AlbumName albumName);
}
