package net.mzimmer.gpmexodus.domain;

import net.mzimmer.gpmexodus.domain.spotify.SpotifyPlaylist;

public final record PlaylistSynchronizationResult(SpotifyPlaylist playlist) {
}
