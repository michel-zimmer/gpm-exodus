package net.mzimmer.gpmexodus.domain.spotify;

import net.mzimmer.gpmexodus.domain.spotify.types.SpotifyPlaylistId;
import net.mzimmer.gpmexodus.domain.types.PlaylistName;

public interface SpotifyPlaylistRepository {
    SpotifyPlaylist create(PlaylistName name);

    SpotifyPlaylist spotifyPlaylist(SpotifyPlaylistId id);

    void save(SpotifyPlaylist playlist);
}
