package net.mzimmer.gpmexodus.domain.gpm;

import io.vavr.Lazy;
import io.vavr.collection.Map;
import io.vavr.collection.Seq;
import net.mzimmer.gpmexodus.domain.PlaylistLink;
import net.mzimmer.gpmexodus.domain.PlaylistLinkRepository;
import net.mzimmer.gpmexodus.domain.TrackLinkRepository;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmPlaylistEntryPosition;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmPlaylistId;
import net.mzimmer.gpmexodus.domain.types.*;

import java.util.function.Supplier;

public interface GpmPlaylist {
    GpmPlaylistId id();

    PlaylistName name();

    PlaylistDescription description();

    Seq<GpmTrack> tracks();

    default Map<GpmPlaylistEntryPosition, GpmTrack> positionsToTrack() {
        return tracks().zipWithIndex()
                       .toMap(trackWithIndex -> trackWithIndex.swap()
                                                              .map1(index -> new GpmPlaylistEntryPosition(index + 1)));
    }

    default PlaylistLinkProgress syncStatus(final PlaylistLinkRepository playlistLinkRepository,
                                            final TrackLinkRepository trackLinkRepository) {
        final Seq<GpmTrack> uniqueTracks = tracks().distinctBy(GpmTrack::storeId);
        return new PlaylistLinkProgress(playlistLinkRepository.playlistLink(id())
                                                              .map(PlaylistLink::spotifyPlaylistId),
                                        new NumberOfUniqueLinkedTracks(uniqueTracks.map(GpmTrack::storeId)
                                                                                   .flatMap(trackLinkRepository::trackLink)
                                                                                   .size()),
                                        new NumberOfUniqueTracks(uniqueTracks.size()));
    }

    record Immutable(GpmPlaylistId id,
                     PlaylistName name,
                     PlaylistDescription description,
                     Seq<GpmTrack> tracks) implements GpmPlaylist {
    }

    final class LazyTracks implements GpmPlaylist {
        private final GpmPlaylistId id;
        private final PlaylistName name;
        private final PlaylistDescription description;
        private final Lazy<Seq<GpmTrack>> lazyTracks;

        public LazyTracks(GpmPlaylistId id,
                          PlaylistName name,
                          PlaylistDescription description,
                          Supplier<Seq<GpmTrack>> tracksProvider) {
            this.id = id;
            this.name = name;
            this.description = description;
            this.lazyTracks = Lazy.of(tracksProvider);
        }

        public GpmPlaylistId id() {
            return id;
        }

        public PlaylistName name() {
            return name;
        }

        public PlaylistDescription description() {
            return description;
        }

        @Override
        public Seq<GpmTrack> tracks() {
            return lazyTracks.get();
        }
    }
}
