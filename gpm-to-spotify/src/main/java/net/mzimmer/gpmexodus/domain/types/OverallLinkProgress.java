package net.mzimmer.gpmexodus.domain.types;

public record OverallLinkProgress(NumberOfUniqueLinkedTracks numberOfUniqueLinkedTracks,
                                  NumberOfUniqueTracks numberOfUniqueTracks) implements LinkProgress {
}
