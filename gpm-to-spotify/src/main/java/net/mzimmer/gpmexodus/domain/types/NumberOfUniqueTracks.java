package net.mzimmer.gpmexodus.domain.types;

public record NumberOfUniqueTracks(int value) {
}
