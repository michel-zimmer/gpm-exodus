package net.mzimmer.gpmexodus.domain;

import io.vavr.collection.Map;
import io.vavr.collection.Seq;
import net.mzimmer.gpmexodus.domain.gpm.GpmTrack;
import net.mzimmer.gpmexodus.domain.spotify.SpotifyTrack;
import net.mzimmer.gpmexodus.domain.spotify.types.SpotifyTrackId;

public record TrackLinkingResults(Map<GpmTrack, SpotifyTrackId> alreadyLinkedTracks,
                                  Map<GpmTrack, SpotifyTrack> newlyLinkedTracks,
                                  Map<GpmTrack, Seq<SpotifyTrack>> notLinkedTracks) {
}
