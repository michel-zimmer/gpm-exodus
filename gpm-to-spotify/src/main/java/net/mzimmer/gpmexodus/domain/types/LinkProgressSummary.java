package net.mzimmer.gpmexodus.domain.types;

import io.vavr.collection.Map;
import net.mzimmer.gpmexodus.domain.gpm.GpmPlaylist;

public record LinkProgressSummary(OverallLinkProgress overallProgress,
                                  Map<GpmPlaylist, PlaylistLinkProgress> playlistProgress) {
}
