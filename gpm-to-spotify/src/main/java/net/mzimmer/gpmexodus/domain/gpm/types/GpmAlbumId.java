package net.mzimmer.gpmexodus.domain.gpm.types;

import com.fasterxml.jackson.annotation.JsonCreator;

public record GpmAlbumId(String value) {
    @JsonCreator
    private static GpmAlbumId jsonCreator(String value) {
        return new GpmAlbumId(value);
    }
}
