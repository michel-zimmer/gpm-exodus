package net.mzimmer.gpmexodus.domain.types;

public record NumberOfChangedPlaylistEntries(int value) {
}
