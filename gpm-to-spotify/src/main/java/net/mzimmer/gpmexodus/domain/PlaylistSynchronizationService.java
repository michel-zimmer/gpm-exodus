package net.mzimmer.gpmexodus.domain;

import io.vavr.control.Option;
import net.mzimmer.gpmexodus.domain.gpm.GpmPlaylist;
import net.mzimmer.gpmexodus.domain.spotify.SpotifyPlaylist;
import net.mzimmer.gpmexodus.domain.spotify.SpotifyPlaylistRepository;

public class PlaylistSynchronizationService {
    private final PlaylistLinkRepository playlistLinkRepository;
    private final SpotifyPlaylistRepository spotifyPlaylistRepository;
    private final PlaylistConverter playlistConverter;

    public PlaylistSynchronizationService(final PlaylistLinkRepository playlistLinkRepository,
                                          final SpotifyPlaylistRepository spotifyPlaylistRepository,
                                          final PlaylistConverter playlistConverter) {
        this.playlistLinkRepository = playlistLinkRepository;
        this.spotifyPlaylistRepository = spotifyPlaylistRepository;
        this.playlistConverter = playlistConverter;
    }

    public PlaylistSynchronizationResult synchronizePlaylist(final GpmPlaylist gpmPlaylist) {
        final Option<PlaylistLink> maybePlaylistLink = playlistLinkRepository.playlistLink(gpmPlaylist.id());

        final SpotifyPlaylist spotifyPlaylist = maybePlaylistLink.fold(
                () -> {
                    final SpotifyPlaylist newlyCreatedSpotifyPlaylist = spotifyPlaylistRepository.create(gpmPlaylist.name());
                    playlistLinkRepository.save(new PlaylistLink(gpmPlaylist.id(),
                                                                 newlyCreatedSpotifyPlaylist.id()));
                    return newlyCreatedSpotifyPlaylist;
                },
                playlistLink -> spotifyPlaylistRepository.spotifyPlaylist(playlistLink.spotifyPlaylistId())
        );

        final SpotifyPlaylist updatedSpotifyPlaylist = playlistConverter.toSpotify(gpmPlaylist, spotifyPlaylist.id());
        spotifyPlaylistRepository.save(updatedSpotifyPlaylist);

        return new PlaylistSynchronizationResult(updatedSpotifyPlaylist);
    }
}
