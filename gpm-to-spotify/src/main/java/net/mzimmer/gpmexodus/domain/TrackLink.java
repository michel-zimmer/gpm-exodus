package net.mzimmer.gpmexodus.domain;

import net.mzimmer.gpmexodus.domain.gpm.types.GpmStoreId;
import net.mzimmer.gpmexodus.domain.spotify.types.SpotifyTrackId;

public record TrackLink(GpmStoreId gpmTrackStoreId,
                        SpotifyTrackId spotifyTrackId) {
}
