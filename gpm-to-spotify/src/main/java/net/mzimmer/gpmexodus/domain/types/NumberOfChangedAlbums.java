package net.mzimmer.gpmexodus.domain.types;

public record NumberOfChangedAlbums(int value) {
}
