package net.mzimmer.gpmexodus.domain;

import io.vavr.API;
import net.mzimmer.gpmexodus.domain.gpm.GpmPlaylist;
import net.mzimmer.gpmexodus.domain.spotify.SpotifyPlaylist;
import net.mzimmer.gpmexodus.domain.spotify.types.SpotifyPlaylistId;

import static java.util.function.Function.identity;

public class PlaylistConverter {
    private final TrackConverter trackConverter;

    public PlaylistConverter(final TrackConverter trackConverter) {
        this.trackConverter = trackConverter;
    }

    public SpotifyPlaylist toSpotify(final GpmPlaylist gpmPlaylist,
                                     final SpotifyPlaylistId id) {
        return new SpotifyPlaylist.LazyTracks(
                id,
                gpmPlaylist.name(),
                gpmPlaylist.description(),
                () -> gpmPlaylist.tracks()
                                 .map(gpmTrack -> trackConverter.toSpotify(gpmTrack)
                                                                .fold(API::TODO, identity()))
        );
    }
}
