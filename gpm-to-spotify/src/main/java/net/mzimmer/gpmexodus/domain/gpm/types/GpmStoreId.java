package net.mzimmer.gpmexodus.domain.gpm.types;

import com.fasterxml.jackson.annotation.JsonCreator;

public record GpmStoreId(String value) {
    @JsonCreator
    private static GpmStoreId jsonCreator(String value) {
        return new GpmStoreId(value);
    }
}
