package net.mzimmer.gpmexodus.domain.gpm;

import io.vavr.collection.Seq;

public interface GpmTrackRepository {
    Seq<GpmTrack> tracks();
}
