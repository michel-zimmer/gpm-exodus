package net.mzimmer.gpmexodus.domain.spotify;

public record SpotifyTrackUri(String value) {
}
