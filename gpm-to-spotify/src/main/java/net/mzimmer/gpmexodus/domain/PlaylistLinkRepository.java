package net.mzimmer.gpmexodus.domain;

import io.vavr.control.Option;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmPlaylistId;

public interface PlaylistLinkRepository {
    Option<PlaylistLink> playlistLink(GpmPlaylistId gpmPlaylistId);

    void save(PlaylistLink playlistLink);
}
