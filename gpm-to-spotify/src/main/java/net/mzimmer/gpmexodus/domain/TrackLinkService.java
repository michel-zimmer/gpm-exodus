package net.mzimmer.gpmexodus.domain;

import io.vavr.Tuple;
import io.vavr.collection.Map;
import io.vavr.collection.Seq;
import io.vavr.collection.Set;
import io.vavr.control.Option;
import net.mzimmer.gpmexodus.domain.gpm.GpmPlaylist;
import net.mzimmer.gpmexodus.domain.gpm.GpmTrack;
import net.mzimmer.gpmexodus.domain.gpm.types.GpmStoreId;
import net.mzimmer.gpmexodus.domain.spotify.SpotifyTrack;
import net.mzimmer.gpmexodus.domain.spotify.SpotifyTrackRepository;
import net.mzimmer.gpmexodus.domain.spotify.types.SpotifyTrackId;

import java.util.function.Function;

import static java.util.function.Predicate.not;

public class TrackLinkService {
    private final TrackLinkRepository trackLinkRepository;
    private final SpotifyTrackRepository spotifyTrackRepository;

    public TrackLinkService(final TrackLinkRepository trackLinkRepository,
                            final SpotifyTrackRepository spotifyTrackRepository) {
        this.trackLinkRepository = trackLinkRepository;
        this.spotifyTrackRepository = spotifyTrackRepository;
    }

    public TrackLinkingResults linkTracks(final GpmPlaylist gpmPlaylist) {
        final Map<GpmStoreId, SpotifyTrackId> alreadyLinkedIds = trackLinkRepository.trackLinks(gpmPlaylist.tracks().map(GpmTrack::storeId).distinct());
        final Map<GpmTrack, SpotifyTrackId> alreadyLinkedTracks =
                gpmPlaylist.tracks()
                           .distinctBy(GpmTrack::storeId)
                           .toMap(Function.identity(),
                                  track -> alreadyLinkedIds.get(track.storeId()))
                           .flatMap((gpmTrack, maybeSpotifyTrackId) ->
                                            maybeSpotifyTrackId.map(spotifyTrackId -> Tuple.of(gpmTrack, spotifyTrackId))
                           );
        final Set<GpmStoreId> alreadyLinkedTrackStoreIds = alreadyLinkedIds.keySet();
        final Seq<GpmTrack> tracksToSearch = gpmPlaylist.tracks().filter(not(track -> alreadyLinkedTrackStoreIds.contains(track.storeId())));
        final Map<GpmTrack, Seq<SpotifyTrack>> searchResults = tracksToSearch.toMap(Function.identity(),
                                                                                    trackToLink -> spotifyTrackRepository.tracks(trackToLink.title(),
                                                                                                                                 trackToLink.artistName(),
                                                                                                                                 trackToLink.album().name()));

        final Map<GpmTrack, Seq<SpotifyTrack>> optimizedSearchResults = searchResults.map(
                (gpmTrack, spotifyTracks) -> {
                    final Seq<SpotifyTrack> filteredSpotifyTracks =
                            spotifyTracks.size() > 1
                                    ? spotifyTracks.filter(gpmTrack::matchesIgnoreCase)
                                    : spotifyTracks;
                    return Tuple.of(gpmTrack,
                                    filteredSpotifyTracks.size() == 1 ?
                                            filteredSpotifyTracks :
                                            spotifyTracks);
                }
        );

        final Map<GpmTrack, SpotifyTrack> tracksToLink = optimizedSearchResults.flatMap(
                (gpmTrack, spotifyTracks) ->
                        spotifyTracks.size() == 1 ?
                                Option.of(Tuple.of(gpmTrack, spotifyTracks.get(0))) :
                                Option.none()
        );
        final Map<GpmTrack, SpotifyTrack> newlyLinkedTracks = tracksToLink.map((gpmTrack, spotifyTrack) -> {
            final TrackLink trackLink = new TrackLink(gpmTrack.storeId(),
                                                      spotifyTrack.id());
            trackLinkRepository.save(trackLink);
            return Tuple.of(gpmTrack, spotifyTrack);
        });
        final Map<GpmTrack, Seq<SpotifyTrack>> notLinkedTracks = optimizedSearchResults.filterValues(spotifyTracks -> spotifyTracks.size() != 1);
        return new TrackLinkingResults(alreadyLinkedTracks,
                                       newlyLinkedTracks,
                                       notLinkedTracks);
    }
}
