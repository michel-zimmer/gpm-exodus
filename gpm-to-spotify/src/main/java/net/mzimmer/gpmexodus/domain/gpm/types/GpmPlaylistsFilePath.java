package net.mzimmer.gpmexodus.domain.gpm.types;

import java.nio.file.Path;

public record GpmPlaylistsFilePath(Path value) {
}
