package net.mzimmer.gpmexodus.domain.gpm.types;

public record GpmPlaylistEntryPosition(int value) implements Comparable<GpmPlaylistEntryPosition> {
    @Override
    public int compareTo(GpmPlaylistEntryPosition o) {
        return Integer.compare(value, o.value);
    }
}
