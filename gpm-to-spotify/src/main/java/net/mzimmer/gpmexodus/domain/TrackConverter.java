package net.mzimmer.gpmexodus.domain;

import io.vavr.control.Option;
import net.mzimmer.gpmexodus.domain.gpm.GpmTrack;
import net.mzimmer.gpmexodus.domain.spotify.SpotifyTrack;
import net.mzimmer.gpmexodus.domain.spotify.types.SpotifyTrackId;

public class TrackConverter {
    private final TrackLinkRepository trackLinkRepository;

    public TrackConverter(final TrackLinkRepository trackLinkRepository) {
        this.trackLinkRepository = trackLinkRepository;
    }

    public Option<SpotifyTrack> toSpotify(final GpmTrack gpmTrack) {
        return trackLinkRepository.trackLink(gpmTrack.storeId())
                                  .map(trackLink -> toSpotify(gpmTrack, trackLink.spotifyTrackId()));
    }

    public SpotifyTrack toSpotify(final GpmTrack gpmTrack,
                                  final SpotifyTrackId id) {
        return new SpotifyTrack.Immutable(id,
                                          gpmTrack.title(),
                                          gpmTrack.artistName(),
                                          gpmTrack.album().name());
    }
}
