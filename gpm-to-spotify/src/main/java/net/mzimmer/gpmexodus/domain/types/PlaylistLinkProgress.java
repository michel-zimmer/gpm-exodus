package net.mzimmer.gpmexodus.domain.types;

import io.vavr.control.Option;
import net.mzimmer.gpmexodus.domain.spotify.types.SpotifyPlaylistId;

public record PlaylistLinkProgress(Option<SpotifyPlaylistId> maybeSpotifyPlaylistId,
                                   NumberOfUniqueLinkedTracks numberOfUniqueLinkedTracks,
                                   NumberOfUniqueTracks numberOfUniqueTracks) implements LinkProgress {
}
