package net.mzimmer.gpmexodus.domain.types;

public record NumberOfChangedTracks(int value) {
}
