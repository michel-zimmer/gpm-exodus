package net.mzimmer.gpmexodus.domain.types;

import com.fasterxml.jackson.annotation.JsonCreator;

public record AlbumName(String value) {
    @JsonCreator
    private static AlbumName jsonCreator(String value) {
        return new AlbumName(value);
    }

    public boolean equalsIgnoreCase(final AlbumName anotherAlbumName) {
        return value.equalsIgnoreCase(anotherAlbumName.value);
    }
}
