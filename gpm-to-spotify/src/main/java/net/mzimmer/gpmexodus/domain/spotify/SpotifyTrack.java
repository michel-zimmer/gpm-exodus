package net.mzimmer.gpmexodus.domain.spotify;

import net.mzimmer.gpmexodus.domain.spotify.types.SpotifyTrackId;
import net.mzimmer.gpmexodus.domain.types.AlbumName;
import net.mzimmer.gpmexodus.domain.types.ArtistName;
import net.mzimmer.gpmexodus.domain.types.TrackName;

public interface SpotifyTrack {
    SpotifyTrackId id();

    default SpotifyTrackUri uri() {
        return new SpotifyTrackUri("spotify:track:" + id().value());
    }

    TrackName title();

    ArtistName artistName();

    AlbumName albumName();

    final record Immutable(SpotifyTrackId id,
                           TrackName title,
                           ArtistName artistName,
                           AlbumName albumName) implements SpotifyTrack {
    }
}
