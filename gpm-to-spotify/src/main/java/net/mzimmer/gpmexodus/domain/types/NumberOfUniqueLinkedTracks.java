package net.mzimmer.gpmexodus.domain.types;

public record NumberOfUniqueLinkedTracks(int value) {
}
