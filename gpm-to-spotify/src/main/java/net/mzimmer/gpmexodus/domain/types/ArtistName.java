package net.mzimmer.gpmexodus.domain.types;

public record ArtistName(String value) {
    public boolean equalsIgnoreCase(final ArtistName anotherArtistName) {
        return value.equalsIgnoreCase(anotherArtistName.value);
    }
}
