package net.mzimmer.gpmexodus.lib.database;

public record ColumnName(String value) {
}
