package net.mzimmer.gpmexodus.lib.stdio;

public record StdoutOutput(String value) {
}
