package net.mzimmer.gpmexodus.lib;

import io.vavr.control.Option;

import static org.apache.commons.lang3.StringUtils.trimToNull;

public interface StringUtils {
    static Option<String> emptyToNone(String string) {
        return Option.of(trimToNull(string));
    }
}
