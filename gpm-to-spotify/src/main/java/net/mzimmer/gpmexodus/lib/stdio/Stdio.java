package net.mzimmer.gpmexodus.lib.stdio;

import java.io.InputStream;
import java.io.PrintStream;

public interface Stdio {
    InputStream stdin();

    PrintStream stdout();

    PrintStream stderr();

    Stdio SYSTEM = new Immutable(System.in, System.out, System.err);

    record Immutable(InputStream stdin,
                     PrintStream stdout,
                     PrintStream stderr) implements Stdio {
    }
}
