package net.mzimmer.gpmexodus.lib.cli.parser;

import io.vavr.collection.Array;
import io.vavr.control.Either;

import static net.mzimmer.gpmexodus.lib.cli.parser.GlobalContext.empty;

public class Parser<C> {
    private final CommandDefinitions<C> commandDefinitions;

    public Parser(final CommandDefinitions<C> commandDefinitions) {
        this.commandDefinitions = commandDefinitions;
    }

    public Either<String, C> parse(final String... arguments) {
        return Array.of(arguments)
                    .foldLeft(empty(commandDefinitions), Context::next)
                    .build();
    }
}
