package net.mzimmer.gpmexodus.lib.database;

import io.vavr.collection.Seq;
import io.vavr.collection.Stream;
import io.vavr.control.Option;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static net.mzimmer.gpmexodus.lib.database.Column.mkString;

public abstract class AbstractDao<E extends Entity<PK>, PK>
        implements Dao<E, PK> {

    private final DataSource dataSource;
    private final Columns<E, PK> columns;
    private final String findAllSql;
    private final String findByPrimaryKeySql;
    private final String insertSql;
    private final String updateSql;
    private final String deleteByPrimaryKeySql;

    public AbstractDao(final DataSource dataSource,
                       final TableName tableName,
                       final Columns<E, PK> columns) {
        this.dataSource = dataSource;
        this.columns = columns;
        findAllSql = "select * from " + tableName.value();
        findByPrimaryKeySql = "select * from %s where %s"
                .formatted(tableName.value(),
                           mkString(columns.primaryKeyColumns(), " = ?", " AND "));
        insertSql = "insert into %s (%s) values (%s)"
                .formatted(tableName.value(),
                           mkString(columns.columns(), "", ", "),
                           Stream.fill(columns.columns().size(), "?").mkString(", "));
        updateSql = "update %s set %s where %s"
                .formatted(tableName.value(),
                           mkString(columns.nonPrimaryKeyColumns(), " = ?", ", "),
                           mkString(columns.primaryKeyColumns(), " = ?", " AND "));
        deleteByPrimaryKeySql = "delete from %s where %s"
                .formatted(tableName.value(),
                           mkString(columns.primaryKeyColumns(), " = ?", " AND "));
    }

    @Override
    public Seq<E> findAll() {
        return withPreparedStatement(findAllSql, preparedStatement -> {
            final ResultSet resultSet = preparedStatement.executeQuery();
            return Stream.iterate(() -> nextEntity(resultSet)).toArray();
        });
    }

    @Override
    public Option<E> findByPrimaryKey(final PK primaryKey) {
        return withPreparedStatement(findByPrimaryKeySql, preparedStatement -> {
            writeParameters(preparedStatement,
                            columns.primaryKeyParameters(primaryKey),
                            0);
            final ResultSet resultSet = preparedStatement.executeQuery();
            return nextEntity(resultSet);
        });
    }

    @Override
    public int insert(final E entity) {
        return withPreparedStatement(insertSql, preparedStatement -> {
            writeParameters(preparedStatement,
                            columns.parameters(entity),
                            0);
            return preparedStatement.executeUpdate();
        });
    }

    @Override
    public int update(final E entity) {
        return withPreparedStatement(updateSql, preparedStatement -> {
            final int whereClauseOffset = writeParameters(preparedStatement,
                                                          columns.nonPrimaryKeyParameters(entity),
                                                          0);
            writeParameters(preparedStatement,
                            columns.primaryKeyParameters(entity),
                            whereClauseOffset);
            return preparedStatement.executeUpdate();
        });
    }

    @Override
    public int deleteByPrimaryKey(PK primaryKey) {
        return withPreparedStatement(deleteByPrimaryKeySql, preparedStatement -> {
            writeParameters(preparedStatement,
                            columns.primaryKeyParameters(primaryKey),
                            0);
            return preparedStatement.executeUpdate();
        });
    }

    protected int writeParameters(final PreparedStatement preparedStatement,
                                  final Seq<Parameter<?>> parameters,
                                  final int offset) throws SQLException {
        int parameterIndex = offset;
        for (Parameter<?> parameter : parameters) {
            parameterIndex += 1;
            if (parameter instanceof Parameter.Text text) {
                preparedStatement.setString(parameterIndex, text.value());
            } else if (parameter instanceof Parameter.Integer integer) {
                preparedStatement.setInt(parameterIndex, integer.value());
            } else {
                throw new IllegalStateException();
            }
        }
        return parameterIndex;
    }

    protected Option<E> nextEntity(final ResultSet resultSet) {
        try {
            return resultSet.next() ?
                    Option.of(readEntity(resultSet)) :
                    Option.none();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    protected E readEntity(final ResultSet resultSet) throws SQLException {
        return columns.readEntity(resultSet);
    }

    protected <T> T withPreparedStatement(final String sql,
                                          final PreparedStatementFunction<T> preparedStatementFunction) {
        try (final Connection connection = dataSource.getConnection()) {
            try (final PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                return preparedStatementFunction.apply(preparedStatement);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
