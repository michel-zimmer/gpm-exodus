package net.mzimmer.gpmexodus.lib.stdio;

import io.vavr.control.Option;

public record Output(Option<StdoutOutput> stdout,
                     Option<StderrOutput> stderr) {
    public static Output stdout(String text) {
        return new Output(noneIfEmpty(text).map(StdoutOutput::new), Option.none());
    }

    public static Output stderr(String text) {
        return new Output(Option.none(), noneIfEmpty(text).map(StderrOutput::new));
    }

    public void println(Stdio stdio) {
        stdout().map(StdoutOutput::value).forEach(stdio.stdout()::println);
        stderr().map(StderrOutput::value).forEach(stdio.stderr()::println);
    }

    private static Option<String> noneIfEmpty(String text) {
        return "".equals(text) ? Option.none() : Option.of(text);
    }
}
