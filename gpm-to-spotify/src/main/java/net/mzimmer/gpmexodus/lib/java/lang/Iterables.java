package net.mzimmer.gpmexodus.lib.java.lang;

import java.util.Iterator;
import java.util.function.Function;

public interface Iterables {
    @SafeVarargs
    static <T> T singleFromDuplicates(final Iterable<T> iterable,
                                      final Function<T, ?>... extractors) {
        if (iterable == null) throw new IllegalArgumentException("iterable must not be null");
        if (extractors == null) throw new IllegalArgumentException("extractors must not be null");
        final Iterator<T> iterator = iterable.iterator();
        if (!iterator.hasNext()) {
            throw new IllegalArgumentException("iterable must not be empty");
        }
        final T single = iterator.next();
        final Object[] singleElementClassifiedArray = new Object[extractors.length];
        for (int extractorIndex = 0; extractorIndex < extractors.length; extractorIndex++) {
            final Function<T, ?> extractor = extractors[extractorIndex];
            singleElementClassifiedArray[extractorIndex] = extractor.apply(single);
        }
        while (iterator.hasNext()) {
            final T duplicatesElement = iterator.next();
            for (int extractorIndex = 0; extractorIndex < extractors.length; extractorIndex++) {
                final Function<T, ?> extractor = extractors[extractorIndex];
                final Object singleElementClassified = singleElementClassifiedArray[extractorIndex];
                final Object duplicateElementClassified = extractor.apply(duplicatesElement);
                final boolean matches = singleElementClassified.equals(duplicateElementClassified);
                if (!matches) {
                    throw new IllegalArgumentException(
                            "%s is not a duplicate of %s because %s does not equal %s".formatted(duplicatesElement,
                                                                                                 single,
                                                                                                 duplicateElementClassified,
                                                                                                 singleElementClassified)
                    );
                }
            }
        }
        return single;
    }

    @SafeVarargs
    static <T> Function<Iterable<T>, T> singleFromDuplicates(final Function<T, ?>... extractors) {
        return iterable -> singleFromDuplicates(iterable, extractors);
    }
}
