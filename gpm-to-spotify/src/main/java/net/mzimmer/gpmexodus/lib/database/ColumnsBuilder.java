package net.mzimmer.gpmexodus.lib.database;

import io.vavr.collection.Array;
import io.vavr.collection.Seq;

import java.util.function.Function;

import static io.vavr.API.TODO;

public final class ColumnsBuilder<E extends Entity<PK>, PK> {
    private Seq<Column> primaryKeyColumns = Array.empty();
    private Seq<Column> nonPrimaryKeyColumns = Array.empty();
    private Function<PK, Seq<Parameter<?>>> primaryKeyParameters = ignored -> Array.empty();
    private Function<E, Seq<Parameter<?>>> nonPrimaryKeyParameters = ignored -> Array.empty();
    private ResultSetFunction<E> readEntity = ignored -> TODO();

    private ColumnsBuilder() {
    }

    public static <E extends Entity<PK>, PK> ColumnsBuilder<E, PK> columns() {
        return new ColumnsBuilder<>();
    }

    public static <E extends Entity<PK>, PK> ColumnsBuilder<E, PK> columns(final Class<E> entityClass,
                                                                           final Class<PK> primaryKeyClass) {
        return columns();
    }

    public ColumnsBuilder<E, PK> withPrimaryColumns(final Seq<Column> primaryKeyColumns) {
        this.primaryKeyColumns = primaryKeyColumns;
        return this;
    }

    public ColumnsBuilder<E, PK> withPrimaryColumnNames(final Seq<ColumnName> primaryKeyColumnNames) {
        return withPrimaryColumns(primaryKeyColumnNames.map(Column.Default::new));
    }

    public ColumnsBuilder<E, PK> withPrimaryColumnNames(final ColumnName... primaryKeyColumnNames) {
        return withPrimaryColumnNames(Array.of(primaryKeyColumnNames));
    }

    public ColumnsBuilder<E, PK> withNonPrimaryColumns(final Seq<Column> nonPrimaryKeyColumns) {
        this.nonPrimaryKeyColumns = nonPrimaryKeyColumns;
        return this;
    }

    public ColumnsBuilder<E, PK> withNonPrimaryColumnNames(final Seq<ColumnName> nonPrimaryKeyColumnNames) {
        return withNonPrimaryColumns(nonPrimaryKeyColumnNames.map(Column.Default::new));
    }

    public ColumnsBuilder<E, PK> withNonPrimaryColumnNames(final ColumnName... nonPrimaryKeyColumnNames) {
        return withNonPrimaryColumnNames(Array.of(nonPrimaryKeyColumnNames));
    }

    public ColumnsBuilder<E, PK> withPrimaryKeyParameters(final Function<PK, Seq<Parameter<?>>> primaryKeyParameters) {
        this.primaryKeyParameters = primaryKeyParameters;
        return this;
    }

    public ColumnsBuilder<E, PK> withNonPrimaryKeyParameters(final Function<E, Seq<Parameter<?>>> nonPrimaryKeyParameters) {
        this.nonPrimaryKeyParameters = nonPrimaryKeyParameters;
        return this;
    }

    public ColumnsBuilder<E, PK> withReadEntity(final ResultSetFunction<E> readEntity) {
        this.readEntity = readEntity;
        return this;
    }

    public Columns<E, PK> build() {
        return new Columns.Default<>(primaryKeyColumns,
                                     nonPrimaryKeyColumns,
                                     primaryKeyParameters,
                                     nonPrimaryKeyParameters,
                                     readEntity);
    }
}
