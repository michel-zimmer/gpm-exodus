package net.mzimmer.gpmexodus.lib.cli;

public record ParameterValue(String value) {
}
