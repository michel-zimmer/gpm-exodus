package net.mzimmer.gpmexodus.lib.cli.parser;

import io.vavr.collection.Seq;
import io.vavr.control.Either;
import io.vavr.control.Option;
import net.mzimmer.gpmexodus.lib.cli.CommandDefinition;
import net.mzimmer.gpmexodus.lib.cli.Parameter;
import net.mzimmer.gpmexodus.lib.cli.ParameterDefinition;
import net.mzimmer.gpmexodus.lib.cli.ParameterName;

import static io.vavr.API.TODO;

final record SubCommandContext<C>(CommandDefinition<? extends C> commandDefinition,
                                  Seq<Parameter> parameters) implements Context<C> {
    @Override
    public Context<C> next(String argument) {
        final Option<ParameterName> maybeParameterName = Context.parseParameterName(argument);

        return maybeParameterName.fold(
                () -> new ErrorContext<>(TODO()),
                parameterName -> commandDefinition.findParameterDefinition(parameterName)
                                                  .fold(() -> new ErrorContext<>(TODO()),
                                                        this::withParameter)
        );
    }

    private Context<C> withParameter(ParameterDefinition<?> parameterDefinition) {
        if (parameters
                .map(Parameter::name)
                .find(parameterDefinition.name()::equals)
                .isDefined()) {
            return new ErrorContext<>(TODO());
        }

        if (parameterDefinition.hasPayload()) {
            return new SubCommandParameterValueContext<>(commandDefinition, parameters, parameterDefinition);
        }

        final Parameter newParameter = new Parameter(parameterDefinition.name(), Option.none());
        return new SubCommandContext<>(commandDefinition, parameters.append(newParameter));
    }

    @Override
    public Either<String, C> build() {
        // TODO: Check required args
        return Either.right(commandDefinition.builder().apply(parameters));
    }
}
