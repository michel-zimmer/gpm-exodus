package net.mzimmer.gpmexodus.lib.cli;

import io.vavr.control.Option;

public record Parameter(ParameterName name,
                        Option<ParameterValue> parameterValue) {
}
