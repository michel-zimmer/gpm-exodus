package net.mzimmer.gpmexodus.lib.cli.parser;

import io.vavr.collection.Seq;
import io.vavr.control.Either;
import net.mzimmer.gpmexodus.lib.cli.CommandDefinition;
import net.mzimmer.gpmexodus.lib.cli.Parameter;
import net.mzimmer.gpmexodus.lib.cli.ParameterDefinition;
import net.mzimmer.gpmexodus.lib.cli.ParameterValue;

import static io.vavr.API.TODO;


final record SubCommandParameterValueContext<C>(CommandDefinition<? extends C> commandDefinition,
                                                Seq<Parameter> parameters,
                                                ParameterDefinition<?> parameterDefinition) implements Context<C> {

    @Override
    public Context<C> next(String argument) {
        final ParameterValue value = new ParameterValue(argument);
        final Parameter newParameter = parameterDefinition.asParameter(value);
        return new SubCommandContext<>(commandDefinition, parameters.append(newParameter));
    }

    @Override
    public Either<String, C> build() {
        return Either.left(TODO());
    }
}
