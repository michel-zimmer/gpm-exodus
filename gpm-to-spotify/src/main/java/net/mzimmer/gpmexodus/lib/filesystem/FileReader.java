package net.mzimmer.gpmexodus.lib.filesystem;

import io.vavr.CheckedFunction1;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;

@FunctionalInterface
public interface FileReader {
    InputStream read(Path path);

    FileReader SYSTEM_FILE_READER = CheckedFunction1.<Path, InputStream>of(Files::newInputStream).unchecked()::apply;
    FileReader RESOURCES_FILE_READER = resourcesFileReader(FileReader.class.getClassLoader());

    static FileReader resourcesFileReader(final ClassLoader classLoader) {
        return path -> classLoader.getResourceAsStream(path.toString());
    }
}
