package net.mzimmer.gpmexodus.lib.stdio;

public record StderrOutput(String value) {
}
