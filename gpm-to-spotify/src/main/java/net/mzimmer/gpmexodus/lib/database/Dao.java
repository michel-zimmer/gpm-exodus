package net.mzimmer.gpmexodus.lib.database;

import io.vavr.Tuple;
import io.vavr.collection.Map;
import io.vavr.collection.Seq;
import io.vavr.control.Option;

import java.util.function.Function;

public interface Dao<ENTITY extends Entity<PRIMARY_KEY>, PRIMARY_KEY> {

    Seq<ENTITY> findAll();

    Option<ENTITY> findByPrimaryKey(PRIMARY_KEY primaryKey);

    default Map<PRIMARY_KEY, ENTITY> findByPrimaryKeys(Seq<PRIMARY_KEY> primaryKeys) {
        return primaryKeys.toMap(Function.identity(), this::findByPrimaryKey)
                          .flatMap((primaryKey, maybeEntity) -> maybeEntity.map(entity -> Tuple.of(primaryKey, entity)));
    }

    int insert(ENTITY entity);

    default int insert(Seq<ENTITY> entities) {
        return entities.map(this::insert).fold(0, Integer::sum);
    }

    default int insertOrUpdate(ENTITY entity) {
        return findByPrimaryKey(entity.primaryKey()).fold(
                () -> insert(entity),
                savedEntity -> update(entity)
        );
    }

    default int insertOrUpdate(Seq<ENTITY> entities) {
        return entities.map(this::insertOrUpdate).fold(0, Integer::sum);
    }

    int update(ENTITY entity);

    default int delete(ENTITY entity) {
        return deleteByPrimaryKey(entity.primaryKey());
    }

    default int delete(Seq<ENTITY> entities) {
        return entities.map(this::delete).fold(0, Integer::sum);
    }

    int deleteByPrimaryKey(PRIMARY_KEY primaryKey);

    default int deleteByPrimaryKeys(Seq<PRIMARY_KEY> primaryKeys) {
        return primaryKeys.map(this::deleteByPrimaryKey).fold(0, Integer::sum);
    }
}
