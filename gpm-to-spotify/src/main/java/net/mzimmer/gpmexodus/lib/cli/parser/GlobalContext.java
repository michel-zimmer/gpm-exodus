package net.mzimmer.gpmexodus.lib.cli.parser;

import io.vavr.collection.List;
import io.vavr.collection.Seq;
import io.vavr.control.Either;
import io.vavr.control.Option;
import net.mzimmer.gpmexodus.lib.cli.*;

import static io.vavr.API.TODO;

final record GlobalContext<C>(CommandDefinitions<C> commandDefinitions,
                              Seq<Parameter> parameters) implements Context<C> {
    public static <C> Context<C> empty(final CommandDefinitions<C> commandDefinitions) {
        return new GlobalContext<>(commandDefinitions, List.empty());
    }

    @Override
    public Context<C> next(final String argument) {
        final Option<ParameterName> maybeParameterName = Context.parseParameterName(argument);

        return maybeParameterName.fold(
                () -> {
                    final Option<SubCommandName> maybeSubCommandName = Context.parseSubCommandName(argument);

                    return maybeSubCommandName.fold(
                            () -> new ErrorContext<>(TODO()),
                            subCommandName -> commandDefinitions.findCommandDefinition(subCommandName)
                                                                .fold(() -> new ErrorContext<>(TODO()),
                                                                      this::withSubCommand)
                    );
                },
                parameterName -> commandDefinitions.findGlobalParameterDefinition(parameterName)
                                                   .fold(() -> new ErrorContext<>(TODO()),
                                                         this::withParameter)
        );
    }

    private Context<C> withParameter(final ParameterDefinition<?> parameterDefinition) {
        if (this.parameters()
                .map(Parameter::name)
                .find(parameterDefinition.name()::equals)
                .isDefined()) {
            return new ErrorContext<>(TODO());
        }

        if (parameterDefinition.hasPayload()) {
            return new GlobalParameterValueContext<>(commandDefinitions, parameters, parameterDefinition);
        }

        final Parameter newParameter = new Parameter(parameterDefinition.name(), Option.none());
        return new GlobalContext<>(commandDefinitions, parameters.append(newParameter));
    }

    private Context<C> withSubCommand(CommandDefinition<? extends C> commandDefinition) {
        return new SubCommandContext<>(commandDefinition, parameters);
    }

    @Override
    public Either<String, C> build() {
        final Seq<CommandDefinition<? extends C>> matches =
                commandDefinitions.findGlobalCommandDefinitions(parameters);

        if (matches.size() == 1) {
            final CommandDefinition<? extends C> singleMatch = matches.get(0);
            // TODO: Check required args
            return Either.right(singleMatch.builder().apply(parameters));
        } else {
            return Either.left("" + TODO());
        }
    }
}
