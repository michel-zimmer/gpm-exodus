package net.mzimmer.gpmexodus.lib.database;

public interface Entity<PRIMARY_KEY> {
    PRIMARY_KEY primaryKey();
}
