package net.mzimmer.gpmexodus.lib.cli;

public record SubCommandName(String value) {
}
