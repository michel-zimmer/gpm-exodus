package net.mzimmer.gpmexodus.lib.database;

import java.sql.PreparedStatement;
import java.sql.SQLException;

@FunctionalInterface
public interface PreparedStatementFunction<T> {
    T apply(PreparedStatement preparedStatement) throws SQLException;
}
