package net.mzimmer.gpmexodus.lib.cli.parser;

import io.vavr.collection.Seq;
import io.vavr.collection.Stream;
import io.vavr.control.Option;
import net.mzimmer.gpmexodus.lib.cli.*;

public class CommandDefinitions<C> {
    private final Stream<CommandDefinition<? extends C>> allCommandDefinitions;
    private final Stream<ParameterDefinition<?>> allGlobalParameters;

    private CommandDefinitions(final Stream<CommandDefinition<? extends C>> allCommandDefinitions,
                               final Stream<ParameterDefinition<?>> allGlobalParameters) {
        this.allCommandDefinitions = allCommandDefinitions;
        this.allGlobalParameters = allGlobalParameters;
    }

    @SafeVarargs
    public static <C> CommandDefinitions<C> commandDefinitions(final CommandDefinition<? extends C>... commandDefinitions) {
        return commandDefinitions(Stream.of(commandDefinitions));
    }

    public static <C> CommandDefinitions<C> commandDefinitions(Seq<CommandDefinition<? extends C>> commandDefinitions) {
        final Stream<ParameterDefinition<?>> allGlobalParameters =
                commandDefinitions.flatMap(CommandDefinition::globalParameters)
                                  .toArray()
                                  .toStream();

        allGlobalParameters
                .groupBy(ParameterDefinition::name)
                .forEach((parameterName, parameterDefinitions) -> {
                    final ParameterDefinition<?> headParameterDefinition = parameterDefinitions.head();
                    parameterDefinitions.tail().forEach(tailParameterDefinition -> {
                        if (!headParameterDefinition.equals(tailParameterDefinition))
                            throw new RuntimeException("Global parameter " + parameterName.value() + " has been defined in different ways");
                    });
                });

        commandDefinitions
                .flatMap(CommandDefinition::subCommand)
                .groupBy(SubCommandDefinition::name)
                .forEach((subCommandName, subCommandDefinitions) -> {
                    if (subCommandDefinitions.size() > 1)
                        throw new RuntimeException("Sub command " + subCommandName.value() + " has been defined multiple times");
                });

        commandDefinitions
                .flatMap(CommandDefinition::subCommand)
                .forEach(subCommandDefinition -> subCommandDefinition
                        .parameters()
                        .groupBy(ParameterDefinition::name)
                        .forEach((parameterName, parameterDefinitions) -> {
                            if (parameterDefinitions.size() > 1)
                                throw new RuntimeException("Sub command " + subCommandDefinition.name().value() + " parameter " + parameterName.value() + " has been defined multiple times");
                        })
                );

        final Stream<CommandDefinition<? extends C>> allCommandDefinitions = commandDefinitions.toArray().toStream();
        return new CommandDefinitions<>(allCommandDefinitions, allGlobalParameters);
    }

    public Option<ParameterDefinition<?>> findGlobalParameterDefinition(ParameterName parameterName) {
        return allGlobalParameters.find(parameterDefinition -> parameterDefinition.name().equals(parameterName));
    }

    public Seq<CommandDefinition<? extends C>> findGlobalCommandDefinitions(Seq<Parameter> parameters) {
        // TODO: Test
        return allCommandDefinitions.filter(
                commandDefinition -> commandDefinition.subCommand()
                                                      .isEmpty() &&
                                     commandDefinition.globalParameters()
                                                      .map(ParameterDefinition::name)
                                                      .containsAll(parameters.map(Parameter::name))
        );
    }

    public Option<CommandDefinition<? extends C>> findCommandDefinition(SubCommandName subCommandName) {
        // TODO: Test
        return allCommandDefinitions.find(
                commandDefinition -> commandDefinition.subCommand()
                                                      .map(SubCommandDefinition::name)
                                                      .fold(() -> false, subCommandName::equals)
        );
    }
}
