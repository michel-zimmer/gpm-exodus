package net.mzimmer.gpmexodus.lib;

import io.vavr.Tuple;
import io.vavr.collection.Array;
import io.vavr.collection.Seq;
import io.vavr.control.Option;

public class TableRenderer {
    public <ROW extends Tuple> String renderAsTable(final Seq<ROW> rows) {
        return renderAsTable(Option.none(), rows);
    }

    public <ROW extends Tuple> String renderAsTable(final ROW header,
                                                    final Seq<ROW> rows) {
        return renderAsTable(Option.of(header), rows);
    }

    public <ROW extends Tuple> String renderAsTable(final Option<ROW> header,
                                                    final Seq<ROW> rows) {
        final Seq<ROW> allContentRows = header.fold(() -> rows, rows::prepend);
        final Option<ROW> maybeFirstContentRow = allContentRows.headOption();
        if (maybeFirstContentRow.isEmpty()) {
            return "";
        }
        final ROW firstContentRow = maybeFirstContentRow.get();
        final int numberOfColumns = firstContentRow.toSeq().size();
        if (numberOfColumns == 0) {
            return "";
        }
        final Seq<Seq<String>> allContentRowsRendered = allContentRows.map(this::toString);
        final Seq<Integer> columnIndices = Array.range(0, numberOfColumns);
        final Seq<Integer> maxWidths = columnIndices.map(
                columnIndex -> allContentRowsRendered.foldLeft(0, (maxWidth, row) -> {
                    final String cell = row.get(columnIndex);
                    final int cellWidth = cell.length();
                    return Math.max(maxWidth, cellWidth);
                })
        );

        final Seq<Seq<String>> allRowsRendered = header.fold(
                () -> allContentRowsRendered,
                notNeeded -> {
                    final Seq<String> borders = maxWidths.map("-"::repeat);
                    return allContentRowsRendered.insert(1, borders);
                }
        );

        return allRowsRendered.zipWithIndex().map(rowWithIndex -> {
            final Seq<String> row = rowWithIndex._1();
            final int rowIndex = rowWithIndex._2();
            return row.zipWithIndex().map(cellWithColumnIndex -> {
                final String cell = cellWithColumnIndex._1();
                final int columnIndex = cellWithColumnIndex._2();
                final boolean isLastColumn = columnIndex + 1 == numberOfColumns;
                if (header.isDefined() && rowIndex == 1) {
                    final String tableFilling = isLastColumn ? "" : "-+-";
                    return cell + tableFilling;
                }
                final int maxWidth = maxWidths.get(columnIndex);
                final int width = cell.length();
                final String sameWidthFilling = isLastColumn ? "" : " ".repeat(maxWidth - width);
                final String tableFilling = isLastColumn ? "" : " | ";
                return cell + sameWidthFilling + tableFilling;
            }).mkString().trim();
        }).mkString(System.lineSeparator());
    }

    private <ROW extends Tuple> Seq<String> toString(ROW row) {
        return row.toSeq().map(String::valueOf).toArray();
    }
}
