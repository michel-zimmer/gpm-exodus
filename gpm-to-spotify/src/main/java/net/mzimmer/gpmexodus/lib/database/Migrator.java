package net.mzimmer.gpmexodus.lib.database;

import javax.sql.DataSource;

public interface Migrator {
    void migrate(DataSource dataSource);
}
