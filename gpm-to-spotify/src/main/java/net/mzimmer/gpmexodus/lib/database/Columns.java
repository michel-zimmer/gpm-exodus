package net.mzimmer.gpmexodus.lib.database;

import io.vavr.collection.Seq;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.function.Function;

public interface Columns<E extends Entity<PK>, PK> {
    default Seq<Column> columns() {
        return primaryKeyColumns().appendAll(nonPrimaryKeyColumns());
    }

    Seq<Column> primaryKeyColumns();

    Seq<Column> nonPrimaryKeyColumns();

    default Seq<Parameter<?>> parameters(E entity) {
        return primaryKeyParameters(entity).appendAll(nonPrimaryKeyParameters(entity));
    }

    default Seq<Parameter<?>> primaryKeyParameters(E entity) {
        return primaryKeyParameters(entity.primaryKey());
    }

    Seq<Parameter<?>> primaryKeyParameters(PK primaryKey);

    Seq<Parameter<?>> nonPrimaryKeyParameters(E entity);

    E readEntity(ResultSet resultSet) throws SQLException;

    record Default<E extends Entity<PK>, PK>(Seq<Column> primaryKeyColumns,
                                             Seq<Column> nonPrimaryKeyColumns,
                                             Function<PK, Seq<Parameter<?>>> primaryKeyParameters,
                                             Function<E, Seq<Parameter<?>>> nonPrimaryKeyParameters,
                                             ResultSetFunction<E> readEntity) implements Columns<E, PK> {
        @Override
        public Seq<Parameter<?>> primaryKeyParameters(final PK primaryKey) {
            return primaryKeyParameters.apply(primaryKey);
        }

        @Override
        public Seq<Parameter<?>> nonPrimaryKeyParameters(final E entity) {
            return nonPrimaryKeyParameters.apply(entity);
        }

        @Override
        public E readEntity(final ResultSet resultSet) throws SQLException {
            return readEntity.apply(resultSet);
        }
    }
}
