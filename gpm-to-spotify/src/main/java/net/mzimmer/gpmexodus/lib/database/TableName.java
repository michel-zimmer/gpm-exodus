package net.mzimmer.gpmexodus.lib.database;

public record TableName(String value) {
}
