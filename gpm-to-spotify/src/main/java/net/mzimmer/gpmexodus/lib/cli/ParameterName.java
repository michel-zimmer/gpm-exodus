package net.mzimmer.gpmexodus.lib.cli;

public record ParameterName(String value) {
}
