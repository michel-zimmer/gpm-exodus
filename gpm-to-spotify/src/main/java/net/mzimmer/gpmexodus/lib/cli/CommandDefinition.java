package net.mzimmer.gpmexodus.lib.cli;

import io.vavr.collection.List;
import io.vavr.collection.Seq;
import io.vavr.control.Option;

import java.util.function.Function;

public record CommandDefinition<C>(Seq<ParameterDefinition<?>> globalParameters,
                                   Option<SubCommandDefinition> subCommand,
                                   Function<Seq<Parameter>, C> builder) {

    public static <C> CommandDefinition<C> commandDefinition(final Seq<ParameterDefinition<?>> globalParameters,
                                                             final Option<SubCommandDefinition> subCommand,
                                                             final Function<Seq<Parameter>, C> builder) {
        return new CommandDefinition<>(globalParameters,
                                       subCommand,
                                       builder);
    }

    public Option<ParameterDefinition<?>> findParameterDefinition(ParameterName parameterName) {
        // TODO: Test
        return globalParameters.appendAll(subCommand.fold(List::empty, SubCommandDefinition::parameters))
                               .find(parameterDefinition -> parameterDefinition.matches(parameterName));
    }
}
