package net.mzimmer.gpmexodus.lib.database;

public interface Parameter<T> {
    T value();

    final record Text(String value) implements Parameter<String> {
    }

    final record Integer(java.lang.Integer value) implements Parameter<java.lang.Integer> {
    }
}
