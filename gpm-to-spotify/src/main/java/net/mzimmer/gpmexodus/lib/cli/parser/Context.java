package net.mzimmer.gpmexodus.lib.cli.parser;

import io.vavr.control.Either;
import io.vavr.control.Option;
import net.mzimmer.gpmexodus.lib.cli.ParameterName;
import net.mzimmer.gpmexodus.lib.cli.SubCommandName;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

interface Context<C> {
    Context<C> next(String argument);

    Either<String, C> build();

    Pattern PARAMETER_NAME_PATTERN = Pattern.compile("--([a-z][a-z0-9\\-]*)");

    static Option<ParameterName> parseParameterName(final String argument) {
        final Matcher matcher = PARAMETER_NAME_PATTERN.matcher(argument);
        if (matcher.matches()) {
            return Option.of(new ParameterName(matcher.group(1)));
        } else {
            return Option.none();
        }
    }

    Pattern SUB_COMMAND_NAME_PATTERN = Pattern.compile("([a-z][a-z0-9\\-]*)");

    static Option<SubCommandName> parseSubCommandName(final String argument) {
        final Matcher matcher = SUB_COMMAND_NAME_PATTERN.matcher(argument);
        if (matcher.matches()) {
            return Option.of(new SubCommandName(matcher.group(1)));
        } else {
            return Option.none();
        }
    }
}
