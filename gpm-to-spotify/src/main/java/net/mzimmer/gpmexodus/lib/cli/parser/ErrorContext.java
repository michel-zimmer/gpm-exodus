package net.mzimmer.gpmexodus.lib.cli.parser;

import io.vavr.control.Either;


final record ErrorContext<C>(String message) implements Context<C> {
    @Override
    public Context<C> next(String argument) {
        return this;
    }

    @Override
    public Either<String, C> build() {
        return Either.left(message);
    }
}
