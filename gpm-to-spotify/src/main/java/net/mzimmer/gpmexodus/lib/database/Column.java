package net.mzimmer.gpmexodus.lib.database;

import io.vavr.collection.Seq;

public interface Column {
    ColumnName name();

    record Default(ColumnName name) implements Column {
    }

    static String mkString(final Seq<Column> columns,
                           final String suffix,
                           final String delimiter) {
        return columns.map(Column::name)
                      .map(ColumnName::value)
                      .map(columnName -> columnName + suffix)
                      .mkString(delimiter);
    }
}
