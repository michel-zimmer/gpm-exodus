package net.mzimmer.gpmexodus.lib.cli;

import io.vavr.collection.Seq;
import io.vavr.control.Option;

import java.util.function.Function;

public record ParameterDefinition<PAYLOAD>(ParameterName name,
                                           Option<Function<String, PAYLOAD>> payloadBuilder) {
    public static ParameterDefinition<Void> parameterDefinition(final String name) {
        return new ParameterDefinition<>(new ParameterName(name),
                                         Option.none());
    }

    public static <PAYLOAD> ParameterDefinition<PAYLOAD> parameterDefinition(final String name,
                                                                             final Function<String, PAYLOAD> payloadBuilder) {
        return new ParameterDefinition<>(new ParameterName(name),
                                         Option.of(payloadBuilder));
    }

    public boolean hasPayload() {
        return payloadBuilder.isDefined();
    }

    public PAYLOAD payload(Seq<Parameter> parameters) {
        return payloadBuilder.get()
                             .apply(parameters.find(this::matches)
                                              .get()
                                              .parameterValue()
                                              .get()
                                              .value());
    }

    public boolean matches(final Parameter parameter) {
        return matches(parameter.name());
    }

    public boolean matches(ParameterName parameterName) {
        return name.equals(parameterName);
    }

    public Parameter asParameter(ParameterValue value) {
        return new Parameter(name, Option.of(value));
    }
}
