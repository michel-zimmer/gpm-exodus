package net.mzimmer.gpmexodus.lib.database.flyway;

import net.mzimmer.gpmexodus.lib.database.Migrator;
import org.flywaydb.core.Flyway;

import javax.sql.DataSource;

public class FlywayMigrator implements Migrator {
    @Override
    public void migrate(DataSource dataSource) {
        Flyway.configure().dataSource(dataSource).load().migrate();
    }
}
