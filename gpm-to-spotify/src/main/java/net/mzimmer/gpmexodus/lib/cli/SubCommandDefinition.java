package net.mzimmer.gpmexodus.lib.cli;

import io.vavr.collection.List;
import io.vavr.collection.Seq;

public record SubCommandDefinition(SubCommandName name,
                                   Seq<ParameterDefinition<?>> parameters) {
    public static SubCommandDefinition subCommandDefinition(final String name) {
        return new SubCommandDefinition(new SubCommandName(name),
                                        List.empty());
    }

    public static SubCommandDefinition subCommandDefinition(final String name,
                                                            final Seq<ParameterDefinition<?>> parameters) {
        return new SubCommandDefinition(new SubCommandName(name),
                                        parameters);
    }
}
