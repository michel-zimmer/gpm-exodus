package net.mzimmer.gpmexodus.boot;

import io.vavr.control.Option;
import net.mzimmer.gpmexodus.infrastructure.spotify.SpotifyCredentials;
import net.mzimmer.gpmexodus.lib.database.JdbcUri;

public record Config(Option<JdbcUri> jdbcUri,
                     Option<SpotifyCredentials> spotifyCredentials) {

    public static final Config EMPTY = new Config(Option.none(),
                                                  Option.none());

    public Config(final JdbcUri jdbcUri) {
        this(Option.of(jdbcUri),
             Option.none());
    }

    public Config(final SpotifyCredentials spotifyCredentials) {
        this(Option.none(),
             Option.of(spotifyCredentials));
    }

    public Config(final JdbcUri jdbcUri,
                  final SpotifyCredentials spotifyCredentials) {
        this(Option.of(jdbcUri),
             Option.of(spotifyCredentials));
    }
}
