package net.mzimmer.gpmexodus.boot;

import io.vavr.control.Either;
import net.mzimmer.gpmexodus.adapter.active.*;
import net.mzimmer.gpmexodus.lib.cli.parser.CommandDefinitions;
import net.mzimmer.gpmexodus.lib.cli.parser.Parser;

import static net.mzimmer.gpmexodus.lib.cli.parser.CommandDefinitions.commandDefinitions;

public class Cli {
    public Either<String, Command> parse(String... args) {
        final CommandDefinitions<Command> commandDefinitions = commandDefinitions(
                ImportGpmPlaylistsCommand.DEFINITION,
                ListImportedGpmPlaylistsCommand.DEFINITION,
                ListImportedGpmPlaylistCommand.DEFINITION,
                LinkSpotifyTracksCommand.DEFINITION,
                LinkSpotifyTrackCommand.DEFINITION,
                LinkProgressCommand.DEFINITION,
                SpotifyLoginCommand.DEFINITION,
                SyncPlaylistCommand.DEFINITION
        );
        final Parser<Command> parser = new Parser<>(commandDefinitions);
        return parser.parse(args);
    }
}
