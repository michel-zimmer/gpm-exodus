package net.mzimmer.gpmexodus.boot;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wrapper.spotify.IHttpManager;
import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.exceptions.SpotifyWebApiException;
import com.wrapper.spotify.model_objects.credentials.ClientCredentials;
import io.vavr.Lazy;
import net.mzimmer.gpmexodus.adapter.passive.PlaylistLinkDaoRepository;
import net.mzimmer.gpmexodus.adapter.passive.TrackLinkDaoRepository;
import net.mzimmer.gpmexodus.adapter.passive.gpm.GpmPlaylistDatabaseRepository;
import net.mzimmer.gpmexodus.adapter.passive.gpm.GpmTrackDatabaseRepository;
import net.mzimmer.gpmexodus.adapter.passive.spotify.PagingHandler;
import net.mzimmer.gpmexodus.adapter.passive.spotify.SpotifyPlaylistUpstreamRepository;
import net.mzimmer.gpmexodus.adapter.passive.spotify.SpotifyTrackUpstreamRepository;
import net.mzimmer.gpmexodus.application.ImportApplicationService;
import net.mzimmer.gpmexodus.application.ManageApplicationService;
import net.mzimmer.gpmexodus.application.SpotifyApplicationService;
import net.mzimmer.gpmexodus.application.SpotifyWithLoginApplicationService;
import net.mzimmer.gpmexodus.domain.*;
import net.mzimmer.gpmexodus.domain.gpm.GpmPlaylistRepository;
import net.mzimmer.gpmexodus.domain.gpm.GpmTrackRepository;
import net.mzimmer.gpmexodus.domain.spotify.SpotifyPlaylistRepository;
import net.mzimmer.gpmexodus.domain.spotify.SpotifyTrackRepository;
import net.mzimmer.gpmexodus.infrastructure.callbackserver.CallbackServer;
import net.mzimmer.gpmexodus.infrastructure.database.PlaylistLinkDao;
import net.mzimmer.gpmexodus.infrastructure.database.TrackLinkDao;
import net.mzimmer.gpmexodus.infrastructure.database.gpm.GpmAlbumDao;
import net.mzimmer.gpmexodus.infrastructure.database.gpm.GpmPlaylistDao;
import net.mzimmer.gpmexodus.infrastructure.database.gpm.GpmPlaylistEntryDao;
import net.mzimmer.gpmexodus.infrastructure.database.gpm.GpmTrackDao;
import net.mzimmer.gpmexodus.infrastructure.spotify.SpotifyCredentials;
import net.mzimmer.gpmexodus.lib.TableRenderer;
import net.mzimmer.gpmexodus.lib.database.JdbcUri;
import net.mzimmer.gpmexodus.lib.database.Migrator;
import net.mzimmer.gpmexodus.lib.filesystem.FileReader;
import net.mzimmer.gpmexodus.lib.stdio.Stdio;
import org.apache.hc.core5.http.ParseException;
import org.sqlite.SQLiteDataSource;

import javax.sql.DataSource;
import java.io.IOException;

public class Application {
    public Application(final Config config,
                       final Stdio stdio,
                       final FileReader fileReader,
                       final ObjectMapper objectMapper,
                       final Migrator migrator,
                       final CallbackServer callbackServer,
                       final IHttpManager spotifyHttpManager) {
        this.config = config;
        this.stdio = stdio;
        this.fileReader = fileReader;
        this.objectMapper = objectMapper;
        this.migrator = migrator;
        this.callbackServer = callbackServer;
        this.spotifyHttpManager = spotifyHttpManager;
    }

    // common

    private final Config config;

    public Config config() {
        return config;
    }

    private final Stdio stdio;

    public Stdio stdio() {
        return stdio;
    }

    private final Lazy<TableRenderer> tableRenderer = Lazy.of(TableRenderer::new);

    public TableRenderer tableRenderer() {
        return tableRenderer.get();
    }

    private final CallbackServer callbackServer;

    public CallbackServer callbackServer() {
        return callbackServer;
    }

    private final FileReader fileReader;

    public FileReader fileReader() {
        return fileReader;
    }

    private final ObjectMapper objectMapper;

    public ObjectMapper objectMapper() {
        return objectMapper;
    }

    // database

    private final Lazy<JdbcUri> jdbcUri = Lazy.of(this::buildJdbcUri);

    private JdbcUri buildJdbcUri() {
        return config().jdbcUri().get();
    }

    public JdbcUri getJdbcUri() {
        return jdbcUri.get();
    }

    private final Migrator migrator;

    public Migrator migrator() {
        return migrator;
    }

    private final Lazy<DataSource> dataSource = Lazy.of(this::buildDataSource);

    private DataSource buildDataSource() {
        SQLiteDataSource sqliteDataSource = new SQLiteDataSource();
        sqliteDataSource.setUrl(getJdbcUri().value());
        migrator().migrate(sqliteDataSource);
        return sqliteDataSource;
    }

    public DataSource dataSource() {
        return dataSource.get();
    }

    // data access objects

    private final Lazy<GpmAlbumDao> gpmAlbumDao = Lazy.of(this::buildGpmAlbumDao);

    private GpmAlbumDao buildGpmAlbumDao() {
        return new GpmAlbumDao(dataSource());
    }

    public GpmAlbumDao gpmAlbumDao() {
        return gpmAlbumDao.get();
    }

    private final Lazy<GpmTrackDao> gpmTrackDao = Lazy.of(this::buildGpmTrackDao);

    private GpmTrackDao buildGpmTrackDao() {
        return new GpmTrackDao(dataSource());
    }

    public GpmTrackDao gpmTrackDao() {
        return gpmTrackDao.get();
    }

    private final Lazy<GpmPlaylistDao> gpmPlaylistDao = Lazy.of(this::buildGpmPlaylistDao);

    private GpmPlaylistDao buildGpmPlaylistDao() {
        return new GpmPlaylistDao(dataSource());
    }

    public GpmPlaylistDao gpmPlaylistDao() {
        return gpmPlaylistDao.get();
    }

    private final Lazy<GpmPlaylistEntryDao> gpmPlaylistEntryDao = Lazy.of(this::buildGpmPlaylistEntryDao);

    private GpmPlaylistEntryDao buildGpmPlaylistEntryDao() {
        return new GpmPlaylistEntryDao(dataSource());
    }

    public GpmPlaylistEntryDao gpmPlaylistEntryDao() {
        return gpmPlaylistEntryDao.get();
    }

    private final Lazy<TrackLinkDao> trackLinkDao = Lazy.of(this::buildTrackLinkDao);

    private TrackLinkDao buildTrackLinkDao() {
        return new TrackLinkDao(dataSource());
    }

    public TrackLinkDao trackLinkDao() {
        return trackLinkDao.get();
    }

    private final Lazy<PlaylistLinkDao> playlistLinkDao = Lazy.of(this::buildPlaylistLinkDao);

    private PlaylistLinkDao buildPlaylistLinkDao() {
        return new PlaylistLinkDao(dataSource());
    }

    public PlaylistLinkDao playlistLinkDao() {
        return playlistLinkDao.get();
    }

    // spotify

    private final Lazy<SpotifyCredentials> spotifyCredentials = Lazy.of(this::buildSpotifyCredentials);

    private SpotifyCredentials buildSpotifyCredentials() {
        return config().spotifyCredentials().get();
    }

    public SpotifyCredentials spotifyCredentials() {
        return spotifyCredentials.get();
    }

    private final IHttpManager spotifyHttpManager;

    public IHttpManager spotifyHttpManager() {
        return spotifyHttpManager;
    }

    public SpotifyApi.Builder spotifyApiBuilder() {
        return SpotifyApi.builder()
                         .setHttpManager(spotifyHttpManager())
                         .setClientId(spotifyCredentials().clientId().value())
                         .setClientSecret(spotifyCredentials().clientSecret().value())
                         .setRedirectUri(callbackServer().redirectUri());
    }

    private final Lazy<SpotifyApi> spotifyApiWithoutLogin = Lazy.of(this::buildSpotifyApiWithoutLogin);

    private SpotifyApi buildSpotifyApiWithoutLogin() {
        try {
            final SpotifyApi spotifyApi = spotifyApiBuilder().build();
            final ClientCredentials clientCredentials = spotifyApi.clientCredentials()
                                                                  .grant_type("client_credentials")
                                                                  .build()
                                                                  .execute();
            spotifyApi.setAccessToken(clientCredentials.getAccessToken());
            return spotifyApi;
        } catch (ParseException | SpotifyWebApiException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    public SpotifyApi spotifyApiWithoutLogin() {
        return spotifyApiWithoutLogin.get();
    }

    private final Lazy<SpotifyApi> spotifyApiWithLogin = Lazy.of(this::buildSpotifyApiWithLogin);

    private SpotifyApi buildSpotifyApiWithLogin() {
        return spotifyApiBuilder().setAccessToken(spotifyCredentials().maybeAccessToken().get().value())
                                  .build();
    }

    public SpotifyApi spotifyApiWithLogin() {
        return spotifyApiWithLogin.get();
    }

    private final Lazy<PagingHandler> pagingHandler = Lazy.of(this::buildPagingHandler);

    private PagingHandler buildPagingHandler() {
        return new PagingHandler();
    }

    public PagingHandler pagingHandler() {
        return pagingHandler.get();
    }

    // repositories

    private final Lazy<GpmPlaylistRepository> gpmPlaylistRepository = Lazy.of(this::buildGpmPlaylistRepository);

    private GpmPlaylistRepository buildGpmPlaylistRepository() {
        return new GpmPlaylistDatabaseRepository(gpmAlbumDao(),
                                                 gpmTrackDao(),
                                                 gpmPlaylistDao(),
                                                 gpmPlaylistEntryDao());
    }

    public GpmPlaylistRepository gpmPlaylistRepository() {
        return gpmPlaylistRepository.get();
    }

    private final Lazy<GpmTrackRepository> gpmTrackRepository = Lazy.of(this::buildGpmTrackRepository);

    private GpmTrackRepository buildGpmTrackRepository() {
        return new GpmTrackDatabaseRepository(gpmTrackDao(),
                                              gpmAlbumDao());
    }

    public GpmTrackRepository gpmTrackRepository() {
        return gpmTrackRepository.get();
    }

    private final Lazy<TrackLinkRepository> trackLinkRepository = Lazy.of(this::buildTrackLinkRepository);

    private TrackLinkRepository buildTrackLinkRepository() {
        return new TrackLinkDaoRepository(trackLinkDao());
    }

    public TrackLinkRepository trackLinkRepository() {
        return trackLinkRepository.get();
    }

    private final Lazy<PlaylistLinkRepository> playlistLinkRepository = Lazy.of(this::buildPlaylistLinkRepository);

    private PlaylistLinkRepository buildPlaylistLinkRepository() {
        return new PlaylistLinkDaoRepository(playlistLinkDao());
    }

    public PlaylistLinkRepository playlistLinkRepository() {
        return playlistLinkRepository.get();
    }

    private final Lazy<SpotifyTrackRepository> spotifyTrackRepository = Lazy.of(this::buildSpotifyTrackRepository);

    private SpotifyTrackRepository buildSpotifyTrackRepository() {
        return new SpotifyTrackUpstreamRepository(spotifyApiWithoutLogin(),
                                                  pagingHandler());
    }

    public SpotifyTrackRepository spotifyTrackRepository() {
        return spotifyTrackRepository.get();
    }

    private final Lazy<SpotifyPlaylistRepository> spotifyPlaylistRepository = Lazy.of(this::buildSpotifyPlaylistRepository);

    private SpotifyPlaylistRepository buildSpotifyPlaylistRepository() {
        return new SpotifyPlaylistUpstreamRepository(spotifyApiWithLogin());
    }

    public SpotifyPlaylistRepository spotifyPlaylistRepository() {
        return spotifyPlaylistRepository.get();
    }

    // domain services

    private final Lazy<TrackConverter> trackConverter = Lazy.of(this::buildTrackConverter);

    private TrackConverter buildTrackConverter() {
        return new TrackConverter(trackLinkRepository());
    }

    public TrackConverter trackConverter() {
        return trackConverter.get();
    }

    private final Lazy<PlaylistConverter> playlistConverter = Lazy.of(this::buildPlaylistConverter);

    private PlaylistConverter buildPlaylistConverter() {
        return new PlaylistConverter(trackConverter());
    }

    public PlaylistConverter playlistConverter() {
        return playlistConverter.get();
    }

    private final Lazy<TrackLinkService> trackLinkService = Lazy.of(this::buildTrackLinkService);

    private TrackLinkService buildTrackLinkService() {
        return new TrackLinkService(trackLinkRepository(),
                                    spotifyTrackRepository());
    }

    public TrackLinkService trackLinkService() {
        return trackLinkService.get();
    }

    private final Lazy<PlaylistSynchronizationService> playlistSynchronizationService = Lazy.of(this::buildPlaylistSynchronizationService);

    private PlaylistSynchronizationService buildPlaylistSynchronizationService() {
        return new PlaylistSynchronizationService(playlistLinkRepository(),
                                                  spotifyPlaylistRepository(),
                                                  playlistConverter());
    }

    public PlaylistSynchronizationService playlistSynchronizationService() {
        return playlistSynchronizationService.get();
    }

    private final Lazy<LinkProgressService> linkProgressService = Lazy.of(this::buildLinkProgressService);

    private LinkProgressService buildLinkProgressService() {
        return new LinkProgressService(gpmPlaylistRepository(),
                                       gpmTrackRepository(),
                                       trackLinkRepository(),
                                       playlistLinkRepository());
    }

    public LinkProgressService linkProgressService() {
        return linkProgressService.get();
    }

    // application services

    private final Lazy<ImportApplicationService> importApplicationService = Lazy.of(this::buildImportApplicationService);

    private ImportApplicationService buildImportApplicationService() {
        return new ImportApplicationService(gpmPlaylistRepository());
    }

    public ImportApplicationService importApplicationService() {
        return importApplicationService.get();
    }

    private final Lazy<ManageApplicationService> manageApplicationService = Lazy.of(this::buildManageApplicationService);

    private ManageApplicationService buildManageApplicationService() {
        return new ManageApplicationService(gpmPlaylistRepository(),
                                            trackLinkRepository(),
                                            linkProgressService());
    }

    public ManageApplicationService manageApplicationService() {
        return manageApplicationService.get();
    }

    private final Lazy<SpotifyApplicationService> spotifyApplicationService = Lazy.of(this::buildSpotifyApplicationService);

    private SpotifyApplicationService buildSpotifyApplicationService() {
        return new SpotifyApplicationService(gpmPlaylistRepository(),
                                             trackLinkService());
    }

    public SpotifyApplicationService spotifyApplicationService() {
        return spotifyApplicationService.get();
    }

    private final Lazy<SpotifyWithLoginApplicationService> spotifyWithLoginApplicationService = Lazy.of(this::buildSpotifyWithLoginApplicationService);

    private SpotifyWithLoginApplicationService buildSpotifyWithLoginApplicationService() {
        return new SpotifyWithLoginApplicationService(gpmPlaylistRepository(),
                                                      playlistSynchronizationService());
    }

    public SpotifyWithLoginApplicationService spotifyWithLoginApplicationService() {
        return spotifyWithLoginApplicationService.get();
    }
}
