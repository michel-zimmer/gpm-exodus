package net.mzimmer.gpmexodus.boot;

@FunctionalInterface
public interface ApplicationBuilder {
    Application build(Config config);
}
