package net.mzimmer.gpmexodus.boot;

import com.fasterxml.jackson.databind.ObjectMapper;

public interface ObjectMappers {
    ObjectMapper DEFAULT_OBJECT_MAPPER = new ObjectMapper().findAndRegisterModules();
}
