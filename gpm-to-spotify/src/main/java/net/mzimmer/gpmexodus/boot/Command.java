package net.mzimmer.gpmexodus.boot;

import net.mzimmer.gpmexodus.lib.stdio.Output;

import static net.mzimmer.gpmexodus.boot.Config.EMPTY;

public interface Command {
    default Config config() {
        return EMPTY;
    }

    Output execute(Application application);
}
