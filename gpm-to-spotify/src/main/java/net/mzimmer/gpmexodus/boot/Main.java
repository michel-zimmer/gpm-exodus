package net.mzimmer.gpmexodus.boot;

import com.wrapper.spotify.SpotifyApi;
import net.mzimmer.gpmexodus.infrastructure.callbackserver.CallbackServer;
import net.mzimmer.gpmexodus.lib.database.flyway.FlywayMigrator;
import net.mzimmer.gpmexodus.lib.stdio.Output;
import net.mzimmer.gpmexodus.lib.stdio.Stdio;

import static net.mzimmer.gpmexodus.boot.ObjectMappers.DEFAULT_OBJECT_MAPPER;
import static net.mzimmer.gpmexodus.lib.filesystem.FileReader.SYSTEM_FILE_READER;

public class Main {
    private final Stdio stdio;
    private final Cli cli;
    private final ApplicationBuilder applicationBuilder;

    public Main(final Stdio stdio,
                final Cli cli,
                final ApplicationBuilder applicationBuilder) {
        this.stdio = stdio;
        this.cli = cli;
        this.applicationBuilder = applicationBuilder;
    }

    public static void main(final String... arguments) {
        new Main(Stdio.SYSTEM,
                 new Cli(),
                 config -> new Application(config,
                                           Stdio.SYSTEM,
                                           SYSTEM_FILE_READER,
                                           DEFAULT_OBJECT_MAPPER,
                                           new FlywayMigrator(),
                                           CallbackServer.DEFAULT,
                                           SpotifyApi.DEFAULT_HTTP_MANAGER))
                .run(arguments);
    }

    public void run(final String... arguments) {
        cli.parse(arguments)
           .fold(Output::stderr, this::executeCommand)
           .println(stdio);
    }

    private Output executeCommand(Command command) {
        final Config config = command.config();
        final Application application = applicationBuilder.build(config);
        return command.execute(application);
    }
}
