create table gpm_data_track_to_spotify
(
    gpm_data_track_store_id TEXT PRIMARY KEY,
    spotify_track_id        TEXT,
    FOREIGN KEY (gpm_data_track_store_id) REFERENCES gpm_data_track (store_id)
);
