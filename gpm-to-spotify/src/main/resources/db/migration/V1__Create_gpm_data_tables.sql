create table gpm_data_album
(
    id   TEXT PRIMARY KEY NOT NULL,
    name TEXT             NOT NULL
);

create table gpm_data_track
(
    title    TEXT             NOT NULL,
    artist   TEXT             NOT NULL,
    store_id TEXT PRIMARY KEY NOT NULL,
    album_id TEXT             NOT NULL,
    FOREIGN KEY (album_id) REFERENCES gpm_data_album (id)
);

create table gpm_data_playlist
(
    id          TEXT PRIMARY KEY NOT NULL,
    name        TEXT             NOT NULL,
    description TEXT             NOT NULL
);

create table gpm_data_playlist_entry
(
    playlist_id    TEXT    NOT NULL,
    position       INTEGER NOT NULL,
    track_store_id TEXT    NOT NULL,
    PRIMARY KEY (playlist_id, position),
    FOREIGN KEY (playlist_id) REFERENCES gpm_data_playlist (id),
    FOREIGN KEY (track_store_id) REFERENCES gpm_data_track (store_id)
);
