create table gpm_data_playlist_to_spotify
(
    gpm_data_playlist_id TEXT PRIMARY KEY,
    spotify_playlist_id  TEXT UNIQUE,
    FOREIGN KEY (gpm_data_playlist_id) REFERENCES gpm_data_playlist (id)
);
